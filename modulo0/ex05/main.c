#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>


int main(void){
	int vec[] = {1,2,3,4,5};
	int tamanho = 5;
	int vec2[5];
	int i;
	
	res_inverso(vec, tamanho, vec2);
	
	printf("Vetor original:\n");
	for(i = 0; i < tamanho; i++){
		printf("%d\n", *(vec+i));
	}
	
	printf("Segundo vetor:\n");
	for(i = 0; i < tamanho; i++){
		printf("%d\n", *(vec2+i));
	}
	
	return 0;
}

