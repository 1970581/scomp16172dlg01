#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h> 

// Calcula a soma dos valores pares de um vector de n inteiros.
int soma_par(int * vec, int tamanho){
	int resultado = 0;
	//int i = 0;

	int novo_tamanho = tamanho -1;
	if (novo_tamanho < 0) return 0;
	if( *(vec + (tamanho -1)) % 2 == 0) {
		resultado =  *(vec + (tamanho -1));
	}	

	resultado = soma_par( vec, novo_tamanho) + resultado;
	return resultado;

}



