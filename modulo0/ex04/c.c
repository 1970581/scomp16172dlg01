#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h> 

// Metodo que recursivamente subrai valor a todos os elementos de um vector
void subtrai_valor(int * vec, int tamanho, int valor){
	int novo_tamanho = tamanho-1;

	if( novo_tamanho < 0) return;

	*(vec + novo_tamanho) = * (vec + novo_tamanho) - valor;

	subtrai_valor(vec, novo_tamanho, valor);
	
}



