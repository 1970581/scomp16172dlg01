#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//Bibliotes de semaforos
#include <semaphore.h>

#define SEM_01 "sem_M4ex04"
#define SEM_LINHAS "sem_M4ex04_linhas"


//*******************************************
// Para correr o exercicio basta apenas escreve o seguinte commando no bash:
// ./main.exe lixo.txt &
// varias vezes. O "&" obriga o bash a correr o executavel como um processo filho 
// ".main.exe lixo.txt apagar" - apaga uma linha do ficheiro.
//**********
// Utilizacao de um semaforo para sinalizar o numero de linhas escritas no ficheiro
// conforme sugerido pela professora.
// ATENCAO: O Semafero pode disincronizar com o ficheiro, se o ficheiro for apagado ou o PC reiniciado.
//********
// APAGAR SEMAFOROS: 
// rm /dev/shm/sem.sem_M4ex04_linhas
// rm /dev/shm/sem.sem_M4ex04 


void escreverLinha(char * nomeFicheiro );
int  apagarLinha(char * nomeFicheiro );

int main(int argc, char *argv[]){ 
	int error;		//Diagnostico dos processos de partilha de memoria
	size_t pid ;
	sem_t * semaforo;
	sem_t * semaforoLinhas;
//	int i = 0;
//	int j = 0;
	int apagar = 0; // Indica se e para pagar uma linha (apagar == 1);
	 	
	
	//printf("arg: %i\n", argc);
	if(argc < 2 ) {
			printf("\nINSTRUCOES:\n");
			printf("Indique o nome do ficheiro como argumento ex: lixo.txt\n\n");
			printf("./main.exe lixo.txt               	adiciona linha\n");
			printf("./main.exe lixo.txt apagar   		apaga uma linha.\n");
			printf("rm /dev/shm/sem.sem_M4ex04_linhas     	remover semaforo1\n");
			printf("rm /dev/shm/sem.sem_M4ex04            	remover semaforo2\n");
			return 0;
	}

	//Vamos obter o nome do ficheiro.
	char nomeFicheiro[255];
	strcpy(nomeFicheiro, "./");
	strcat(nomeFicheiro, argv[1]);
	
	//Verificar se vamos apagar.
	if(argc > 2 ){
		if (strcmp( argv[2] , "apagar" ) == 0) apagar = 1 ;
		// strcmp compara os strings e se forem iguais retorna 0.
		// Se retornar 0, vamos colocar a variavel apagar a true, apagar == 1
	}

	pid = getpid();
	printf("[pid:%i]FILE: %s     Apagar: %s \n", pid, nomeFicheiro, apagar ? "true" : "false");

	
	
	
	// Cria o semaforo que isola o ficheiro, mas se existir nao cria, apenas abre.
	semaforo = sem_open(SEM_01, O_CREAT , 0644, 1); 
	if(semaforo == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR\n"); exit(EXIT_FAILURE);}

	//Abre o semaforo que controla o numero de linhas.
	semaforoLinhas = sem_open(SEM_LINHAS, O_CREAT , 0644, 10); 
	if(semaforoLinhas == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR\n"); exit(EXIT_FAILURE);}

	//ZONA CRITICA
	sem_wait(semaforo); //Pede permicao para entrar.

	int valorSemaforo = 0;
	int * ptr_valorSemaforo = & valorSemaforo; //Cricao de um apontador int para usar sem_getvalue

	error = sem_getvalue(semaforoLinhas, ptr_valorSemaforo);
	if(error != 0) perror("Erro a ler valor semaforo\n");

	printf("Numero de linhas livres indicado pelo semaforoLinhas= %i\n", valorSemaforo);	

	//Se o semLinhas indicar que ja tem 10 linhas e nao for para apagar, informamos e saimos.
	if(valorSemaforo <= 0 && !apagar){
		printf("Semaforo indica ficheiro cheio. A sair.\n");
		goto sair; // Salta todo o accesso ao ficheiro e para a zona de fechar os semaforos.
	}
	
	//Se nao for para apagar vamos escrever uma linha.
	if(!apagar){
			printf("Escrevendo linha\n");
			escreverLinha(nomeFicheiro ); //funcao, ver abaixo.
			sem_trywait(semaforoLinhas); 	//deve funcionar sempre.
	}
	
	if(apagar){
		printf("Apagando linha...\n");
		error = apagarLinha( nomeFicheiro );
		if(error == 0) 	{	//  Se nao existirem erros.
					if(valorSemaforo < 10) sem_post(semaforoLinhas); 
					// libertamos 1 se for inferior a 10 linhas livres.
				}
		
	}


	sair: //TAG de saida com fecho de semaforos dentro da zona critica.

	sem_post(semaforo); //Liberta vez.


	error = sem_close(semaforo);
	if(error != 0) perror("SEMAPHORE CLOSE ERROR\n");

	error = sem_close(semaforoLinhas);
	if(error != 0) perror("SEMAPHORE linhas CLOSE ERROR\n");
	
//	COMENTADO PORQUE NAO SABEMOS O NUMERO DE INSTANCIAS, PORTANTO DEIXAMOS O SEMAFORO.
//	error sclose = sem_unlink(SEM_LINHAS);	//apagar o semaforo 
//	if(error !=0) perror("SEMAPHORE UNLINK ERROR\n");

//	COMENTADO PORQUE NAO SABEMOS O NUMERO DE INSTANCIAS, PORTANTO DEIXAMOS O SEMAFORO.
//	error = sem_unlink(SEM_01);	//apagar o semaforo 
//	if(error !=0) perror("SEMAPHORE UNLINK ERROR\n");


	printf("PAI[pid:%i]: terminado.\n", getpid());

	return 0;
}


// FUNCAO QUE PERMITE ESCREVER UMA LINHA NO FICHEIRO.
void escreverLinha(char * nomeFicheiro){
	size_t pid;
	int error = 0;

	FILE *fp = fopen(nomeFicheiro , "a");  // Opcao "a" serve para escrever em append, ou seja adicionar ao fim.
	if(fp == NULL) {perror("Erro a abrir ficheiro");}

		//ESCEVE E DORME
		pid = getpid();
		fprintf(fp,"Eu sou o processo com o PID %i\n", pid);
		sleep(2);

	//Fecha ficheiro.
	error = fclose(fp);
	if(error != 0) perror("Erro a fechar ficheiro.");

}

//Tenta apagar uma linha do ficheiro, reconhecendo o /n
// retorna 0 se ok, 1 se erro.
int apagarLinha(char * nomeFicheiro){
	int erro = 0;	
	int i = 0;
	
	char textoFicheiro[5000] = { EOF }; //inicializa todas as posicoes a EOF .
	int posicao = 0;

	//ABRIMOS EM MODO DE LEITURA e copiamos para um vector de char.  
	FILE *fpmain = fopen(nomeFicheiro, "r");
	if(fpmain == NULL) {perror("Erro a abrir ficheiro"); return 1;}
	
	char c;
	do{
		c = getc(fpmain);
		textoFicheiro[posicao] = c;
		posicao++;
	 
	} while(c != EOF && posicao <4999);     // < 4999 para [4999] = EOF;
	
	erro = fclose(fpmain);
	if(erro != 0) {perror("FILE CLOSE ERROR\n"); return 1;}

	//Vamos encontrar a segunda linha no vector.
	//Percorendo o vector para tras, ate encontrar o primeiro \n. A nao existir, ja temos o valor zero.
	int segundaLinha = 0;
	for (i = 5000 -1; i>= 0; i--){
		if (textoFicheiro[i] == '\n') segundaLinha = i +1;
	}

	//Vamos agora escrever por cima abrindo em modo Write sem append,
	// e escrevemos o vector apartir da posicao do inicio da segunda linha.  
	fpmain = fopen(nomeFicheiro, "w");
	if(fpmain == NULL) {perror("Erro a abrir ficheiro"); return 1;}
	
	posicao = segundaLinha;
	do {
		c = textoFicheiro[posicao];
		posicao++;
		if(c != EOF) putc(c, fpmain);
	} while ( c != EOF);

	erro = fclose(fpmain);
	if(erro != 0) {perror("FILE CLOSE ERROR\n"); return 1;}


	return erro; // que deve ser zero.
}
