#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//Bibliotes de semaforos
#include <semaphore.h>

#define SEM_EN "semaforoEntrada"
#define SEM_SA "semaforoSaida"
#define NUM_VISITANTES 5
#define TEMPO_EXPOSICAO 10

//FUNCAO DE CRIAR SEMAFOROS 
sem_t * criarSemaforo( char nome[] , int valorInicial){
	sem_t * objecto = sem_open(nome, O_CREAT , 0644, valorInicial); //Inicializa a 10
	if( objecto == SEM_FAILED) {
		perror("SEMAPHORE CREATION ERROR\n");
		printf("TYPE: rm /dev/shm/sem.%s \n", nome);
		exit(EXIT_FAILURE);
	}
	return objecto;
}

//FUNCAO DE FECHAR SEMAFOROS
void fecharSemaforo( sem_t * semaforo){
	int error = sem_close(semaforo);
	if(error != 0) perror("SEMAPHORE CLOSE ERROR\n");
}

//FUNCAO PARA FAZER UNLINK A UM SEMAFORO 
void deslinkarSemaforo(char nome[] ){
	int error = sem_unlink( nome );
	if(error !=0) perror("SEMAPHORE UNLINK ERROR\n");
}

int main(int argc, char *argv[]){
	sem_t * semaforoEntrada = criarSemaforo(SEM_EN , NUM_VISITANTES);			// coordena o semaforo de entrada 
	sem_t * semaforoSaida 	= criarSemaforo(SEM_SA , NUM_VISITANTES);			// coordena o semaforo de saida
	
	printf("A esperar...\n");
	
	sem_wait(semaforoEntrada);
	
	printf("A visitar...\n");
	sleep(2);		//Para a pessoa nao sair de imediato da exposicao da sessao atual
	
	sem_wait(semaforoSaida);
	
	printf("A sair...\n");
	
	return 0;
}
