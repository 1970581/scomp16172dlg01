#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//Bibliotes de semaforos
#include <semaphore.h>

#define SEM_EN "semaforoEntrada"
#define SEM_SA "semaforoSaida"
#define NUM_VISITANTES 5
#define TEMPO_EXPOSICAO 10

//FUNCAO DE CRIAR SEMAFOROS 
sem_t * criarSemaforo( char nome[] , int valorInicial){
	sem_t * objecto = sem_open(nome, O_CREAT , 0644, valorInicial); //Inicializa a 10
	if( objecto == SEM_FAILED) {
		perror("SEMAPHORE CREATION ERROR\n");
		printf("TYPE: rm /dev/shm/sem.%s \n", nome);
		exit(EXIT_FAILURE);
	}
	return objecto;
}

//FUNCAO DE FECHAR SEMAFOROS
void fecharSemaforo( sem_t * semaforo){
	int error = sem_close(semaforo);
	if(error != 0) perror("SEMAPHORE CLOSE ERROR\n");
}

//FUNCAO PARA FAZER UNLINK A UM SEMAFORO 
void deslinkarSemaforo(char nome[] ){
	int error = sem_unlink( nome );
	if(error !=0) perror("SEMAPHORE UNLINK ERROR\n");
}

int main(int argc, char *argv[]){
	//Cria os semaforos.
	sem_t * semaforoEntrada = criarSemaforo(SEM_EN , 0);			// coordena o semaforo de entrada 
	sem_t * semaforoSaida 	= criarSemaforo(SEM_SA , 0);			// coordena o semaforo de saida
	int sessao = 1;
	int i;
	do{
		printf("Sessao: %d\n", sessao);
		
		//Aqui "as portas" de entrada e saida vao ser abertas
		for(i = 0; i < NUM_VISITANTES ; i++){
			sem_post(semaforoSaida);
		}
		for(i = 0; i < NUM_VISITANTES ; i++){
			sem_post(semaforoEntrada);
		}
		
		printf("Aberto\n");
		
		sleep(1); //Existe para que os visitantes tenham tempo de entrar para a sessao atual
				  //e os que vao sair da sessao anterior
		
		//Aqui "as portas" de entrada e saida vao ser fechadas (trywait para evitar deadlock)
		for(i = 0; i < NUM_VISITANTES ; i++){
			sem_trywait(semaforoEntrada);
		}
		for(i = 0; i < NUM_VISITANTES ; i++){
			sem_trywait(semaforoSaida);
		}
		
		printf("Fechado, exposicao a decorrer\n");
		
		sleep(TEMPO_EXPOSICAO);		//10 Segundos de exposicao
		
		printf("Fim da exposicao\n");
		
		sessao++;
	}while(sessao > 0);
	//Sessao > 0 : Infinito
	//Sessao < 5 : 5 sessoes de exposicao para fins de teste
	
	return 0;
}


