#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <semaphore.h>

#define SEM_01 "semaforo01"
#define SEM_02 "semaforo02"
#define SEM_03 "semaforo03"
#define SEM_04 "semaforo04"
#define SEM_05 "semaforo05"

#define FICHEIRO "./lixo.txt"

//ATENCAO: Este programa pode raramente dar um resultado incorreto
int main(int argc, char *argv[]){ 
	int error;		//Diagnostico
	size_t pid;
	
	sem_t * semaforo[5];
	
	int i = 0;
	int j = 0;	

	semaforo[0] = sem_open(SEM_01, O_CREAT | O_EXCL , 0644, 1); //Criar semaforo 1
	if(semaforo[0] == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	
	semaforo[1] = sem_open(SEM_02, O_CREAT | O_EXCL , 0644, 0); //Criar semaforo 2
	if(semaforo[1] == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	
	semaforo[2] = sem_open(SEM_03, O_CREAT | O_EXCL , 0644, 0); //Criar semaforo 3
	if(semaforo[2] == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	
	semaforo[3] = sem_open(SEM_04, O_CREAT | O_EXCL , 0644, 0); //Criar semaforo 4
	if(semaforo[3] == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	
	semaforo[4] = sem_open(SEM_05, O_CREAT | O_EXCL , 0644, 0); //Criar semaforo 5
	if(semaforo[4] == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}

	
	FILE *fp = fopen(FICHEIRO , "w");
	if(fp == NULL) perror("Erro a abrir ficheiro");
	

	//Ciclo de criacao de filhos.
	for(i = 0; i < 5; i++){
		pid = fork();
		if(pid < 0) {perror("Erro a fazer fork.\n"); exit(1);}
		else{printf("%s[%i] em concorrencia.\n", (pid > 0) ? "PAI  " : "Filho" , getpid() );}
		
		if (pid == 0) break;
	}

	//CODIGO FILHOS
	
	if(pid == 0){
		sem_wait(semaforo[i]); //Pede permissao para entrar.
				
		fprintf(fp,"Impressao filho: %i\n", i);
		for (j = 1; j <= 200; j++){
			fprintf(fp, "%d ",j); 
		}
		fprintf(fp,"\n");
		
		if(i < 4){		
			sem_post(semaforo[i+1]); //Liberta vez.
		}
		exit(1);
	}
	

	//PAI	
	for(i = 0 ; i < 5; i++){
		wait(NULL);
	}

	sem_unlink(SEM_01);	//apagar o semaforo 1
	sem_unlink(SEM_02);	//apagar o semaforo 2
	sem_unlink(SEM_03);	//apagar o semaforo 3
	sem_unlink(SEM_04);	//apagar o semaforo 4
	sem_unlink(SEM_05);	//apagar o semaforo 5

	error = fclose(fp);
	if(error != 0) perror("Erro a fechar ficheiro.");

	//IMPRIMIR O FICHEIRO 	

	printf("\nA Imprimir ficheiro:\n\n");

	fp = fopen(FICHEIRO , "r");
	if(fp == NULL) perror("Erro a abrir ficheiro");
	
	char c;
	do {
		c = fgetc(fp);
		if(c != EOF) printf("%c",c);  // Obtermos um carater e imprimimos enquanto nao for o <<EOF>>
	} while (c != EOF);
		
	error = fclose(fp);
	if(error != 0) perror("Erro a fechar ficheiro.");


	printf("PAI: terminado.\n");

	return 0;
}


