#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <semaphore.h>

#define SEM_01 "semaforo01"
#define FICHEIRO "./lixo.txt"


int main(int argc, char *argv[]){ 
	int error;		//Diagnostico dos processos de partilha de memoria
	size_t pid;
	sem_t 	 * semaforo;
	int i = 0;
	int j = 0;	

	semaforo = sem_open(SEM_01, O_CREAT | O_EXCL , 0644, 1); //Criar semaforo 
	if(semaforo == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}

	FILE *fp = fopen(FICHEIRO , "w");
	if(fp == NULL) perror("Erro a abrir ficheiro");
	

	//Ciclo de criacao de filhos.
	for(i=1; i <= 5; i++){
		pid = fork();
		if(pid < 0) {perror("Erro a fazer fork.\n"); exit(1);}
		else{printf("%s[%i] em concorrencia.\n", (pid > 0) ? "PAI  " : "Filho" , getpid() );}
		
		if (pid == 0) break;
	}

	//CODIGO FILHOS
	if(pid == 0){

		sem_wait(semaforo); //Pede permicao para entrar.
		

		fprintf(fp,"Impressao filho: %i\n", i);
		for (j = 1; j <= 200; j++){
			//printf("[%i] entrei semaforo\n",i);
			fprintf(fp, "%d ",j); 
		}
		fprintf(fp,"\n");		

		sem_post(semaforo); //Liberta vez.

		exit(1);
	}
	



	//PAI
	
	for(i = 1 ; i <= 5; i++){
		wait(NULL);
	}

	int sclose = sem_unlink(SEM_01);	//apagar o semaforo 
	if(sclose !=0) perror("SEMAPHORE CLOSE ERROR\n");

	error = fclose(fp);
	if(error != 0) perror("Erro a fechar ficheiro.");

	//IMPRIMIR O FICHEIRO 	

	printf("\nA Imprimir ficheiro:\n\n");

	fp = fopen(FICHEIRO , "r");
	if(fp == NULL) perror("Erro a abrir ficheiro");
	
	char c;
	do {
		c = fgetc(fp);
		if(c != EOF) printf("%c",c);  // Obtermos um carater e imprimimos enquanto nao for o <<EOF>>
	} while (c != EOF);
		
	error = fclose(fp);
	if(error != 0) perror("Erro a fechar ficheiro.");


	printf("PAI: terminado.\n");

	return 0;
}


