#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <semaphore.h>

#define SEM_01 "semaforo01"
#define SEM_02 "semaforo02"

int main(int argc, char *argv[]){ 
	size_t pid;
	sem_t * semaforoPai;
	sem_t * semaforoFilho;
	int i;

	semaforoPai = sem_open(SEM_01, O_CREAT | O_EXCL , 0644, 1); //Criar semaforo 
	if(semaforoPai == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	
	semaforoFilho = sem_open(SEM_02, O_CREAT | O_EXCL , 0644, 0); //Criar semaforo 
	if(semaforoFilho == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}

	//Criacao do filho
	pid = fork();
	if(pid < 0) {
		perror("Erro a fazer fork.\n"); 
		exit(1);
	} else {
		printf("%s[%i] em concorrencia.\n", (pid > 0) ? "PAI  " : "Filho" , getpid() );
	}	
	

	//CODIGO FILHO
	if(pid == 0){
		for(i = 0; i < 10; i++){
			sem_wait(semaforoFilho); 	//Pede permissao para entrar.
			printf("Eu sou o filho\n");
			sem_post(semaforoPai); 		//Liberta vez.
		}		
		exit(1);
	}
	
	//PAI
	for(i = 0; i < 10; i++){
		sem_wait(semaforoPai);
		printf("Eu sou o pai\n");
		sem_post(semaforoFilho); 		//Liberta vez.
	}
	
	wait(NULL);				//O pai espera que o filho termine

	int sclose = sem_unlink(SEM_01);	//apagar o semaforo 
	if(sclose !=0) perror("SEMAPHORE CLOSE ERROR\n");
	
	int sclose2 = sem_unlink(SEM_02);	//apagar o semaforo 
	if(sclose2 !=0) perror("SEMAPHORE CLOSE ERROR\n");
	
	printf("PAI: terminado.\n");

	return 0;
}


