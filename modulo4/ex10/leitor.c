#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <semaphore.h>

#define SEM_LE "semaforoLeitor"
#define SEM_ES "semaforoEscritor"
#define SEM_MULE "semaforoMutexLeitor"
#define SEM_MUES "semaforoMutexEscritor"

typedef struct {
	char buffer[26];
	int numberPid;
	int nr_leitores;
	int nr_escritores;		
} shared_data_type;

int main(int argc, char *argv[]){
	int fd;			//Descritor da shared memory 
	int error;		//Diagnostico dos processos de partilha de memoria
	int data_size = sizeof(shared_data_type);	//Tamanho da estrutura definida
	shared_data_type *shared_data;				//Apontador de uma estrutura
	sem_t * semaforoLeitor;
	sem_t * semaforoEscritor;
	sem_t * semaforoMutexLeitor;
	sem_t * semaforoMutexEscritor;
	
	//SEMAFORO - START 	
	semaforoLeitor = sem_open(SEM_LE, O_CREAT , 0644, 1); // Abrir/criar semaforo 
	if(semaforoLeitor == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	
	semaforoEscritor = sem_open(SEM_ES, O_CREAT , 0644, 1); // Abrir/criar semaforo 
	if(semaforoEscritor == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	
	semaforoMutexLeitor = sem_open(SEM_MULE, O_CREAT , 0644, 1); // Abrir/criar semaforo 
	if(semaforoMutexLeitor == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	
	semaforoMutexEscritor = sem_open(SEM_MUES, O_CREAT , 0644, 1); // Abrir/criar semaforo 
	if(semaforoMutexEscritor == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	
	//PARTILHA DE MEMORIA - START
	fd = shm_open("/shmtest", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);	// Abrir/criar ficheiro de memoria partilhada
	if(fd == -1){perror("SHM OPEN RETURNED -1"); exit(0);}
	
	error = ftruncate(fd, data_size);			// Indica o tamanho da zona de memoria					
	if(error == -1){perror("FTRUNCATE RETURNED -1"); exit(1);}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	//CODIGO
	sem_wait(semaforoMutexLeitor);
		
		if(shared_data->nr_leitores == 0){ //Os leitores tem prioridade em relacao aos escritores 
			sem_wait(semaforoEscritor);		//Enquanto existir um processor a querer ler, nenhum processo pode escrever
		}
		shared_data->nr_leitores++;
	sem_post(semaforoMutexLeitor);
	
	//LE OS DADOS
	printf("LEITOR: (Mensagem: \"%s PID: %d \" Nleitores: %d\n", shared_data->buffer, shared_data->numberPid, shared_data->nr_leitores);
		
	sem_wait(semaforoMutexLeitor);
		shared_data->nr_leitores--;
		if(shared_data->nr_leitores == 0){	//Quando nao houver mais leitores
			sem_post(semaforoEscritor);		//Desbloqueia o escritor
		}
	sem_post(semaforoMutexLeitor);
	
	//SEMAFORO - FINISH
    error = sem_close(semaforoLeitor);
	if(error != 0) perror("SEMAPHORE leitor CLOSE ERROR\n");
	error = sem_close(semaforoEscritor);
	if(error != 0) perror("SEMAPHORE escritor CLOSE ERROR\n");
	error = sem_close(semaforoMutexLeitor);
	if(error != 0) perror("SEMAPHORE mutex leitor CLOSE ERROR\n");
	error = sem_close(semaforoMutexEscritor);
	if(error != 0) perror("SEMAPHORE mutex escritor CLOSE ERROR\n");
    
    
    //PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Desmapea
	if(error == -1){perror("MUNMAP RETURNED -1");}
	
	error = close(fd);						// Fecha o descritor
	if(error == -1){perror("CLOSE RETURNED -1");}
	
	return 0;
}


