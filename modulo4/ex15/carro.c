#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//#include <time.h>  		// usada pelo random.

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//Bibliotes de semaforos
#include <semaphore.h>

#define SEM_1 "sem_M4ex15_mutexEste"
#define SEM_2 "sem_M4ex15_mutexOeste"
#define SEM_3 "sem_M4ex15_inverterSentido"



//*******************************************
//  IMPORTANTE!!!!!
//********
// Escreva carro.exe este   ou carro.exe oeste para enviar carros nessa direccao.
// make clean  para remover os semaforos e memoria partilhada.



//Declaracao de funcoes:
sem_t * criarSemaforo( char nome[] , int valorInicial);
void fecharSemaforo( sem_t * semaforo);
void deslinkarSemaforo(char nome[] );

typedef struct {  // Zona de memoria partilhada
	int numeroCarrosEste;		// numero de carros na ponte sentido Este.
	int numeroCarrosOeste;		// numero de carros na ponte sentido Oeste
} shared_data_type;


//  <<< MAIN >>>
int main(int argc, char *argv[]) { 
	int error;		//Diagnostico dos processos de partilha de memoria
	size_t pid ;
//	int i = 0;

	//PARAMETROS DO EXECUTAVEL
	if(argc != 2){
		printf("Escreva carro.exe este   ou carro.exe oeste para enviar carros nessa direccao. \nSaindo.\n");
		exit(0);
	}
	int este = 1;
	int oeste = 1;					// strcmp() compara strings e devolve 0 se iguais.
	este 	= strcmp (argv[1] , "este");		//Se argumento = "este"  a funcao devolve 0,  este = 0;
	oeste 	= strcmp (argv[1] , "oeste");		//Se argumento = "oeste" a funcao devolve 0, oeste = 0;
	if( este != 0 && oeste != 0){
		printf("Escreva carro.exe este   ou carro.exe oeste para enviar carros nessa direccao. \nSaindo.\n");
		exit(0);
	}


	//Cria os semaforos.
	sem_t * sem_mutexEste 		= criarSemaforo( SEM_1 , 1);			// coordena o acesso a variavel numeroCarrosEste 
	sem_t * sem_mutexOeste		= criarSemaforo(SEM_2 , 1);			// coordena o acesso a variavel numeroCarrosOeste
	sem_t * sem_inverterSentido 	= criarSemaforo(SEM_3 , 1);			// permite libertar a inversao de sentido.




	//Memoria partilhada.
	int fd;						//Descritor da shared memory 
	int data_size = sizeof(shared_data_type);	//Tamanho da estrutura definida
	shared_data_type *shared_data;			//Apontador de uma estrutura de memoria partilhada.

	//Criar/Open ficheiro de memoria partilhada. 
	fd = shm_open("/shmtest", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);	// Criar ficheiro de memoria partilhada 
	if(fd == -1){perror("SHM OPEN RETURNED -1 \nTYPE: rm /dev/shm/shmtest\n "); return 0;}		
	
	error = ftruncate(fd, data_size);			// Indica o tamanho da zona de memoria					
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	//Inicializar memoria partilhada. 
//	shared_data->numeroCarrosEste  = 0;	  Desnecessario, inicializa tudo a 0.	
//	shared_data->numeroCarrosOeste = 0; 
//		http://man7.org/linux/man-pages/man3/shm_open.3.html
//		A new shared memory object initially has zero length—the
//	         size of the object can be set using ftruncate(2).  The
//       	 newly allocated bytes of a shared memory object are
//                 automatically initialized to 0.

	pid = getpid();
//	printf("Carro[pid:%i] a correr. \n", getpid());


//================

	// CARRO VAI PARA ESTE.
	if(este == 0){
		
		sem_wait(sem_mutexEste);				
			
			int nCarrosEste = shared_data->numeroCarrosEste;
			shared_data->numeroCarrosEste++ ;
			
			if(nCarrosEste == 0) sem_wait(sem_inverterSentido);		// Se for o primeiro carro nesta direcao, pede o direito de inverterSentido.
											// Se ouver carros no sentido inverso, aguarda. Senao, limita-o.

			printf("CARRO[%i] Vou para Este .  nEste = %i na ponte (contando comigo).\n",pid , shared_data->numeroCarrosEste);

		sem_post(sem_mutexEste);

			//Atravessa a ponte.
			sleep(1);
		
		sem_wait(sem_mutexEste);

			shared_data->numeroCarrosEste--;
			nCarrosEste = shared_data->numeroCarrosEste;
			printf("CARRO[%i] Chegei a Este .  nEste = %i na ponte (ja nao estou na ponte).\n", pid,shared_data->numeroCarrosEste);

			if(nCarrosEste == 0) sem_post(sem_inverterSentido);


		sem_post(sem_mutexEste);
			
	}

	// CARRO VAI PARA OESTE.
	if(oeste == 0){
		
		sem_wait(sem_mutexOeste);				
			
			int nCarrosOeste = shared_data->numeroCarrosOeste;
			shared_data->numeroCarrosOeste++ ;
			
			if(nCarrosOeste == 0) sem_wait(sem_inverterSentido);		// Se for o primeiro carro nesta direcao, pede o direito de inverterSentido.
											// Se ouver carros no sentido inverso, aguarda. Senao, limita-o.

			printf("CARRO[%i] Vou para Oeste. nOeste = %i na ponte (contando comigo).\n", pid ,shared_data->numeroCarrosOeste);
	
		sem_post(sem_mutexOeste);

			//Atravessa a ponte.
			sleep(1);
		
		sem_wait(sem_mutexOeste);

			shared_data->numeroCarrosOeste--;
			nCarrosOeste = shared_data->numeroCarrosOeste;
			printf("CARRO[%i] Chegei a Oeste. nOeste = %i na ponte (ja nao estou na ponte).\n", pid, shared_data->numeroCarrosOeste);

			if(nCarrosOeste == 0) sem_post(sem_inverterSentido);


		sem_post(sem_mutexOeste);
			
	}



	
//==========
	
	//finalizar: 
	
	fecharSemaforo(sem_mutexEste);
	fecharSemaforo(sem_mutexOeste);
	fecharSemaforo(sem_inverterSentido);

//Ninguem deslinka os semaforos.
//	deslinkarSemaforo(SEM_1);
//	deslinkarSemaforo(SEM_2);
//	deslinkarSemaforo(SEM_3);
//	deslinkarSemaforo(SEM_4);
//	deslinkarSemaforo(SEM_5);


	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Desmapea
	if(error == -1){perror("SERVER MUNMAP RETURNED -1");}
	
	error = close(fd);						// Fecha o descritor
	if(error == -1){perror("SERVER CLOSE RETURNED -1");}
	
//Ninguem faz unlink da memoria partilhada.
//	error = shm_unlink("/shmtest");					// Fecha o ficheiro
//	if(error == -1) {perror("SERVER UNLINK RETURNED -1\n");}	

//	printf("CARRO[%i]: terminado.\n", getpid());

	return 0;
}


//FUNCAO DE CRIAR SEMAFOROS 
sem_t * criarSemaforo( char nome[] , int valorInicial){
	sem_t * objecto = sem_open(nome, O_CREAT , 0644, valorInicial); //Inicializa a 10
	if( objecto == SEM_FAILED) {
		perror("SEMAPHORE CREATION ERROR\n");
		printf("TYPE: rm /dev/shm/sem.%s \n", nome);
		exit(EXIT_FAILURE);
	}
	return objecto;
}

//FUNCAO DE FECHAR SEMAFOROS
void fecharSemaforo( sem_t * semaforo){
	int error = sem_close(semaforo);
	if(error != 0) perror("SEMAPHORE CLOSE ERROR\n");
}

//FUNCAO PARA FAZER UNLINK A UM SEMAFORO 
void deslinkarSemaforo(char nome[] ){
	int error = sem_unlink( nome );
	if(error !=0) perror("SEMAPHORE UNLINK ERROR\n");
}



