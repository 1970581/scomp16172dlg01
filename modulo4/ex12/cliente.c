#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

#include <time.h>  		// usada pelo random.

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//Bibliotes de semaforos
#include <semaphore.h>

#define SEM_1 "sem_M4ex12_vez"
#define SEM_2 "sem_M4ex12_balcao"
#define SEM_3 "sem_M4ex12_chamarCliente"
#define SEM_4 "sem_M4ex12_clienteMoveBalcao"
#define SEM_5 "sem_M4ex12_atendido"
#define MAX_BUFFER 10

//*******************************************
//  IMPORTANTE!!!!!
//  Para cada servidor correr um cliente, para nao ficar com processos encravados. 
//  >ps    para ver quais os processos.
//  Se estiverem mais de 3 servidores online, os outros esperam vez de balcao antes de avancarem. Max 3 balcoes.		
//  O random, se for corrido no mesmo segundo da gralha, pk a seed é baseada no segundo.
//********


//Declaracao do funcao random.
int random_number(int max, int seed_multiplier);

//FUNCAO DE CRIAR SEMAFOROS 
sem_t * criarSemaforo( char nome[] , int valorInicial){
	sem_t * objecto = sem_open(nome, O_CREAT , 0644, valorInicial); //Inicializa a 10
	if( objecto == SEM_FAILED) {
		perror("SEMAPHORE CREATION ERROR\n");
		printf("TYPE: rm /dev/shm/sem.%s \n", nome);
		exit(EXIT_FAILURE);
	}
	return objecto;
}

//FUNCAO DE FECHAR SEMAFOROS
void fecharSemaforo( sem_t * semaforo){
	int error = sem_close(semaforo);
	if(error != 0) perror("SEMAPHORE CLOSE ERROR\n");
}

//FUNCAO PARA FAZER UNLINK A UM SEMAFORO 
void deslinkarSemaforo(char nome[] ){
	int error = sem_unlink( nome );
	if(error !=0) perror("SEMAPHORE UNLINK ERROR\n");
}

int main(int argc, char *argv[]){ 
//	int error;		//Diagnostico dos processos de partilha de memoria
//	size_t pid ;
//	int i = 0;


	//Cria os semaforos.
	sem_t * sem_vez 		= criarSemaforo( SEM_1 , 10);			// coordena os max de 10 em espera 
	sem_t * sem_balcao 		= criarSemaforo(SEM_2 , 3);			// coordena a restricao de 3 balcoes (3 max Servers)
	sem_t * sem_chamarCliente 	= criarSemaforo(SEM_3 , 0);			// chamar um cliente para o balcao
	sem_t * sem_clienteMoveBalcao 	= criarSemaforo(SEM_4 , 0);			// esperar que o cliente chege ao balcao	
	sem_t * sem_atendido 		= criarSemaforo(SEM_5 , 0);			// atender/prossesar o cliente

	printf("Cliente[pid:%i] a correr. \n", getpid());


//================

	
	//CLIENTE

	int espera = sem_trywait(sem_vez);						//Verifica se tem vez (existem 10)
	if (espera != 0){								// 0 se faz wait, -1 se passa sem esperar.
		printf("Cliente[%i]: Fila cheia. Vou embora.\n", getpid() );		// Se esta cheio (nao espera) vamos embora (jump para finalizar);
		goto finalizar;								
	}

	sem_wait(sem_chamarCliente);							// Espera que o chamem. (Tem vez)

	printf("Cliente[%i] fui para o balcao.\n", getpid() );
	sem_post(sem_clienteMoveBalcao);						// Vamos para o balcao para sermos atendidos processados.

	sem_wait(sem_atendido);								// Estou no balcao a espera que termine o atendimento.
	printf("Cliente[%i] fui atendido.\n", getpid() );
	
	sem_post(sem_vez);								// Vou embora, liberto a vez na fila.

	
//==========
	
	finalizar: 
	
	fecharSemaforo(sem_vez);
	fecharSemaforo(sem_balcao);
	fecharSemaforo(sem_chamarCliente);
	fecharSemaforo(sem_clienteMoveBalcao);
	fecharSemaforo(sem_atendido);

// Ninguem deslinka os semaforos.
//	deslinkarSemaforo(SEM_1);
//	deslinkarSemaforo(SEM_2);
//	deslinkarSemaforo(SEM_3);
//	deslinkarSemaforo(SEM_4);
//	deslinkarSemaforo(SEM_5);

	

//	printf("Cliente[%i]: terminado.\n", getpid());

	return 0;
}


/*
*	FUNCAO DE RANDOM
*/
int random_number(int max, int seed_multiplier){
	int valor_saida = -1;
	int max_number = max;
	//int minimum_number = 1;
	if(seed_multiplier == 0) seed_multiplier = 1;
	
	// Vamos alterar a seed to rand() com recurso a uma funcao que devolve o tempo actual.
	// Isto previne rand() de dar sempre o mesmo valor de saida.
	srand(time(NULL)*seed_multiplier);
	
	//Chamamos o rand() para obter um numero random de 0 a RAND_MAX 
	int numero_random = rand();
	
	// Dividimos o valor obtido pelo RAND_MAX para obtermos um float de 0 até 1.
	// Como em Java.
	float aux = (float) numero_random / RAND_MAX;

	// Multiplicamos por maior valor, mais 1 para obter numeros de 1 a 5.
	valor_saida = aux * (max_number) +1 ;


	//Devolvemos o valor.
	return(valor_saida);
}
