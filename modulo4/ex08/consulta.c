#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//Bibliotes de semaforos
#include <semaphore.h>

#define SEM_AT_CL "sem_M4ex08_atenderCliente"
#define SEM_CH_SE "sem_M4ex08_chamarServidor"
#define SEM_CL_LE "sem_M4ex08_clienteLer"
#define MAX_CONTA 10

//*******************************************
//********
// APAGAR SEMAFOROS: 
// rm /dev/shm/sem.sem_M4ex08_atenderCliente 
// rm /dev/shm/sem.sem_M4ex08_chamarServidor 
// APAGAR MEMORIA PARTILHADA:
// rm rm /dev/dhm/shmtest

typedef struct {  // Zona de memoria usada para transferir informacao do cliente->servidor e servidor-> cliente.
	int operacao;  		//tipo de operacao 0 - saldo , 1 - levanta 
	int valor;		// valor de saldo devolvido pelo Servidor.
	int conta;	// numero de conta do cliente.
	int sair;		// Escrito por um cliente a informar o servidor para terminar. 1 sair.
} shared_data_type;


int main(int argc, char *argv[]){ 
	int error;		//Diagnostico dos processos de partilha de memoria
//	size_t pid ;
	int indicarSaida = 0;
	int sair = 0;
	int conta = 0;
	int operacao = 0;
	int valor = 0;

	int fd;						//Descritor da shared memory 
	int data_size = sizeof(shared_data_type);	//Tamanho da estrutura definida
	shared_data_type *shared_data;				//Apontador de uma estrutura de memoria partilhada.

	sem_t * atenderCliente;		// Semaforo que indica ao cliente que o servidor esta livre para o atender.
	sem_t * chamarServidor;		// Semaforo que indica ao servidor que tem um pedido.
	sem_t * clienteLer;		// Semaforo que permite a um cliente ler.

	//Verificar se temos parametros, e quais sao eles.
	if(argc != 2) {printf("consulta.exe sair   para encerrar o servidor \n consulta.exe 1    para consultar a conta 1\n"); return 0;}
	int comp = strcmp (argv[1] , "sair");
	if (comp == 0) indicarSaida = 1;
	else conta = atoi(argv[1]);
	
	printf("Consulta[pid:%i] iniciada.", getpid());

	//ABRIR SEMAFOROS. NAO CRIA!!!!!!!!!!!!!!!
	// Abreo semaforo atenderCliente. 
	atenderCliente = sem_open(SEM_AT_CL, O_EXCL , 0644, 0); 
	if(atenderCliente == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR\n Garanta que o servidor esta a correr em paralelo. \n"); exit(EXIT_FAILURE);}

	// Abre o chamarServidor. 
	chamarServidor = sem_open(SEM_CH_SE, O_EXCL , 0644, 0); 
	if(chamarServidor == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR\n Garanta que o servidor esta a correr em paralelo.  \n"); exit(EXIT_FAILURE);}

	// Abre o clienteLer. 
	clienteLer = sem_open(SEM_CL_LE, O_EXCL , 0644, 0); 
	if(clienteLer == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR\n Garanta que o servidor esta a correr em paralelo.  \n"); exit(EXIT_FAILURE);}
	

	
	//Abre o ficheiro de memoria partilhada. 
//	fd = shm_open("/shmtest", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);	// Criar ficheiro de memoria partilhada se ja existir aborta!!!
	fd = shm_open("/shmtest", O_RDWR, S_IRUSR | S_IWUSR);	// Vamos tentar apenas abrir sem criar.
	if(fd == -1){perror("SHM OPEN RETURNED -1 \nTYPE: rm /dev/dhm/shmtest\n "); return 0;}		
	
	error = ftruncate(fd, data_size);			// Indica o tamanho da zona de memoria					
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

//===========================


	printf(" Consulta [pid:%i]  a correr.\n", getpid());

	sem_wait(atenderCliente);  	// [000] aguarda que o servidor o atenda.


	sair = shared_data->sair;    	//Verificar se o servidor tem indicacao de sair. Se tiver, ele esta a fechar. Portanto vamos abortar elegantemente.
	if (sair != 0){
		sem_post(atenderCliente);	// Libertar a vez para outros sairem.
		printf("Consulta: Servidor ja nao aceita pedidos. Terminando.\n");	//Informar utilizador.

		goto finalizar;			//Vamos fechar semaforos e memoria.
	}


	if (indicarSaida == 1){		//Vamos indicar ao servidor que e para sair. Nao fazemso consulta. 
		shared_data->sair = indicarSaida;
		printf("Consulta: indicamos ao servidor para terminar.\n");		//Informar utilizador.
		sem_post(chamarServidor);	// [000] Chamamos o servidor para ele ler.

		goto finalizar;			//Fechamos.
	}


	//Se nao for para sair, nem indicarSaida
	{	
		//mensagem -> servidor.
		operacao = 0;				// Consultar saldo.
		shared_data->conta = conta;		//Indicamos conta 
		shared_data->operacao = operacao;	//Indicamos operacao consulta saldo.

		sem_post(chamarServidor);	// [000] Chamamos o servidor para ele ler.
		sem_wait(clienteLer);		// [000] Aguardamos que o servidor nos de vez de leitura.

		//resposta do servidor 
		conta = shared_data->conta;
		valor = shared_data->valor;
		printf("Consulta: O saldo da conta[%i]= %i .\n", conta, valor); // Informar cliente.
		
		sem_post(atenderCliente);	// [000] Passamos a vez a outro cliente.
	}


//=========================

	finalizar:
	
	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Desmapea
	if(error == -1){perror("MUNMAP RETURNED -1");}
	
	error = close(fd);						// Fecha o descritor
	if(error == -1){perror("CLOSE RETURNED -1");}

// 	NAO FAZEMOS UNLINK DA MEMORIA PARTILHADA, O SERVIDOR E QUE FAZ.	
//	error = shm_unlink("/shmtest");					// Fecha o ficheiro , senão nao cria na proxima corrida.
//	if(error == -1) {perror("SERVER UNLINK RETURNED -1\n");}

	
	//Fechar os 3 semaforos.
	error = sem_close(clienteLer);
	if(error != 0) perror("SEMAPHORE linhas CLOSE ERROR\n");
	error = sem_close(chamarServidor);
	if(error != 0) perror("SEMAPHORE linhas CLOSE ERROR\n");
	error = sem_close(atenderCliente);
	if(error != 0) perror("SEMAPHORE linhas CLOSE ERROR\n");

// NAO FAZEMOS UNLINK DOS SEMAFOROS. O SERVIDOR E QUE FAZ.
//	// Fazer unlink dos 3 semaforos. So efectiva quando todos fizerem close. http://pubs.opengroup.org/onlinepubs/7908799/xsh/sem_unlink.html
//	error = sem_unlink(SEM_CL_LE );
//	if(error !=0) perror("server SEMAPHORE UNLINK ERROR\n");
//	error = sem_unlink(SEM_CH_SE );
//	if(error !=0) perror("server SEMAPHORE UNLINK ERROR\n");
//	error = sem_unlink(SEM_AT_CL );
//	if(error !=0) perror("server SEMAPHORE UNLINK ERROR\n");

	

	printf("CONSULTA[pid:%i]: terminado.\n", getpid());

	return 0;
}
