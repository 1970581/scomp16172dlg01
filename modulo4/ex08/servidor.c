#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//Bibliotes de semaforos
#include <semaphore.h>

#define SEM_AT_CL "sem_M4ex08_atenderCliente"
#define SEM_CH_SE "sem_M4ex08_chamarServidor"
#define SEM_CL_LE "sem_M4ex08_clienteLer"
#define MAX_CONTA 10

//*******************************************
//	printf("O servidor tem de estar a correr para o consulta e levanta funcionar.\n");
//	printf("PF use 2 terminais ou \"./servidor.exe &\" para o correr em background\n");
//	printf("O servidor so termina com o comando: ./consulta.exe sair \n");
//	printf("O servidor so corre se os semaforos e memoria partilhada NAO EXISTIREM\n"); 
//
//********
// APAGAR SEMAFOROS: 
// rm /dev/shm/sem.sem_M4ex08_atenderCliente 
// rm /dev/shm/sem.sem_M4ex08_chamarServidor 
// APAGAR MEMORIA PARTILHADA:
// rm rm /dev/dhm/shmtest

typedef struct {  // Zona de memoria usada para transferir informacao do cliente->servidor e servidor-> cliente.
	int operacao;  		//tipo de operacao 0 - saldo , 1 - levanta 
	int valor;		// valor de saldo devolvido pelo Servidor.
	int conta;	// numero de conta do cliente.
	int sair;		// Escrito por um cliente a informar o servidor para terminar. 1 sair.
} shared_data_type;


int main(int argc, char *argv[]){ 
	int error;		//Diagnostico dos processos de partilha de memoria
//	size_t pid ;
	int i = 0;

	int fd;						//Descritor da shared memory 
	int data_size = sizeof(shared_data_type);	//Tamanho da estrutura definida
	shared_data_type *shared_data;				//Apontador de uma estrutura de memoria partilhada.

	sem_t * atenderCliente;		// Semaforo que indica ao cliente que o servidor esta livre para o atender.
	sem_t * chamarServidor;		// Semaforo que indica ao servidor que tem um pedido.
	sem_t * clienteLer;		// Semaforo que permite a um cliente ler.

	//VALOR NAS CONTAS.
	int saldoConta[MAX_CONTA]; //Inicializa o vector das contas todas com um certo saldo.
	for (i = 0; i < MAX_CONTA; i++) saldoConta[i] = 1000;
	
	printf("SERVIDOR [pid:%i] A INICIALIZAR ... ", getpid());

	//INFO:
	printf("O servidor tem de estar a correr para o consulta e levanta funcionar.\n");
	printf("PF use 2 terminais ou \"./servidor.exe &\" para o correr em background\n");
	printf("O servidor so termina com o comando: ./consulta.exe sair \n");
	printf("O servidor so corre se os semaforos e memoria partilhada NAO EXISTIREM\n");
	printf("AVISO: Se nao terminares o servidor com ./consulta.exe sair  vais ter de apagar semaforos/memoria a mao quando o quiseres reiniciar\n" );
	printf("ps. Make clean remove os semaforos.\n");


	// Cria o semaforo atenderCliente. Se ja existir da erro e avisa.
	atenderCliente = sem_open(SEM_AT_CL, O_CREAT | O_EXCL , 0644, 0); 
	if(atenderCliente == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR\n TYPE: rm /dev/shm/sem.sem_M4ex08_atenderCliente \n"); exit(EXIT_FAILURE);}

	// Cria o chamarServidor. Se ja existir da erro e avisa.
	chamarServidor = sem_open(SEM_CH_SE, O_CREAT | O_EXCL , 0644, 0); 
	if(chamarServidor == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR\n TYPE: rm /dev/shm/sem.sem_M4ex08_chamarServidor \n"); exit(EXIT_FAILURE);}

	// Cria o clienteLer. Se ja existir da erro e avisa.
	clienteLer = sem_open(SEM_CL_LE, O_CREAT | O_EXCL , 0644, 0); 
	if(clienteLer == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR\n TYPE: rm /dev/shm/sem.sem_M4ex08_clienteLer \n"); exit(EXIT_FAILURE);}
	

	
	//Criar ficheiro de memoria partilhada. Se ja existir com o mesmo nome aborta.
	fd = shm_open("/shmtest", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);	// Criar ficheiro de memoria partilhada se ja existir aborta!!!
	if(fd == -1){perror("SHM OPEN RETURNED -1 \nTYPE: rm /dev/dhm/shmtest\n "); return 0;}		
	
	error = ftruncate(fd, data_size);			// Indica o tamanho da zona de memoria					
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	//Inicializar memoria partilhada. 	
	shared_data->operacao = 0;	  	
	shared_data->conta = 0; 
	shared_data->sair = 0;
	shared_data->valor = 0;

	printf(" ... SERVIDOR [pid:%i]  INICIALIZADO.\n", getpid());
	
	int sair = 0;
	int conta = 0;
	int operacao = 0;
	int valor = 0;
	int saldo = 0;

//=============

	sem_post(atenderCliente);	// [000] Abre vez para O PRIMEIRO cliente. Depois a vez e passada entre os proprios clientes.

	
	while (sair == 0){	
	
		sem_wait(chamarServidor);	// [000] Aguarda que um cliente lhe passe a vez.
		
		//Ganhou vez.
		sair = shared_data->sair ; 	//Actualiza se deve sair.
		conta = shared_data->conta ;
		operacao = shared_data->operacao ;
		valor = shared_data->valor ;
		
		//Verificacao se SAIR.
		if(sair != 0) {	
				printf("SERVIDOR[pid:%i] indicacao para sair.\n", getpid());
				sem_post(atenderCliente);	// [000] Passamos libertamos o proximo cliente, para que ele leia o sair e termine			
				break;
		}
		
		printf("SERVIDOR: Processando pedido. [sleep(2)]\n");
		sleep(2); //DEBUG	

		//Tratar pedido 
		if(conta < MAX_CONTA && conta >= 0){	//Se o numero de conta for valido.
			//Consultar saldo 
			if(operacao == 0) {
					saldo = saldoConta[conta];
					//printf("Saldo = %i\n",saldo);
					shared_data->valor = saldo;
			}
			//Levantar dinheiro do saldo 
			if(operacao == 1){	
				if(saldoConta[conta] >= valor){  //se existir saldo, removemos e devolvemos 1 no valor.
					saldoConta[conta] = saldoConta[conta] - valor;
					shared_data->valor = 1;
				}
				else {				// se nao existir salod, informamos 0 no valor.
					shared_data->valor = 0;
				}
			}
		}
		else {printf("SERVIDOR[pid:%i] Conta inexistente.\n",getpid( ));}
		
		sem_post(clienteLer);		// [000] Abre para o cliente ler.

	}
	
//==========
	
	//finalizar: 


	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Desmapea
	if(error == -1){perror("SERVER MUNMAP RETURNED -1");}
	
	error = close(fd);						// Fecha o descritor
	if(error == -1){perror("SERVER CLOSE RETURNED -1");}
	
	error = shm_unlink("/shmtest");					// Fecha o ficheiro , senão nao cria na proxima corrida.
	if(error == -1) {perror("SERVER UNLINK RETURNED -1\n");}

	
	//Fechar os 3 semaforos.
	error = sem_close(clienteLer);
	if(error != 0) perror("SERVER SEMAPHORE linhas CLOSE ERROR\n");
	error = sem_close(chamarServidor);
	if(error != 0) perror("SERVER SEMAPHORE linhas CLOSE ERROR\n");
	error = sem_close(atenderCliente);
	if(error != 0) perror("SERVER SEMAPHORE linhas CLOSE ERROR\n");

	// Fazer unlink dos 3 semaforos. So efectiva quando todos fizerem close. http://pubs.opengroup.org/onlinepubs/7908799/xsh/sem_unlink.html
	error = sem_unlink(SEM_CL_LE );
	if(error !=0) perror("server SEMAPHORE UNLINK ERROR\n");
	error = sem_unlink(SEM_CH_SE );
	if(error !=0) perror("server SEMAPHORE UNLINK ERROR\n");
	error = sem_unlink(SEM_AT_CL );
	if(error !=0) perror("server SEMAPHORE UNLINK ERROR\n");

	

	printf("SERVER[pid:%i]: terminado.\n", getpid());

	return 0;
}
