#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//Bibliotes de semaforos
#include <semaphore.h>

#define SEM_01 "sem_M4ex03"

//*******************************************
// Para correr o exercicio basta apenas escreve o seguinte commando no bash:
// ./main.c lixo.txt &
// Varias vezes. O "&" obriga o bash a correr esse executavel como um processo background 
// permitindo o accesso continuado a linha de comandos.
//**********


int main(int argc, char *argv[]){ 
	int error;		//Diagnostico dos processos de partilha de memoria
	size_t pid;
	sem_t 	 * semaforo;
//	int i = 0;
//	int j = 0;	
	
	printf("arg: %i\n", argc);
	if(argc < 2 ) {printf("Indique o nome do ficheiro como argumento ex: lixo.txt\n");return 0;}

	//Vamos obter o nome do ficheiro.
	char nomeFicheiro[255];
	strcpy(nomeFicheiro, "./");
	strcat(nomeFicheiro, argv[1]);
	
	printf("FILE: %s\n",nomeFicheiro);

	
	// Cria o semaforo, mas se existir nao cria, apenas abre.
	semaforo = sem_open(SEM_01, O_CREAT , 0644, 1); 
	if(semaforo == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}

	sem_wait(semaforo); //Pede permicao para entrar.
	
	//Abre o ficheiro 
	FILE *fp = fopen(nomeFicheiro , "a");  // Opcao "a" serve para escrever em append, ou seja adicionar ao fim.
	if(fp == NULL) {perror("Erro a abrir ficheiro"); return 1;}
	
	//ESCEVE E DORME
	pid = getpid();
	fprintf(fp,"Eu sou o processo com o PID %i\n", pid);
	sleep(2);

	//Fecha ficheiro.
	error = fclose(fp);
	if(error != 0) perror("Erro a fechar ficheiro.");

	sem_post(semaforo); //Liberta vez.


	error = sem_close(semaforo);
	if(error != 0) perror("SEMAPHORE CLOSE ERROR\n");

//	COMENTADO PORQUE NAO SABEMOS O NUMERO DE INSTANCIAS, PORTANTO DEIXAMOS O SEMAFORO.
//	int sclose = sem_unlink(SEM_01);	//apagar o semaforo 
//	if(sclose !=0) perror("SEMAPHORE UNLINK ERROR\n");


	printf("PAI: terminado.\n");

	return 0;
}


