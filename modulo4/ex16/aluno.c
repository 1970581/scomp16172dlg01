#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//Bibliotes de semaforos
#include <semaphore.h>

#define SEM_AL "semaforoAluno"
#define SEM_FU "semaforoFuncionario"
#define SEM_DO "semaforoDocente"
#define SEM_OC "semaforoOcupado"
#define SEM_SA "semaforoSair"
#define MAX 2

//FUNCAO DE CRIAR SEMAFOROS 
sem_t * criarSemaforo( char nome[] , int valorInicial){
	sem_t * objecto = sem_open(nome, O_CREAT , 0644, valorInicial); //Inicializa a 10
	if( objecto == SEM_FAILED) {
		perror("SEMAPHORE CREATION ERROR\n");
		printf("TYPE: rm /dev/shm/sem.%s \n", nome);
		exit(EXIT_FAILURE);
	}
	return objecto;
}

//FUNCAO DE FECHAR SEMAFOROS
void fecharSemaforo( sem_t * semaforo){
	int error = sem_close(semaforo);
	if(error != 0) perror("SEMAPHORE CLOSE ERROR\n");
}

//FUNCAO PARA FAZER UNLINK A UM SEMAFORO 
void deslinkarSemaforo(char nome[] ){
	int error = sem_unlink( nome );
	if(error !=0) perror("SEMAPHORE UNLINK ERROR\n");
}

int main(int argc, char *argv[]){
	//Cria os semaforos.
	sem_t * semaforoAluno = criarSemaforo(SEM_AL , MAX);			// coordena o semaforo Aluno
	sem_t * semaforoFuncionario = criarSemaforo(SEM_FU , MAX);		// coordena o semaforo Funcionario
	sem_t * semaforoDocente = criarSemaforo(SEM_DO , MAX);			// coordena o semaforo Docente
	sem_t * semaforoOcupado = criarSemaforo(SEM_OC , 0);			// coordena o semaforo Lugar Ocupado
	sem_t * semaforoSair = criarSemaforo(SEM_SA , 0);				// coordena o semaforo de carro a sair
	
	printf("Carro de aluno vai utilizar o parque\n");
	
	sem_wait(semaforoAluno);
	if(sem_trywait(semaforoDocente) != 0){ //Aqui e quando a entrada de viaturas e condicionada
		printf("d\n");
	}
	if(sem_trywait(semaforoFuncionario) != 0){
		printf("f\n");
	}
	
	printf("Aluno a utilizar o parque\n");
	sem_post(semaforoOcupado);
	
	sem_wait(semaforoSair);
	printf("Aluno saiu do parque\n");
	
	return 0;
}
