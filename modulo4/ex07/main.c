#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <semaphore.h>

#define SEM_01 "semaforo01"

typedef struct {
	int atenderCliente;
	int numBilhete;		
} shared_data_type;

// INSTRUCOES PARA EXECUTAR O PROGRAMA:
// Ao fazer "make run" existe um script pre-definido para uma simulacao de 6 clientes
//	
//  EXECUCAO "MANUAL" DO PROGRAMA PARA 3 CLIENTES:
//	./atendedor.c & (Inicia a simulacao. "Abriu o balcao")
//	./main.c & (Insere um cliente para a fila.)
//	./main.c & (Insere outro cliente para a fila.)
//	./main.c (Insere o ULTIMO cliente para a fila.)
//


int main(int argc, char *argv[]){
	int fd;			//Descritor da shared memory 
	int error;		//Diagnostico dos processos de partilha de memoria
	int data_size = sizeof(shared_data_type);	//Tamanho da estrutura definida
	shared_data_type *shared_data;				//Apontador de uma estrutura
	sem_t * semaforo;
	
	//SEMAFORO - START (O_CREAT porque o atendedor.c executa primeiro e e responsavel por criar e apagar o semaforo)	
	semaforo = sem_open(SEM_01, O_CREAT , 0644, 0); //Abrir semaforo 
	if(semaforo == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	
	//PARTILHA DE MEMORIA - START
	fd = shm_open("/shmtest", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);	// Abrir/criar ficheiro de memoria partilhada
	if(fd == -1){perror("SHM OPEN RETURNED -1");}
	
	error = ftruncate(fd, data_size);			// Indica o tamanho da zona de memoria					
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	
	//CODIGO	
		shared_data->atenderCliente++;		//Cada vez que o main.c e executado e "mais um cliente para a fila"
		sem_wait(semaforo);					//So passa depois do cliente receber o bilhete
		printf("O cliente recebeu o bilhete numero %i\n", shared_data->numBilhete);	//Indica o numero do bilhete
	
	
	return 0;
}
