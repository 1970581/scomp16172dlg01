#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

#include <time.h>  		// usada pelo random.

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <semaphore.h>

#define SEM_01 "semaforo01"

int random_number(int max, int seed_multiplier){
	int valor_saida = -1;
	int max_number = max;
	//int minimum_number = 1;
	if(seed_multiplier == 0) seed_multiplier = 1;
	
	// Vamos alterar a seed to rand() com recurso a uma funcao que devolve o tempo actual.
	// Isto previne rand() de dar sempre o mesmo valor de saida.
	srand(time(NULL)*seed_multiplier);
	
	//Chamamos o rand() para obter um numero random de 0 a RAND_MAX 
	int numero_random = rand();
	
	// Dividimos o valor obtido pelo RAND_MAX para obtermos um float de 0 até 1.
	// Como em Java.
	float aux = (float) numero_random / RAND_MAX;

	// Multiplicamos por maior valor, mais 1 para obter numeros de 1 a 5.
	valor_saida = aux * (max_number) +1 ;


	//Devolvemos o valor.
	return(valor_saida);
}

typedef struct {
	int atenderCliente;
	int numBilhete;		
} shared_data_type;

int main(int argc, char *argv[]){
	int fd;			//Descritor da shared memory 
	int error;		//Diagnostico dos processos de partilha de memoria
	int data_size = sizeof(shared_data_type);	//Tamanho da estrutura definida
	shared_data_type *shared_data;				//Apontador de uma estrutura
	int tempoServico;			//Variavel usada para saber o tempo de atendimento do cliente	
	sem_t * semaforo;
	int i = 0;		//Esta variavel so vai ser usada para o metodo random_number
	
	//PARTILHA DE MEMORIA - START
	fd = shm_open("/shmtest", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);	// Abrir/criar ficheiro de memoria partilhada
	if(fd == -1){perror("SHM OPEN RETURNED -1");}
	
	error = ftruncate(fd, data_size);			// Indica o tamanho da zona de memoria					
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	//CODIGO
	
	//SEMAFORO - START	
	semaforo = sem_open(SEM_01, O_CREAT | O_EXCL , 0644, 0); //Criar semaforo 
	if(semaforo == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	
	shared_data->atenderCliente = 0;	//Como o atendedor.c e executado apenas uma vez por "simulacao de atendimento"
	shared_data->numBilhete = 0;		//Os valores sao pre definidos como 0
	
	do{
		//AQUI: O cliente esta a ser atendido, os outros clientes esperam
		tempoServico = random_number(10, i+1);
		sleep(tempoServico);
		printf("Atendedor: O cliente foi atendido durante %i segundos\n", tempoServico);
		shared_data->numBilhete++;
		printf("Atendedor: O cliente vai receber o bilhete numero %i\n", shared_data->numBilhete);
		
		//AQUI: O cliente ja foi atendido e "passa para o proximo cliente"
		sem_post(semaforo);
		shared_data->atenderCliente--;
	} while(shared_data->atenderCliente > 0);	//Quando "nao houver mais clientes na fila" o programa termina
												//Para verificar a funcionalidade desta condicao
												//O main.c tem que ser executado varias vezes
	
	//SEMAFORO - FINISH
	int sclose = sem_unlink(SEM_01);	//apagar o semaforo 
	if(sclose !=0) perror("SEMAPHORE CLOSE ERROR\n");
	
	
	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Desmapea
	if(error == -1){perror("MUNMAP RETURNED -1");}
	
	error = close(fd);						// Fecha o descritor
	if(error == -1){perror("CLOSE RETURNED -1");} 
	
	error = shm_unlink("/shmtest");						// Fechar ficheiro de memoria partilhada
	if(error == -1){perror("UNLINK RETURNED -1");}
	
	return 0;
}
