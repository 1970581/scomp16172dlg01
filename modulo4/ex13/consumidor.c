#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//#include <time.h>  		// usada pelo random.

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//Bibliotes de semaforos
#include <semaphore.h>

#define SEM_1 "sem_M4ex13_mutexMemoria"
#define SEM_2 "sem_M4ex13_mensagem"
#define SEM_3 "sem_M4ex13_prioridade1"
#define SEM_4 "sem_M4ex13_prioridade2"
#define SEM_5 "sem_M4ex13_prioridade3"
#define SEM_6 "sem_M4ex13_prioridade4"
#define MEM13 "/shmtestM4ex13"	

#define MAX_MESSAGE_SIZE 255
#define MAX_PRIORITY 4
#define MAX_MESSAGE_PER_PRIORITY 3

//*******************************************
//  IMPORTANTE!!!!!
//********
//   PRIORIDADE: 1 e mais prioritario que 2.
//   Usamos um buffer circular para guardar as mensagens. O buffer e cubico. Tem 3 dimensoes.
//   prioridade x posicao x char da mensagem 	



//Declaracao de funcoes:
sem_t * criarSemaforo( char nome[] , int valorInicial);
void fecharSemaforo( sem_t * semaforo);
void deslinkarSemaforo(char nome[] );

typedef struct {  // Zona de memoria partilhada
	int  	escritas[MAX_PRIORITY + 1];				//   numero de mensagens escritas[prioridade]	// usamos 5 em vez de quatro para facilitar os [n].
	int 	lidas[MAX_PRIORITY +1];
	char mensagem[MAX_PRIORITY +1][MAX_MESSAGE_PER_PRIORITY][MAX_MESSAGE_SIZE];	//   prioridade 1 mensagem numero Y  BUFFER CIRCULAR.
} shared_data_type;


//  <<< MAIN >>>
int main(int argc, char *argv[]) { 
	int error;		//Diagnostico dos processos de partilha de memoria
	size_t pid ;
	int i = 0;

	//PARAMETROS DO EXECUTAVEL

	//Cria os semaforos.
	sem_t * sem_mutexMemoria	= criarSemaforo( SEM_1 , 1);			// coordena o acesso exclusivo a memoria partilhada 
	sem_t * sem_mensagem		= criarSemaforo(SEM_2 , 0);			// coordena o numero de mensagens globais que podem ser lidas
	sem_t * sem_prioridade1 	= criarSemaforo(SEM_3 , 3);			// coordena o numero de mensagens escritas de prioridade X
	sem_t * sem_prioridade2		= criarSemaforo(SEM_4 , 3);			// coordena o numero de mensagens escritas de prioridade X
	sem_t * sem_prioridade3 	= criarSemaforo(SEM_5 , 3);			// coordena o numero de mensagens escritas de prioridade X
	sem_t * sem_prioridade4 	= criarSemaforo(SEM_6 , 3);			// coordena o numero de mensagens escritas de prioridade X



	//Memoria partilhada.
	int fd;						//Descritor da shared memory 
	int data_size = sizeof(shared_data_type);	//Tamanho da estrutura definida
	shared_data_type *shared_data;			//Apontador de uma estrutura de memoria partilhada.

	//Criar/Open ficheiro de memoria partilhada. 
	fd = shm_open( MEM13, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);	// Criar ficheiro de memoria partilhada 
	if(fd == -1){perror("SHM OPEN RETURNED -1 \nTYPE: rm /dev/shm/shmtestM4ex13\n "); return 0;}		
	
	error = ftruncate(fd, data_size);			// Indica o tamanho da zona de memoria					
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	//Inicializar memoria partilhada. 
//		  Desnecessario, inicializa tudo a 0.	
//	 
//		http://man7.org/linux/man-pages/man3/shm_open.3.html
//		A new shared memory object initially has zero length—the
//	         size of the object can be set using ftruncate(2).  The
//       	 newly allocated bytes of a shared memory object are
//                 automatically initialized to 0.

	pid = getpid();
	printf("Consumidor[pid:%i] a correr. \n", pid);


//================

	// CONSUMIDOR / LEITOR.

	sem_wait(sem_mensagem);					// Aguardamos que tenha sido escrita uma mensagem.
 
	sem_wait(sem_mutexMemoria);				// Acesso exclusivo a memoria. VAMOS ESCREVER.
	
//		Buffer: mensagem [prioridade][posicao][char 255];		
	
		//Obter a prioridade.   PRIORIDADE: 1 e mais prioritario que 2.
		int prioridade 	= 0;
		int nEscritas 	= 0;
		int nLidas 	= 0;
		for ( i = 0; i < MAX_PRIORITY ; i++){
			 							// Iteramos as prioridades da mais importante pra a menos.
			nEscritas	= shared_data->escritas[i];		// Numero de mensagens escritas nesta prioridade 
			nLidas		= shared_data->lidas[i];		// Numero de mensagens lidas nesta prioridade 	
			if(nEscritas > nLidas) {				// Se temos mais escritas que lidas, entao.
				prioridade = i;					// Encontramos a nossa prioridade. Terminamos Ciclo.
				break;
			}
		}
		



		if (nLidas >= nEscritas) printf("Consumidor[%i] erro detectado. Lida mensagem nao escrita.\n", pid);

		int posicao = nLidas % MAX_MESSAGE_PER_PRIORITY ;		//  0 % 3 = 0 , 1 % 3 = 1 , 4 % 3 = 1  //posicao no buffer.

		

		// Ler mensagem.
		char palavra [MAX_MESSAGE_SIZE];
		strcpy(palavra , shared_data->mensagem[prioridade][posicao] );
		printf("Consumidor[%i] lido \"%s\" na posicao %i da prioridade %i .\n", pid, palavra , posicao, prioridade);
	
		shared_data->lidas[prioridade]++;			// Indicamos que foi lida uma mensagem daquela prioridade ao buffer.


		if(prioridade == 1) sem_post(sem_prioridade1);      	// Indicamos que libertamos um espaco da memoria de mensagens da prioridade em questao.
		if(prioridade == 2) sem_post(sem_prioridade2);
		if(prioridade == 3) sem_post(sem_prioridade3);
		if(prioridade == 4) sem_post(sem_prioridade4);
 

	sem_post(sem_mutexMemoria);				// Libertado acesso a memoria.

	
//==========
	
	//finalizar: 
	
	fecharSemaforo(sem_mutexMemoria);
	fecharSemaforo(sem_mensagem);
	fecharSemaforo(sem_prioridade1);
	fecharSemaforo(sem_prioridade2);
	fecharSemaforo(sem_prioridade3);
	fecharSemaforo(sem_prioridade4);

//Ninguem deslinka os semaforos.
//	deslinkarSemaforo(SEM_1);
//	deslinkarSemaforo(SEM_2);
//	deslinkarSemaforo(SEM_3);
//	deslinkarSemaforo(SEM_4);
//	deslinkarSemaforo(SEM_5);
//	deslinkarSemaforo(SEM_6);


	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Desmapea
	if(error == -1){perror("SERVER MUNMAP RETURNED -1");}
	
	error = close(fd);						// Fecha o descritor
	if(error == -1){perror("SERVER CLOSE RETURNED -1");}
	
//Ninguem faz unlink da memoria partilhada.
//	error = shm_unlink(MEM13);					// Fecha o ficheiro
//	if(error == -1) {perror("SERVER UNLINK RETURNED -1\n");}	

//	printf("CARRO[%i]: terminado.\n", getpid());

	return 0;
}


//FUNCAO DE CRIAR SEMAFOROS 
sem_t * criarSemaforo( char nome[] , int valorInicial){
	sem_t * objecto = sem_open(nome, O_CREAT , 0644, valorInicial); //Inicializa a 10
	if( objecto == SEM_FAILED) {
		perror("SEMAPHORE CREATION ERROR\n");
		printf("TYPE: rm /dev/shm/sem.%s \n", nome);
		exit(EXIT_FAILURE);
	}
	return objecto;
}

//FUNCAO DE FECHAR SEMAFOROS
void fecharSemaforo( sem_t * semaforo){
	int error = sem_close(semaforo);
	if(error != 0) perror("SEMAPHORE CLOSE ERROR\n");
}

//FUNCAO PARA FAZER UNLINK A UM SEMAFORO 
void deslinkarSemaforo(char nome[] ){
	int error = sem_unlink( nome );
	if(error !=0) perror("SEMAPHORE UNLINK ERROR\n");
}



