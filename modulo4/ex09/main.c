#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//Bibliotes de semaforos
#include <semaphore.h>

#define SEM_ONE "sem_M4ex09_bufferAccess"
#define SEM_TWO "sem_M4ex09_bufferFreeSpace"
#define SEM_THREE "sem_M4ex09_bufferRead"
#define MAX_BUFFER 10

//*******************************************
// Se o main.exe receber algum parametro, forcamos a escrita a demorar 1 segundo na zona critica (sleep).
//********
// APAGAR SEMAFOROS: 
// rm /dev/shm/sem.sem_M4ex09_bufferAccess
// rm /dev/shm/sem.sem_M4ex09_bufferFreeSpace
// rm /dev/shm/sem.sem_M4ex09_bufferRead
// APAGAR MEMORIA PARTILHADA:
// rm rm /dev/dhm/shmtest

typedef struct {  // Zona de memoria partilhada
	int buffer[MAX_BUFFER];	// buffer de partilha de inteiros.
	int nWrite;		// total de escritas no buffer
	int nRead;		// total de leituras do buffer
	int sair;		// Escrito por um cliente a informar o servidor para terminar. 1 sair.
} shared_data_type;


int main(int argc, char *argv[]){ 
	int error;		//Diagnostico dos processos de partilha de memoria
	size_t pid ;
	int i = 0;

	int delay = 0;			// Se o programa receber um parametro, vamos obrigar os filhos e fazer um sleep durante a escrita.
	if(argc >= 2) {
		delay = 1;	// delay == 1 == true, filhos demoram 1 segundo a escrever.
		printf("\n DELAY == true FORCAMOS A ESCRITA A DEMORAR 1 SEGUNDO\n\n");
	}

	int fd;						//Descritor da shared memory 
	int data_size = sizeof(shared_data_type);	//Tamanho da estrutura definida
	shared_data_type *shared_data;				//Apontador de uma estrutura de memoria partilhada.

	sem_t * sem_bufferAccess;	// Semaforo que indica limita acesso ao buffer.
	sem_t * sem_bufferFreeSpace;	//Semaforo que indica o numero de posicoes livres onde escrever.
	sem_t * sem_bufferRead;		//Semaforo que indica que existem valores para ler.

	printf("main.exe [pid:%i] A INICIALIZAR ... \n", getpid());



	// Cria o semaforo. Se ja existir da erro e avisa.
	sem_bufferAccess = sem_open(SEM_ONE, O_CREAT | O_EXCL , 0644, 1); //Inicializa a 1
	if(sem_bufferAccess == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR\n TYPE: rm /dev/shm/sem.sem_M4ex09_bufferAccess \n"); exit(EXIT_FAILURE);}
	sem_bufferFreeSpace = sem_open(SEM_TWO, O_CREAT | O_EXCL , 0644, MAX_BUFFER); // Inicializa a 10
	if(sem_bufferFreeSpace == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR\n TYPE: rm /dev/shm/sem.sem_M4ex09_bufferFreeSpace \n"); exit(EXIT_FAILURE);}
	sem_bufferRead = sem_open(SEM_THREE, O_CREAT | O_EXCL , 0644, 0); // Inicializa a 0
	if(sem_bufferRead == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR\n TYPE: rm /dev/shm/sem.sem_M4ex09_bufferRead \n"); exit(EXIT_FAILURE);}

	
	//Criar ficheiro de memoria partilhada. Se ja existir com o mesmo nome aborta.
	fd = shm_open("/shmtest", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);	// Criar ficheiro de memoria partilhada se ja existir aborta!!!
	if(fd == -1){perror("SHM OPEN RETURNED -1 \nTYPE: rm /dev/dhm/shmtest\n "); return 0;}		
	
	error = ftruncate(fd, data_size);			// Indica o tamanho da zona de memoria					
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	//Inicializar memoria partilhada. 	
	shared_data->nWrite = 0;	  	
	shared_data->nRead = 0; 

//================

	// Criar os filhos.
	for (i = 1; i < 3; i++){
		pid = fork();
		if(pid < 0) {perror("Erro a fazer fork.\n"); exit(1);}
		else{
			printf("%s[%i] processo %i online.\n", (pid > 0) ? "PAI  " : "Filho" , getpid(), i );
		}
		if(pid == 0) break;
	}	


	
	// CODIGO FILHOS PRODUTORES
	if(pid == 0){
		while( 1 ){
			sem_wait(sem_bufferFreeSpace);	//Esperar por espaco livre no buffer para escrever. Inicializa a 10.

			sem_wait(sem_bufferAccess); 	//[000]  acesso exclusivo ao buffer.

			int numeroEscritas = shared_data->nWrite;

			if(numeroEscritas >= 30) goto afterWrite;	//Se um filho faz a 30, o outro vai tentar fazer a 31, por isso existe este check+salto.

			int posicao = numeroEscritas % MAX_BUFFER ; //Obter a posicao onde escrever no buffer.   25 % 10 = 5 ....
			numeroEscritas++;
			int input = numeroEscritas * 100;  // Para nao existirem duvidas que nao lemos o numero de escritas. Enunciado apenas pede numeros crescentes.
;

			shared_data->buffer[posicao] 	= input;
 			shared_data->nWrite 		= numeroEscritas ;
			
			printf("Filho %i [%i] escrita %i buffer[%i] valor= %i.\n", i	, getpid(), numeroEscritas, posicao ,input);
			
			afterWrite:
			
			if(delay) sleep(1);		// Usado para simular que a escrita demora algum tempo.

			sem_post(sem_bufferAccess);	//[000]	 liberta acesso exclusivo ao buffer
			sem_post(sem_bufferRead);	//[000]  liberta uma leitura.
			
			if(numeroEscritas >= 30) break;	//Condicao de saida.
		}

		printf("Filho[%i] terminado.\n",getpid());
		exit(0);

	}

	sleep(1);   // Apenas PAI, atrasamos o Pai para obrigar o CPU a dar tempo ao segundo filho, visto a minha maquina virtual so usar 1 processador.

	// CODIGO DO PAI LEITOR.
	while(1){
		
		sem_wait(sem_bufferRead);	//[000]	 esperar por algo para ler.
		sem_wait(sem_bufferAccess); 	//[000]  acesso exclusivo ao buffer.
		
		
		//ler 
		int leituras = shared_data->nRead;
		int posicao = leituras % MAX_BUFFER;	//Obter posicao onde ler no buffer.  25 % 10 = 5 ...
		int output = shared_data->buffer[posicao];
		leituras++;
		shared_data->nRead = leituras;
		printf("Pai[%i] leitura %i buffer[%i] valor=%i\n", getpid(), leituras, posicao, output);
		
		sem_post(sem_bufferAccess);	//[000]	 liberta acesso exclusivo ao buffer
		sem_post(sem_bufferFreeSpace);   //[000]  liberta espaco para ser escrito.
		
		if(leituras >= 30) break;	//Condicao de saida.
	}
	
	
		

	
//==========
	
	//finalizar: 

	//esperar o fim dos filhos.
	for (i = 0; i<2; i++){
		wait(NULL);
	}

	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Desmapea
	if(error == -1){perror("SERVER MUNMAP RETURNED -1");}
	
	error = close(fd);						// Fecha o descritor
	if(error == -1){perror("SERVER CLOSE RETURNED -1");}
	
	error = shm_unlink("/shmtest");					// Fecha o ficheiro , senão nao cria na proxima corrida.
	if(error == -1) {perror("SERVER UNLINK RETURNED -1\n");}

	
	//Fechar o semaforos.
	error = sem_close(sem_bufferAccess);
	if(error != 0) perror("SEMAPHORE 1 CLOSE ERROR\n");
	error = sem_close(sem_bufferFreeSpace);
	if(error != 0) perror("SEMAPHORE 2 CLOSE ERROR\n");
	error = sem_close(sem_bufferRead);
	if(error != 0) perror("SEMAPHORE 3 CLOSE ERROR\n");



	// Fazer unlink dos  semaforos. So efectiva quando todos fizerem close. http://pubs.opengroup.org/onlinepubs/7908799/xsh/sem_unlink.html
	error = sem_unlink(SEM_ONE );
	if(error !=0) perror("SEMAPHORE UNLINK ERROR\n");
	error = sem_unlink(SEM_TWO );
	if(error !=0) perror("SEMAPHORE UNLINK ERROR\n");
	error = sem_unlink(SEM_THREE );
	if(error !=0) perror("SEMAPHORE UNLINK ERROR\n");

	

	printf("PAI[pid:%i]: terminado.\n\n\n\n", getpid());

	return 0;
}
