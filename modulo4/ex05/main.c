#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <semaphore.h>

#define SEM_01 "semaforo01"


int main(int argc, char *argv[]){ 
	size_t pid;
	sem_t * semaforo;

	semaforo = sem_open(SEM_01, O_CREAT | O_EXCL , 0644, 0); //Criar semaforo 
	if(semaforo == SEM_FAILED) {perror("SEMAPHORE CREATION ERROR - o semaforo ja pode existir , apaga-lo\n"); exit(EXIT_FAILURE);}
	

	//Criacao do filho
	pid = fork();
	if(pid < 0) {
		perror("Erro a fazer fork.\n"); 
		exit(1);
	} else {
		printf("%s[%i] em concorrencia.\n", (pid > 0) ? "PAI  " : "Filho" , getpid() );
	}	
	

	//CODIGO FILHO
	if(pid == 0){
		sem_wait(semaforo); //Pede permicao para entrar.
		printf("Eu sou o filho\n");
		sem_post(semaforo); //Liberta vez.
		exit(1);
	}
	
	//PAI
	printf("Eu sou o pai\n");
	sem_post(semaforo); 	//Liberta vez.
	wait(NULL);				//O pai espera que o filho termine

	int sclose = sem_unlink(SEM_01);	//apagar o semaforo 
	if(sclose !=0) perror("SEMAPHORE CLOSE ERROR\n");
	
	printf("PAI: terminado.\n");

	return 0;
}


