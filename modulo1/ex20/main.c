#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


	
	
int lerNumero(int min, int max){
	printf("Escreva um numero de %i a %i ?", min, max);
	char numeroTexto[256];
	int numero = 0;
	
	scanf("%s" , numeroTexto);
	int valor = atoi(numeroTexto);
	if (valor < min || valor > max){
		printf("Valor %i fora de limites, a sair.", valor);
		exit(2);
	}
	return valor;
} 


int main(int argc, char *argv[]){
	int min = 1;
	int max = 5;	
	int numero_escolhido = 0;
	int numero_random = 0;
	int dinheiro = 25;	
	int aposta = 1;	
	pid_t pid;
	int status;

INICIO:  //Referencia para salto GOTO tipo assembly.
	
	//Escolher valor de aposta.
	printf("Tem %i euros. Escolha valor da aposta?\n", dinheiro);
	aposta = lerNumero(1, dinheiro);
	printf("Apostou: %i euro.\n", aposta);

	//Jogador escolhe numero 
	printf("Vamos escolher o seu numero.");
	numero_escolhido =	lerNumero(min, max);
	printf("Escolheu o %i\n", numero_escolhido );	

	//Obter um numero random
	printf("A gerar um numero random\n");
	pid = fork();

	if(pid == 0){ //CODIGO FILHO
		execlp( "./batota/batota.exe" , "./batota/batota.exe", (char*) NULL);
		exit(-1);
	}
	else {//CODIGO PAI 
	
		waitpid(pid, &status,0);
		numero_random = WEXITSTATUS(status);
		printf("A comparar o seu valor %i com o meu %i.\n", numero_escolhido , numero_random);
		if(numero_random < min || numero_random > max){
		 	printf("Erro a invocar batota\n");
			exit(2);
		}
	
	}
	
	//ACERTAR O VALOR DO DINHEIRO.	
	if(numero_random == numero_escolhido) {
		printf("ACERTOU!!!\n");
		dinheiro += aposta;	
	}
	else {	
		printf("FALHOU!!!\n");
		dinheiro -= aposta;
	}

	//VERIFICAR SE AINDA TEMOS DINHEIRO E REPETIMOS COM UM SALTO GOTO para TAG Inicio
	if (dinheiro > 0) goto INICIO ;
	
	
	printf("Tem %i euro. Perdeu!!!!\n", dinheiro );	

	return 0;
}
