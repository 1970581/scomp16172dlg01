#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(void){
	
	pid_t pid;
	int i;
	int tamanho = 100000;
	int resultados[tamanho];
	int dados[tamanho];
	int status;

	for(i = 0; i < tamanho; i++){
		dados[i] = i;
	}
	
	int start;
	int final;
	
	pid = fork();
	if(pid < 0){
		printf("Erro no fork");
		exit(-1);
	}
	if(pid == 0){	//filho
		start = 0;
		final = tamanho / 2;
	} else {		//pai
		start = tamanho / 2;
		final = tamanho;
	}
	
	
	for(i = start; i < final; i++){
		resultados[i] = dados[i]*4 + 20;
	}

	
	if(pid > 0){ //pai
		waitpid(pid, &status, 0);
	}
	for(i = start; i < final; i++){
		printf("[%d]:%d\n", i, resultados[i]);
	}

	/* 6b)
	 * O processo pai espera pelo processo filho durante a apresentacao dos resultado
	 * Contudo, devido a velocidade da execucao nao e possivel acompanhar a evolucao dos processos
	 */


	return 0;
}

