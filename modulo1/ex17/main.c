#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int fakeSystem(char * s){
		pid_t pid;
		int status;
		pid = fork();

		//CODIGO FILHO
		if (pid == 0) {
			execlp( s, s, (char*) NULL);
			exit(1);
		}
		//CODIGO PAI
		else {
		      	waitpid( pid , &status , 0);
			printf("\n");	
		}
		return 0;
	
	
} 


int main(int argc, char *argv[]){
	
	fakeSystem("ls");
	
	
	return 0;
}
