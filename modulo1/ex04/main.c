#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(void){
	
	pid_t pid;
	int f;
	pid_t vec[4];

	vec[0] = getpid();
	printf("Pid PAI: %d\n", vec[0]);

	for(f=1;f <4;f ++){
		pid = fork();

		//Error dealing
		if(pid < 0) {printf("Erro no fork: %i\n",f); exit(-1);}

		if (pid > 0){
			printf ("Eu sou  PAI\n");
			vec[f] = pid;  //Guardar o pid do processo criado
			

 		}
		else {  //Filho 
			sleep (1);
			printf("PID: %d Terminei.\n",getpid());
			exit(f);
		}
	}

	//PAI
	int wPid;
	int status;
/*	
	for(f = 0; f<3; f++){
		//status = NULL;
		wPid = waitpid(-1 , &status , 0 );
			
		if (wPid == -1) {
			perror("Erro em wait pid");
			exit(-1);	
		}
		else {
			printf("Filho nº: %d ", WEXITSTATUS(status));
			printf("com PID %d\n", wPid);
		}
		
	}
*/


	wPid = waitpid(vec[2],&status, WNOHANG);
	if(wPid == 0) printf("Ainda nao terminou\n");



	return 0;
}

