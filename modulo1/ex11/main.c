#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

char cria_gemeos(pid_t lista[2]){
	int status;
	pid_t p1 = 0;
	pid_t p2;
	char character = 'p';
	
	p1 = fork();
	
	//Verificacao de erro 
	if(p1 < 0){ 
		printf("Erro no fork");
		exit(-1);
	}
	
	//CODIGO PAI
	if(p1 > 0){ 	//pai
		lista[0] = p1;
	}
	// CODIGO DO 1 Filho
	else {	 		//filho
		exit(97);
	}
				
	p2 = fork();
	
	//Verificacao de erro
	if(p2 < 0){
		printf("Erro no fork");
		exit(-1);
	}
	
	//CODIGO DO PAI
	if(p2 > 0){ 	//pai
		lista[1] = p2;
		
	//CODIGO DO 2 FILHO	
	} else {	 		//filho
		exit(98);
	}
				
	
	return character;
}

int main(void){
	pid_t lista1[2];
	pid_t lista2[2];
	pid_t lista3[2];
	int status;
	
	// Correr a criacao dos 2x 3 filhos
	char g1 = cria_gemeos(lista1); printf("%c\n", g1);
	char g2 = cria_gemeos(lista2); printf("%c\n", g2);
	char g3 = cria_gemeos(lista3); printf("%c\n", g3);
	
	pid_t f1Pid;
	pid_t thisPid;
	int f1;
	
	//Devido a usarmos vectores de 2 posições e nao um unico vector de 6 pesoicoes fizemos o codig em linha e nao ciclo.
	
	//Filho 1
	f1Pid = waitpid(lista1[0], &status,0);
	f1 = WEXITSTATUS(status);
	thisPid = getpid();	
	printf("Mmain: %d , terminou filho %d com \"%c\"\n", thisPid, f1Pid , f1); 
	
	//Filho 2
	f1Pid = waitpid(lista1[1], &status,0);
	f1 = WEXITSTATUS(status);
	thisPid = getpid();	
	printf("Mmain: %d , terminou filho %d com \"%c\"\n", thisPid, f1Pid , f1);
	
	//Filho 3
	f1Pid = waitpid(lista2[0], &status,0);
	f1 = WEXITSTATUS(status);
	thisPid = getpid();	
	printf("Mmain: %d , terminou filho %d com \"%c\"\n", thisPid, f1Pid , f1);
	
	//Filho 4
	f1Pid = waitpid(lista2[1], &status,0);
	f1 = WEXITSTATUS(status);
	thisPid = getpid();	
	printf("Mmain: %d , terminou filho %d com \"%c\"\n", thisPid, f1Pid , f1);
	
	//Filho 5
	f1Pid = waitpid(lista3[0], &status,0);
	f1 = WEXITSTATUS(status);
	thisPid = getpid();	
	printf("Mmain: %d , terminou filho %d com \"%c\"\n", thisPid, f1Pid , f1);
	
	//Filho 6
	f1Pid = waitpid(lista3[1], &status,0);
	f1 = WEXITSTATUS(status);
	thisPid = getpid();	
	printf("Mmain: %d , terminou filho %d com \"%c\"\n", thisPid, f1Pid , f1);
	
	
	return 0;
}
