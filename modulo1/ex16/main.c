#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	char comando[250];		//String para o utilizador escrever um comando
	char sair[] = "sair";	//String pre definida para comparacao

	do{							//loop para quando o utilizador escrever sair, o programa terminar
		scanf("%s", comando);	//Scanf fora do fork para fazer a comparacao
		pid = fork();
		
		//CODIGO FILHO
		if (pid == 0) {
			printf("A processar: %s\n", comando);
		    execlp(comando, comando, (char*) NULL);
		    exit(1);
		}//CODIGO PAI
		else {
		    waitpid(pid, &status , 0);
			printf("\n");	
		}
		
		
	}while(strcmp(comando, sair) != 0);	//Caso o utilizador tenha escrito sair, o programa termina

	
	return 0;
}
