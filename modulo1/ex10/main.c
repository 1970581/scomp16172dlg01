#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(void){
	pid_t pid, pidf;
	int i;
	int j;
	int sequencia_num = 20000;
	int tamanho = 100000;
	int status;
	int vec[tamanho];
	int numero = 55500;
	int pos;
	int finish;
	
	for(i = 0; i < tamanho; i++){
		vec[i] = i;
	}
	
	for(i = 0; i < 5; i++){
		pid = fork();
		
		if(pid == 0){
			break; 
		}
	}
		if(pid == 0){ //filho
			finish = i+1; // Variavel do numero de ordem (1 a 5).
						  // IMPORTANTE ser i+1. Para nao haver confusao caso o processo encontra o numero
			for(j = 0; j < sequencia_num; j++){
				pos = j + (sequencia_num * i);
				if(vec[pos] == numero){
					printf("Pos: %d\n", pos);
					exit(finish);
				}
			}
			exit(0);
		}
		
	}
	
	for(i = 0; i < 5; i++){ //pai
		pidf = wait(&status);
		if(WEXITSTATUS(status) == 0){
			printf("Valor nao encontrado neste processo, valor de saida: %d\n", WEXITSTATUS(status));
		} else {
			printf("Valor encontrado no processo %d, numero de ordem: %d\n", pidf, WEXITSTATUS(status));
		}
	}
	printf("Pai terminou\n");
	return 0;
}

