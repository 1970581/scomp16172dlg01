#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	int i;

	char line[256];  // LINHA A mostrar.
	line[0] = 0; // \n String logo terminada
	
	
	//CRiar linha a executar.
	for (i = 1; i < argc; i++){
		strcat(line, argv[i]);
		strcat(line, " ");
	}
	printf("A processar o comando \"%s\".\n", line);

	//int ret = execlp("ls" , "ls" , (char*) NULL);
	//printf("test ret=%d\n", ret);

	//EXECUTAR
	//char mypath[] = "/bin";
	for (i = 1 ; i< argc; i++){
		pid = fork();

		//CODIGO FILHO
		if (pid == 0) {
			execlp( argv[i], argv[i], (char*) NULL);
			exit(1);
		}
		//CODIGO PAI
		else {
		      	waitpid( pid , &status , 0);
			printf("\n");	
		}

	}


/*	
	f1Pid = waitpid(lista1[0], &status,0);
	f1 = WEXITSTATUS(status);
	thisPid = getpid();	
	printf("Mmain: %d , terminou filho %d com \"%c\"\n", thisPid, f1Pid , f1); 
*/	
	
	
	return 0;
}
