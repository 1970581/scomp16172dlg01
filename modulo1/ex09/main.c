#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(void){
	pid_t pid;
	int i;
	int j;
	int tamanho = 200000;
	int status;
	
	for(i = 0; i < 6; i++){
		pid = fork();
		if(pid == 0){ //filho
			for(j = 0; j < tamanho; j++){
				printf("%d\n", (j+1) + (i * tamanho));
			}
			exit(0);
		}
		
	}
	
	for(i = 0; i < 6; i++){
		wait(&status);
	}
	printf("Pai terminou\n");
	return 0;
}

