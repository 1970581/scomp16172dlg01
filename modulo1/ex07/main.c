#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void M(char * c){
	printf("%s\n",c);
}

int main(void){
	
	pid_t pid;
	int i;
	
	
	for(i= 0; i < 2; i++){
		pid = fork();
		if(pid < 0){
			printf("Erro no fork");
			exit(-1);
		}
		
		if(pid > 0){ //pai
			fork();
			M("A");
		} else {	 //filho
			M("B");
			fork();
			exit(0);
		}
		
	}

	


	return 0;
}

