#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(void){
	pid_t pai;
	pid_t f1;
	pid_t f2;
	int status;
	int status2;
	int sleep1 = 5;
	
	
	
	pai = (int) getppid();	//Pid do processo original
	printf("Eu sou o pai com o pid: %i\n", pai);
	
	f1 = fork();	//Fork 1
	if(f1 < 0){printf("Erro no fork");exit(-1);}
	
	if(f1 == 0){	//Codigo do filho 1
		printf("Eu sou o 1 filho com o pid: %i\n", (int) getpid());
		sleep(sleep1);
		exit(1);
	}
	
	f2 = fork();	//Fork 2
	if(f2 < 0){printf("Erro no fork");exit(-1);}
	
	if(f2 == 0){	//Codigo do filho 2
		printf("Eu sou o 2 filho com o pid: %i\n", (int) getpid());
		exit(2);
	}
	
	waitpid(f1,&status,0);	//Esperar pelo filho 1 terminar
	if(WIFEXITED(status)){
		printf("O filho 1 retornou o valor exit: %d\n", WEXITSTATUS(status));
	} else {
		perror("Erro no processo do filho 1\n");
		exit(10);
	}
	
	waitpid(f2,&status2,0);	//Esperar pelo filho 2 terminar
	if(WIFEXITED(status)){
		printf("O filho 2 retornou o valor exit: %d\n", WEXITSTATUS(status2));
	} else {
		perror("Erro no processo do filho 2\n");
		exit(10);
	}
	
	return 0;
}

