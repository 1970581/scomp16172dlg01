#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	char ficheiro[250];		//String para o utilizador escrever um comando
	char cp[] = "cp";		//String pre definido para o comando copy
	char sair[] = "sair";	//String pre definida para comparacao
	char dir[] = "backup";

	do{							//loop para quando o utilizador escrever sair, o programa terminar
		scanf("%s", ficheiro);	//Scanf fora do fork para fazer a comparacao		
		
		if(strcmp(ficheiro, sair) != 0){
			
			pid = fork();		
			//CODIGO FILHO
			if (pid == 0) {
				printf("A copiar...\n");
				execlp("cp", cp, ficheiro, dir, (char*) NULL);
				exit(1);
			}//CODIGO PAI
			else {
				waitpid(pid, &status , 0);
				printf("Ficheiro copiado\n");	
			}
		}
		
		
	}while(strcmp(ficheiro, sair) != 0);	//Caso o utilizador tenha escrito sair, o programa termina

	
	return 0;
}
