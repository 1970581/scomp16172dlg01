#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(int argc, char *argv[]){
//int main(int argc, char **argv){
	int status;
	pid_t pid;
	int i;

	char line[256];  // LINHA A mostrar.
	line[0] = 0; // \n String logo terminada
	
	//Vector para a funcao execvp() [0] contem o comando, o [1] contem o parametro, que nao existe portante NULL.
	char *comando[2];
	comando[1] = NULL;


	
	//CRiar linha a executar.
	for (i = 1; i < argc; i++){
		strcat(line, argv[i]);
		strcat(line, " ");
	}
	printf("A processar o comando \"%s\".\n", line);

// CODIGO DE TESTE DA FUNCAO
/*	comando[0] = "ls";
	int ret = execvp( "ls" , comando);
	printf("test ret=%d\n", ret);
*/

	//EXECUTAR
	//char mypath[] = "/bin";
	for (i = 1 ; i< argc; i++){
		pid = fork();

		//CODIGO FILHO
		if (pid == 0) {
			comando[0] = argv[i];   // Reescrita do comando a usar.
			execvp( argv[i], comando);
			exit(1);
		}
		//CODIGO PAI
		else {
		      	waitpid( pid , &status , 0);
			printf("\n");	
		}

	}


/*	
	f1Pid = waitpid(lista1[0], &status,0);
	f1 = WEXITSTATUS(status);
	thisPid = getpid();	
	printf("Mmain: %d , terminou filho %d com \"%c\"\n", thisPid, f1Pid , f1); 
*/	
	
	
	return 0;
}
