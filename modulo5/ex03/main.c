#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//Biblioteca de threads
#include <pthread.h>

#define COMPRIMENTO_VETOR 1000		//Constante do comprimento do vetor
pthread_mutex_t mux;				//Mutex da thread

int vec[COMPRIMENTO_VETOR];			//Variavel global de um vetor de inteiro
int saldoTotal = 0;					//Variavel global do saldo total

void* thread_func(void *arg){
	int value = *((int*)arg);	//O Value vai ser utilizado para saber o numero da thread
	int j = 0;
	int soma = 0;	//IMPORTANTE: INICIALIZAR AS VARIAVEIS SENAO NAO FUNCIONA
	
	
	// ZONA CRITICA
	pthread_mutex_lock(&mux);
	for(j = value * (COMPRIMENTO_VETOR/5); j < (value * (COMPRIMENTO_VETOR/5)) + 200; j++){
		soma = soma + vec[j];
	}
	saldoTotal += soma;	//Acumula para saber o resultado final
	soma = 0;	//Reinicia a variavel para a proxima soma de 200 posicoes
	pthread_mutex_unlock(&mux);
	
	pthread_exit((void*)NULL);		//Termina a thread
}

int main(int argc, char *argv[]){
	pthread_t threads[5];		//Inicializa 5 threads
	int i, args[5];
	int error;
	
	for(i = 0; i < COMPRIMENTO_VETOR; i++){		//Preenche o vetor
		vec[i] = 1;
	}
	
	error = pthread_mutex_init(&mux, NULL);	//Cria-se o mutex
	if(error){
			printf("ERRO: Return code from pthread_mutex_init is %d\n", error);
			exit(-1);
	}
	
	for(i = 0; i < 5; i++){		//Cria-se as threads
		args[i] = i;
		error = pthread_create(&threads[i], NULL, thread_func, (void*)&args[i]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	}
	
	for(i = 0; i < 5; i++){		//Espera que uma thread especifica termine, bloqueando a thread invocadora
		void * ret = NULL;
		error = pthread_join(threads[i], (void*)&ret);
		if(error){
			printf("ERRO: Return code from pthread_join is %d\n", error);
			exit(-1);
		}
	}
	
	// Destruir o mutex.
	error = pthread_mutex_destroy(&mux);
	if(error){
		printf("ERRO: Return code from pthread_mutex_destroy is %d\n", error);
		exit(-1);
	}
	
	printf("O saldo total e de: %d\n", saldoTotal);
	
	return 0;
}
