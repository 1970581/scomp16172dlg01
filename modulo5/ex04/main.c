#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//#include <time.h>  		// usada pelo random.

//Bibliotecas de memoria partilhada
//#include <sys/mman.h>
//#include <sys/stat.h>
//#include <sys/types.h>
//#include <fcntl.h>

//Bibliotes de semaforos
//#include <semaphore.h>

// Biblioteca de threads.
#include <pthread.h>

#define MAX_NORMAL 49
#define MAX_SORTE 13
#define MIN_NUMERO 1
#define VECTOR_SIZE 10000
#define NUM_THREAD 10


//*******************************************
//  IMPORTANTE!!!!!
//********
// Chaves 5 numeros de 1 a 49 + 1 numero da sorte de 1 a 13.
// 10 threads.
// vector de chaves global
// vector de resultados locais
// Acesso independente ao vector de chaves globais, de apenas leitura. Não existe interferencia.
// Ultima operacao da thread sera transferir os resultados locais para um vector de resultados globais. (conforme sugestao do prof das aulas TP's)
// Este acesso e regulado por um MUTEX. 
//
// A sugestão de utilizacao de um vector de variaveis de exclusao mutua, seria mais complexa e obrigaria a criacao de 62 mutexs.
//  de forma a bater a performance de usar resultados locais.


typedef struct {  // Zona de memoria partilhada
	int 	numero_da_thread;
} parametroEnvio;


typedef struct {  // Representa uma chaves
	int 	normal[5];		// 1-49 x5
	int 	sorte;			// 1-13  numero da sorte.
} chave;

chave g_vetorChaves[VECTOR_SIZE];	// Vector das 10 000 chaves.

pthread_t threads[NUM_THREAD];	// Vector dos id das threads criadas.

parametroEnvio envio[NUM_THREAD] ;	// Vector dos parametros a passar as threads.

int g_normal[MAX_NORMAL +1 ];		// Vector com o somatorio dos resultados de todas as threads.
int g_sorte[MAX_SORTE +1];		// Vector com o somatorio dos resultados de todas as threads.


// MUTEXES
pthread_mutex_t mux;




// FUNCAO DAS THREADS
// LE uma fracao do vector de chaves global, e incrementa o vector dos resultados locais comforme os resultados.
// Os resultados locais guarda quantas vezes o indice do vector se encontra nas chaves.
void* thread_func(void *arg){




	int ii = 0;
	int jj = 0;
	int vec_size2 = VECTOR_SIZE ;	
	int numeroDeThreads = NUM_THREAD ;
	parametroEnvio * parametros = (parametroEnvio*) arg;
	int threadNumber = parametros->numero_da_thread ;

	//printf("Thread[%i]\n", threadNumber);
	threadNumber--;  		// Passar de 1-10 para 0-9;  // Correcao do numero da thread para incluir o zero.

	int maxNormal2 = MAX_NORMAL ;
	int maxSorte2 = MAX_SORTE ;
	
	int localNormal[maxNormal2 +1];		// Vector dos resultados locais numeros normais.  0-49  , o +1 evita terminar no [48]
	int localSorte[maxSorte2 +1];		// Vector dos resultados locais numeros da sorte  0-13


	//SUPER IMPORTANTE - Inicializar os vectores. Estao cheios de lixo.
	int lt = 0;
	int debug = 0;  
//	debug = 1;	//Descomentar para ver o lixo.	
	if(debug){
		for ( lt = 0 ; lt < (maxNormal2 +1); lt++) printf("%i ", localNormal[lt]);
		for ( lt = 0 ; lt < (maxSorte2 +1); lt++) printf(" %i ", localSorte[lt]);
	}
	//Limpeza, inicializar a 0.
	for ( lt = 0 ; lt < (maxNormal2 +1); lt++) localNormal[lt] = 0;
	for ( lt = 0 ; lt < (maxSorte2 +1); lt++) localSorte[lt] = 0;


	int vecMin = threadNumber * (vec_size2 / numeroDeThreads);	//   0 * ( 10000 / 10) = 0 ou 1000 ou 2000 ou 9000
	int vecMax = vecMin + (vec_size2 / numeroDeThreads);		//   0 + ( 10000 / 10) = 1000 ou 2000 ou 10000
	int valorLido = 0;


	//printf("T:%i min:%i max:%i \n", threadNumber, vecMin , vecMax );
	//if(threadNumber != 11) pthread_exit((void*)NULL);	

	for(ii = vecMin; ii < vecMax; ii++){	// 0 a 1000 ou 9000 a 10000
		for(jj = 0; jj < 5;jj++){
			valorLido = g_vetorChaves[ii].normal[jj]	;
			localNormal[valorLido] = localNormal[valorLido] + 1;
		}
		valorLido = g_vetorChaves[ii].sorte; 			
		localSorte[valorLido]++;
	}

	// ZONA CRITICA
	pthread_mutex_lock(&mux);
		int a = 0;
		for(a = 0; a < (maxNormal2 +1); a++){
			g_normal[a] 	= g_normal[a] + localNormal[a];  
		}

		for(a = 0; a < (maxSorte2 +1); a++){
			g_sorte[a] 	+= localSorte[a];
		}

	pthread_mutex_unlock(&mux);
	

	
	pthread_exit((void*)NULL);		//Termina a thread
}






//  <<< MAIN >>>
int main(int argc, char *argv[]) { 
	int error;		//Diagnostico dos processos de partilha de memoria
//	size_t pid ;
	int i = 0;
	int j = 0;
	
	//  *************** Criacao de MUTEX
	error = pthread_mutex_init( &mux, NULL );
	if(error){
			printf("ERRO: Return code from pthread_mutex_init is %d\n", error);
			exit(-1);
		}

	// Inicialização dos vectores respostas.
	for (i = 0; i < (MAX_NORMAL +1) ; i++) g_normal[i] = 0;
	for (i = 0; i < (MAX_SORTE +1) ; i++) g_normal[i] = 0;
	// Interesante, o standard de C pelos vistos indica que vectores globais veem a zero, e que os locais podem conter lixo.
	// https://www.quora.com/C-programming-language-Why-does-the-array-declared-inside-the-main-function-contain-garbage-values-by-default-but-the-array-declared-globally-are-initialized-to-0-value-by-default


	// Preenchimento do vector de parametros a passar as threads.
	for (i = 0; i < NUM_THREAD ;i++){
		envio[i].numero_da_thread = i +1;		//Passagem apenas do numero da thread.
	}

	// Preenche o vector com todas as chaves 10 000 vezes
	int vec_size 	= VECTOR_SIZE ;
	int base 	= MIN_NUMERO;	//  1 
	int maxNormal 	= MAX_NORMAL;	// 49
	int maxSorte  	= MAX_SORTE ;	// 13
	int numero 	= base;
	int sorte 	= base;

	for(i = 0; i < vec_size; i++){		
		
		for(j = 0; j < 5;j++){
			if(numero > maxNormal ) numero = base;		// 1-49  x5
			g_vetorChaves[i].normal[j] = numero;
			numero++;
		}
			if(sorte > maxSorte) sorte = base;		// 1-13
			g_vetorChaves[i].sorte = sorte;
			sorte++;					//printf("%i ", sorte);
	}
	
	
	// Criacao das threads 
	int numeroThreads = NUM_THREAD ;	
	for(i = 0; i < numeroThreads; i++){		
		error = pthread_create(&threads[i], NULL, thread_func, (void*) &envio[i]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	}


	//Esperar que todas as threads filhas terminem	
	void * ret;
	for(i = 0; i < numeroThreads; i++){		
		ret = NULL;
		error = pthread_join(threads[i], (void*)&ret);
		if(error){
			printf("ERRO: Return code from pthread_join is %d\n", error);
			exit(-1);
		}
		
		// Se tivermos de ler a resposta, aqui copiamos o endresso contido em ret   
		// para um vector de "respostas".
		
		/*  VAZIO */
		
	}

	int frequencia = 0;
	printf("Frequencias de numeros normais: \n");
	for (i = 1 ; i < maxNormal +1 ; i++){
		frequencia = g_normal[i];
		printf("%i : %i x \n", i , frequencia);
	}
	for (i = 1 ; i < maxSorte; i++){
		frequencia = g_sorte[i];
		printf("%i : %i x \n", i , frequencia);
	}


	// Fecho.   =========================	
	
	// Destruir o mutex.
	error = pthread_mutex_destroy(&mux);
	if(error){
		printf("ERRO: Return code from pthread_mutex_destroy is %d\n", error);
		exit(-1);
	}


	pthread_t 	myThread = pthread_self();
	pid_t  		myPID    = getpid();

	printf("Main PID[%i] Thread[%i]: terminado.\n", myPID ,(int) myThread);

	return 0;
}


	
	

