#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//Biblioteca de threads
#include <pthread.h>

#define COMPRIMENTO_VETOR 1000		//Constante do comprimento do vetor
pthread_mutex_t mux[4];				//Mutex da thread

int resultado[COMPRIMENTO_VETOR];			//Variavel global de um vetor de inteiro
int dados[COMPRIMENTO_VETOR];

void* thread_func(void *arg){
	int value = *((int*)arg);	//O Value vai ser utilizado para saber o numero da thread
	int j = 0;					//IMPORTANTE: INICIALIZAR AS VARIAVEIS SENAO NAO FUNCIONA
	
	for(j = value * (COMPRIMENTO_VETOR/5); j < (value * (COMPRIMENTO_VETOR/5)) + 200; j++){
		resultado[j] = dados[j] * 2 + 10;		//Calcula e imprime o resultado de 200 a 200 posicoes
	}
	
	
	// ZONA CRITICA				//Primeira situacao: Imprime logo os resultados da thread 0
	if(value != 0){				//A thread 1,2,3 e 4 bloqueiam as mutexes 0,1,2 e 3
		pthread_mutex_lock(&mux[value-1]);
	}
	
	
	//COMO FUNCIONA:
	//Thread 0 : resultado[0] ate resultado[199]   - No fim desbloqueia o mutex 0 o que permite imprimir os resultados da thread 1
	//Thread 1 : resultado[200] ate resultado[399] - No fim desbloqueia o mutex 1 o que permite imprimir os resultados da thread 2
	//Thread 2 : resultado[400] ate resultado[599] - No fim desbloqueia o mutex 2 o que permite imprimir os resultados da thread 3
	//Thread 3 : resultado[600] ate resultado[799] - No fim desbloqueia o mutex 3 o que permite imprimir os resultados da thread 4
	//Thread 4 : resultado[800] ate resultado[999] - Como ja imprimiu os valores finais nao e preciso desbloquear mais mutexes e termina
	
	printf("Numero da thread: %d\n", value);	//Print para saber o numero da thread
	for(j = value * (COMPRIMENTO_VETOR/5); j < (value * (COMPRIMENTO_VETOR/5)) + 200; j++){
		printf("resultado[%d]: %d\n", j , resultado[j]);
	}
	
	if(value != 4){
		pthread_mutex_unlock(&mux[value]);
	}
	
	
	pthread_exit((void*)NULL);		//Termina a thread
}

int main(int argc, char *argv[]){
	pthread_t threads[5];		//Inicializa 5 threads
	int i, args[5];
	int error;
	
	for(i = 0; i < COMPRIMENTO_VETOR; i++){		//Preenche os vetores
		resultado[i] = 0;
		dados[i] = i;
	}
	
	for(i = 0; i < 4; i++){
	error = pthread_mutex_init(&mux[i], NULL);	//Cria-se os mutexes
		if(error){
			printf("ERRO: Return code from pthread_mutex_init is %d\n", error);
			exit(-1);
		}
	}

	for(i = 0; i < 5; i++){		//Cria-se as threads
		if(i != 4){				//Bloqueia-se logo os mutexes todos
			pthread_mutex_lock(&mux[i]);
		}	
		args[i] = i;
		error = pthread_create(&threads[i], NULL, thread_func, (void*)&args[i]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	}
	
	for(i = 0; i < 5; i++){		//Espera que uma thread especifica termine, bloqueando a thread invocadora
		void * ret = NULL;
		error = pthread_join(threads[i], (void*)&ret);
		if(error){
			printf("ERRO: Return code from pthread_join is %d\n", error);
			exit(-1);
		}
	}
	
	for(i = 0; i < 5; i++){
		if(i != 4){		//Desbloqueia-se logo os mutexes todos depois de imprimir os resultados
			pthread_mutex_unlock(&mux[i]);
		}
	}
	
	// Destruir os mutexes
	for(i = 0; i < 4; i++){
		error = pthread_mutex_destroy(&mux[i]);
		if(error){
			printf("ERRO: Return code from pthread_mutex_destroy is %d\n", error);
			exit(-1);
		}
	}
	
	
	return 0;
}
