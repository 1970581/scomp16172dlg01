#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Biblioteca de threads
#include <pthread.h>

#define LINHA_AB 0		//
#define LINHA_BC 1		//ESTAS 3 CONSTANTES VAO SER UTILIZADAS DURANTE AS THREADS
#define LINHA_BD 2		//

pthread_mutex_t mux[3];				//Mutexes da thread	(LINHAS DE COMBOIO)

char* getStringHour() {
	time_t epoch_time;
	epoch_time = time(NULL);
	struct tm *tm_struct = localtime(&epoch_time);
	char* actualTime = (char *) malloc(25 * sizeof(char));
 
	sprintf(actualTime, "%d:%d:%d", tm_struct->tm_hour, tm_struct->tm_min, tm_struct->tm_sec);
	return actualTime;
}

void* viagem_AD(void *arg){
	int numComboio = *((int*)arg);	//Numero do comboio
	char * horaPartida = getStringHour();
	
	pthread_mutex_lock(&mux[LINHA_AB]);
	printf("O comboio %d da origem da cidade A com destino a cidade D encontra-se na linha AB\n", numComboio);
	sleep(5);
	pthread_mutex_unlock(&mux[LINHA_AB]);
	
	
	pthread_mutex_lock(&mux[LINHA_BD]);
	printf("O comboio %d da origem da cidade A com destino a cidade D encontra-se na linha BD\n", numComboio);
	sleep(5);
	pthread_mutex_unlock(&mux[LINHA_BD]);
	
	char * horaChegada = getStringHour();
	
	printf("Terminou a viagem - Hora de partida: %s Hora de chegada: %s\n", horaPartida, horaChegada);
	
	pthread_exit((void*)NULL);		//Termina a thread
}

void* viagem_CA(void *arg){
	int numComboio = *((int*)arg);	//Numero do comboio
	char * horaPartida = getStringHour();
	
	pthread_mutex_lock(&mux[LINHA_BC]);
	printf("O comboio %d da origem da cidade C com destino a cidade A encontra-se na linha BC\n", numComboio);
	sleep(5);
	pthread_mutex_unlock(&mux[LINHA_BC]);
	
	pthread_mutex_lock(&mux[LINHA_AB]);
	printf("O comboio %d da origem da cidade C com destino a cidade A encontra-se na linha AB\n", numComboio);
	sleep(5);
	pthread_mutex_unlock(&mux[LINHA_AB]);
	
	char * horaChegada = getStringHour();
	printf("Terminou a viagem - Hora de partida: %s Hora de chegada: %s\n", horaPartida, horaChegada);
	
	pthread_exit((void*)NULL);		//Termina a thread
}



int main(int argc, char *argv[]){
	pthread_t threads[2];		//Inicializa 2 threads (Viagens)
	int i, args[2];
	int error;
	
	for(i = 0; i < 3; i++){
	error = pthread_mutex_init(&mux[i], NULL);	//Cria-se os mutexes (Linhas de comboio)
		if(error){
			printf("ERRO: Return code from pthread_mutex_init is %d\n", error);
			exit(-1);
		}
	}
	
	//As threads sao criadas individualmente (Os args vao ser os numeros de comboio para uma viagem)
	args[0] = 84;
	args[1] = 88;
	error = pthread_create(&threads[0], NULL, viagem_AD, (void*)&args[0]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	
	error = pthread_create(&threads[1], NULL, viagem_CA, (void*)&args[1]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	
	for(i = 0; i < 2; i++){		//Espera que uma thread especifica termine, bloqueando a thread invocadora
		void * ret = NULL;
		error = pthread_join(threads[i], (void*)&ret);
		if(error){
			printf("ERRO: Return code from pthread_join is %d\n", error);
			exit(-1);
		}
	}
	
	// Destruir os mutexes
	for(i = 0; i < 3; i++){
		error = pthread_mutex_destroy(&mux[i]);
		if(error){
			printf("ERRO: Return code from pthread_mutex_destroy is %d\n", error);
			exit(-1);
		}
	}
	
	return 0;
}
