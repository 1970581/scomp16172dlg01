#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//#include <time.h>  		// usada pelo random.

//Bibliotecas de memoria partilhada
//#include <sys/mman.h>
//#include <sys/stat.h>
//#include <sys/types.h>
//#include <fcntl.h>

//Bibliotes de semaforos
//#include <semaphore.h>

// Biblioteca de threads.
#include <pthread.h>

#define VECTOR_SIZE 15


//*******************************************
//  IMPORTANTE!!!!!
//********
// Usamos um vector (vec) com 15 posicoes, com 3 hipermacos e 5 produtos. 3 x 5 = 15.
// Usamos cada uma das threads 1 2 e 3 para copiars os valores de parte d, passando os dados de cada hipermercado para 
// o vector com o numero do hipermercado (vec1 , vec2 e vec3). 
// Usamos as thread 4 5 e 6 para trabalhar os vectores vec1 vec2 e vec3 respectivamente e obter um resultado local.
// Usamos um mutex para garantir o acesso exclusivo destas ultimas threads a variavel g_resultado onde fica o resultado global.
// Nota as ultimas threads usam outra funcao thread a  " thread_func2 ".



typedef struct {  // Zona de memoria partilhada
	int 	numero_da_thread;
} parametroEnvio;


typedef struct {  // Representa um dado
	int     hipermercado;		// hipermercado
	char 	produto;		// produto 
	float 	preco;			// preco
} dado;

dado vec[VECTOR_SIZE];	// Vector dos dados com 15 elementos.

pthread_t threads[6];	// Vector dos id das threads criadas.

parametroEnvio envio[6] ;	// Vector dos parametros a passar as threads.

dado vec1[VECTOR_SIZE];
dado vec2[VECTOR_SIZE];
dado vec3[VECTOR_SIZE];

dado g_resultado;

// MUTEXES
pthread_mutex_t mux;
pthread_mutex_t mutexT[3];



// FUNCAO DAS THREADS
void* thread_func(void *arg){

	int i = 0;

	parametroEnvio * parametros = (parametroEnvio*) arg;

	int threadNumber = parametros->numero_da_thread ;

	//int threadNumber = envio->numero_da_thread ;
	//printf("Thread[%i]\n", threadNumber);
	threadNumber--;  		// Passar de 1-10 para 0-9;  // Correcao do numero da thread para incluir o zero.
	
	int hipermercado = 0;
	char produto = ' ';
	float preco = 0;
	dado * vecAux = NULL;
	int error = 0;

	int block = 5;	//tamanho do bloco que cada thread le do vector., 5 posicoes de 15.
	for( i = threadNumber * block ; i < (threadNumber * block + block ) ; i++){
		hipermercado = vec[i].hipermercado;
		produto = vec[i].produto;
		preco = vec[i].preco;

		if(hipermercado == 1) vecAux = vec1;  // selecao do vector onde escrever.
		if(hipermercado == 2) vecAux = vec2;
		if(hipermercado == 3) vecAux = vec3;

		error = pthread_mutex_lock(&mutexT[hipermercado]); if(error) {printf("Error in T%i mutex lock\n", threadNumber); exit(-1);}

			vecAux[i].hipermercado = hipermercado;
			vecAux[i].produto = produto;
			vecAux[i].preco = preco;

		error = pthread_mutex_unlock(&mutexT[hipermercado]); if(error) {printf("Error in T%i mutex unlock\n", threadNumber); exit(-1);}

	
	
	}
	
	pthread_exit((void*)NULL);		//Termina a thread
}

//=========================================0
// Funcao de thread 2 , para thread 4 , 5 e 6 , calcular o hipermercado com preco mais baixo.
void* thread_func2(void *arg){
	
	int i = 0;

	parametroEnvio * parametros = (parametroEnvio*) arg;

	int threadNumber = parametros->numero_da_thread ;
	int hipermercado = threadNumber - 3 ;
	
	// Selecionar o vector que vamos usar, segundo o numero do hipermercado.
	dado * vecAux = NULL;
	if(hipermercado == 1) vecAux = vec1;
	if(hipermercado == 2) vecAux = vec2;
	if(hipermercado == 3) vecAux = vec3;
	
	//Obter a soma total para o nosso hipermercado.
	float total = 0;
	int length = VECTOR_SIZE;
	for(i = 0 ; i < length; i++){
		if(vecAux[i].hipermercado != 0){  // temos 5 produtos, mas existem posicoes nao preenchidas (mas inicializadas )
			total += vecAux[i].preco;
		}
	}

	//if(hipermercado == 3) sleep(2);

	// ZONA CRITICA  -- Escrita no vector global.
	pthread_mutex_lock(&mux);
		//     se ainda nao tiver sido escrito um resultado OU existe um resultado E é maior que o nosso.
		if( g_resultado.hipermercado == 0 || (g_resultado.hipermercado != 0 && g_resultado.preco > total) ){
			g_resultado.hipermercado = hipermercado;
			g_resultado.preco = total;		printf("---\n");
		}
		printf("Zona critica: T[%i] hiper:%i total:%f \n", threadNumber,hipermercado , total );

	pthread_mutex_unlock(&mux);
	

	pthread_exit((void*)NULL);		//Termina a thread
}





//  <<< MAIN >>>
int main(int argc, char *argv[]) { 
	int error;		//Diagnostico dos processos de partilha de memoria
	size_t pid ;
	int i = 0;
	int j = 0;
	pid = getpid();
	printf("Main PID: %i \n", pid);

	
	//  *************** Criacao de MUTEX
	error = pthread_mutex_init( &mux, NULL );
	if(error){
			printf("ERRO: Return code from pthread_mutex_init is %d\n", error);
			exit(-1);
		}

	for (j = 0; j < 3; j++){
		error = pthread_mutex_init( &mutexT[j] , NULL);
		if(error) printf("ERRO: Return code from pthread_mutex_init is %d\n", error);
	}


	// Inicialização dos vectores respostas.
	for (i = 0; i < 15 ; i++){
		 vec[i].hipermercado = 0;
		 vec[i].produto = ' ';
		 vec[i].preco = 0;	
		 vec1[i].hipermercado = 0;
		 vec1[i].produto = ' ';
		 vec1[i].preco = 0;	
		 vec2[i].hipermercado = 0;
		 vec2[i].produto = ' ';
		 vec2[i].preco = 0;	
		 vec3[i].hipermercado = 0;
		 vec3[i].produto = ' ';
		 vec3[i].preco = 0;	
	}
	
	// Inicializacao dos resultados global.
	g_resultado.hipermercado = 0;
	g_resultado.produto = 'T';
	g_resultado.preco = 0;


	int index = 0;
	
	// PReencher o vector inicial vec. x15
	index = 0 ; vec[index].hipermercado = 1 ; vec[index].produto = 'A' ; vec[index].preco = 10.0f ;
	index = 1 ; vec[index].hipermercado = 1 ; vec[index].produto = 'B' ; vec[index].preco = 20.0f ;
	index = 2 ; vec[index].hipermercado = 1 ; vec[index].produto = 'C' ; vec[index].preco = 30.0f ;
	index = 3 ; vec[index].hipermercado = 1 ; vec[index].produto = 'D' ; vec[index].preco = 40.0f ;
	index = 4 ; vec[index].hipermercado = 1 ; vec[index].produto = 'E' ; vec[index].preco = 50.0f ;
	index = 5 ; vec[index].hipermercado = 2 ; vec[index].produto = 'A' ; vec[index].preco = 11.0f ;
	index = 6 ; vec[index].hipermercado = 2 ; vec[index].produto = 'B' ; vec[index].preco = 21.0f ;
	index = 7 ; vec[index].hipermercado = 2 ; vec[index].produto = 'C' ; vec[index].preco = 31.0f ;
	index = 8 ; vec[index].hipermercado = 2 ; vec[index].produto = 'D' ; vec[index].preco = 41.0f ;
	index = 9 ; vec[index].hipermercado = 2 ; vec[index].produto = 'E' ; vec[index].preco = 51.0f ;
	index = 10 ; vec[index].hipermercado = 3 ; vec[index].produto = 'A' ; vec[index].preco = 12.0f ;
	index = 11 ; vec[index].hipermercado = 3 ; vec[index].produto = 'B' ; vec[index].preco = 22.0f ;
	index = 12 ; vec[index].hipermercado = 3 ; vec[index].produto = 'C' ; vec[index].preco = 32.0f ;
	index = 13 ; vec[index].hipermercado = 3 ; vec[index].produto = 'D' ; vec[index].preco = 42.0f ;
	index = 14 ; vec[index].hipermercado = 3 ; vec[index].produto = 'E' ; vec[index].preco = 52.0f ;





	// Preenchimento do vector de parametros a passar as threads.
	for (i = 0; i < 6 ;i++){
		envio[i].numero_da_thread = i +1;		//Passagem apenas do numero da thread.
	}

	
	
	// Criacao das threads 1 2 e 3
	int numeroThreads = 3 ;	
	for(i = 0; i < numeroThreads; i++){		
		error = pthread_create(&threads[i], NULL, thread_func, (void*) &envio[i]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	}


	//Esperar que todas as threads filhas terminem	1 2 e 3
	void * ret;
	for(i = 0; i < numeroThreads; i++){		
		ret = NULL;
		error = pthread_join(threads[i], (void*)&ret);
		if(error){
			printf("ERRO: Return code from pthread_join is %d\n", error);
			exit(-1);
		}
		
		// Se tivermos de ler a resposta, aqui copiamos o endresso contido em ret   
		// para um vector de "respostas".
		
		/*  VAZIO */
		
	}

	// Criacao das threads 4 5 e 6
	numeroThreads = 3 ;	
	int offset = 3;
	for(i = 0 + offset; i < numeroThreads + offset ; i++){		
		error = pthread_create(&threads[i], NULL, thread_func2, (void*) &envio[i]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	}


	//Esperar que todas as threads filhas terminem	4 5 e 6
	//void * ret;
	for(i = 0 + offset; i < numeroThreads + offset; i++){		
		ret = NULL;
		error = pthread_join(threads[i], (void*)&ret);
		if(error){
			printf("ERRO: Return code from pthread_join is %d\n", error);
			exit(-1);
		}
		
		// Se tivermos de ler a resposta, aqui copiamos o endresso contido em ret   
		// para um vector de "respostas".
		
		/*  VAZIO */
		
	}


	// Output dos resultados.
	printf("\n===================================================================\n");
	printf("||  Main thread: Hipermercado mais barato %i com custo de %.2f  ||\n", g_resultado.hipermercado , g_resultado.preco );
	printf("===================================================================\n");

	// Fecho.   =========================	
	
	// Destruir o mutex.
	error = pthread_mutex_destroy(&mux);
	if(error){
		printf("ERRO: Return code from pthread_mutex_destroy is %d\n", error);
		exit(-1);
	}
	for(i = 0 ; i < 3; i++){
		error = pthread_mutex_destroy(&mutexT[i]);
		if(error) { printf("ERRO: Return code from pthread_mutex_destroy is %d\n", error); exit(-1);}
	}




	pthread_t 	myThread = pthread_self();
	pid_t  		myPID    = getpid();

	printf("Main PID[%i] Thread[%i]: terminado.\n", myPID ,(int) myThread);

	return 0;
}


	
	

