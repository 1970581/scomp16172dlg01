#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Biblioteca de threads
#include <pthread.h>

//Estas 21 constantes vao ser utilizadas para identificar as mutexes e usar como percurso respetivo
#define CAMINHO_0_1 0		
#define CAMINHO_0_2 1
#define CAMINHO_0_3 2
#define CAMINHO_1_4 3
#define CAMINHO_2_4 4
#define CAMINHO_3_4 5
#define CAMINHO_4_5 6
#define CAMINHO_5_6 7
#define CAMINHO_5_7 8
#define CAMINHO_6_8 9
#define CAMINHO_7_8 10
#define CAMINHO_7_9 11
#define CAMINHO_8_9 12
#define CAMINHO_9_10 13
#define CAMINHO_9_13 14
#define CAMINHO_10_11 15
#define CAMINHO_10_12 16
#define CAMINHO_11_15 17
#define CAMINHO_12_15 18
#define CAMINHO_13_14 19
#define CAMINHO_14_15 20

unsigned long tempoCaracol1;
unsigned long tempoCaracol2;
unsigned long tempoCaracol3;

pthread_mutex_t mux[21];		//Mutexes da thread	(PERCURSOS)
int checkpoint[21];				//Checkpoints para cada um dos percursos (flag). Caso um caracol passe a flag e incrementada		
								//e o proximo caracol vai demorar o dobro do tempo e assim sucessivamente

//Metodo "esqueleto" quando o caracol estiver a correr
void caracol_a_correr(int numero, int caminho, int segundos){
	pthread_mutex_lock(&mux[caminho]);	//Bloqueia o mutex respetivo
	
	if(checkpoint[caminho] == 1){	//So aumenta quando ja tinha passado um caracol nesse percurso respetivo
		segundos = segundos * 2;
	}
	if(checkpoint[caminho] == 2){	//So aumenta quando ja tinha passado dois caracol nesse percurso respetivo
		segundos = segundos * 4;
	}
	
	printf("Caracol %d esta a percorrer...\n", numero);		//Caracol esta a correr
	sleep(segundos);										//Simular um caracol a correr
	checkpoint[caminho]++;									//Incrementa o checkpoint quando um caracol passou no percurso respetivo
	
	pthread_mutex_unlock(&mux[caminho]);	//Desbloqueia o mutex respetivo
}

void* caracol_1(void *arg){
	int numCaracol = *((int*)arg);	//Numero do caracol
	time_t inicio, fim;
	unsigned long tempo = 0;
	inicio = time(NULL);
	
	//START
	caracol_a_correr(numCaracol, CAMINHO_0_1, 5);
	caracol_a_correr(numCaracol, CAMINHO_1_4, 4);
	caracol_a_correr(numCaracol, CAMINHO_5_6, 5);
	caracol_a_correr(numCaracol, CAMINHO_6_8, 6);
	caracol_a_correr(numCaracol, CAMINHO_8_9, 4);
	caracol_a_correr(numCaracol, CAMINHO_9_10, 5);
	caracol_a_correr(numCaracol, CAMINHO_10_11, 5);
	caracol_a_correr(numCaracol, CAMINHO_11_15, 3);
	//FINISH
	
	fim = time(NULL);
	tempo = difftime(fim, inicio);
	printf("Tempo total do caracol %d: %lu segundos\n", numCaracol, tempo);
	tempoCaracol1 = tempo;
	pthread_exit((void*)NULL);		//Termina a thread
}

void* caracol_2(void *arg){
	int numCaracol = *((int*)arg);	//Numero do caracol
	time_t inicio, fim;
	unsigned long tempo = 0;
	inicio = time(NULL);
	
	//START
	caracol_a_correr(numCaracol, CAMINHO_0_2, 5);
	caracol_a_correr(numCaracol, CAMINHO_2_4, 4);
	caracol_a_correr(numCaracol, CAMINHO_4_5, 3);
	caracol_a_correr(numCaracol, CAMINHO_5_7, 5);
	caracol_a_correr(numCaracol, CAMINHO_7_8, 6);
	caracol_a_correr(numCaracol, CAMINHO_8_9, 4);
	caracol_a_correr(numCaracol, CAMINHO_9_13, 5);
	caracol_a_correr(numCaracol, CAMINHO_13_14, 5);
	caracol_a_correr(numCaracol, CAMINHO_14_15, 3);
	//FINISH
	
	fim = time(NULL);
	tempo = difftime(fim, inicio);
	printf("Tempo total do caracol %d: %lu segundos\n", numCaracol, tempo);
	tempoCaracol2 = tempo;
	pthread_exit((void*)NULL);		//Termina a thread
}

void* caracol_3(void *arg){
	int numCaracol = *((int*)arg);	//Numero do caracol
	time_t inicio, fim;
	unsigned long tempo = 0;
	inicio = time(NULL);
	
	//START
	caracol_a_correr(numCaracol, CAMINHO_0_3, 5);
	caracol_a_correr(numCaracol, CAMINHO_3_4, 4);
	caracol_a_correr(numCaracol, CAMINHO_4_5, 3);
	caracol_a_correr(numCaracol, CAMINHO_5_7, 5);
	caracol_a_correr(numCaracol, CAMINHO_7_9, 4);
	caracol_a_correr(numCaracol, CAMINHO_9_10, 5);
	caracol_a_correr(numCaracol, CAMINHO_10_12, 5);
	caracol_a_correr(numCaracol, CAMINHO_12_15, 3);
	//FINISH
	
	fim = time(NULL);
	tempo = difftime(fim, inicio);
	printf("Tempo total do caracol %d: %lu segundos\n", numCaracol, tempo);
	tempoCaracol3 = tempo;
	pthread_exit((void*)NULL);		//Termina a thread
}

int main(int argc, char *argv[]){
	pthread_t threads[3];		//Inicializa 3 threads (Caracois)
	int i, args[3];
	int error;
	
	for(i = 0; i < 21; i++){	//Preenche os checkpoints (flag) a 0
		checkpoint[i] = 0;
	}
	
	for(i = 0; i < 21; i++){
	error = pthread_mutex_init(&mux[i], NULL);	//Cria-se os mutexes (Percursos)
		if(error){
			printf("ERRO: Return code from pthread_mutex_init is %d\n", error);
			exit(-1);
		}
	}
	
	//As threads sao criadas individualmente (Os args vao ser os numeros dos caracois para as threads respetivas)
	args[0] = 1;
	args[1] = 2;
	args[2] = 3;
	error = pthread_create(&threads[0], NULL, caracol_1, (void*)&args[0]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	
	error = pthread_create(&threads[1], NULL, caracol_2, (void*)&args[1]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	
	error = pthread_create(&threads[2], NULL, caracol_3, (void*)&args[2]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	
	for(i = 0; i < 3; i++){		//Espera que uma thread especifica termine, bloqueando a thread invocadora
		void * ret = NULL;
		error = pthread_join(threads[i], (void*)&ret);
		if(error){
			printf("ERRO: Return code from pthread_join is %d\n", error);
			exit(-1);
		}
	}
	
	// Destruir os mutexes
	for(i = 0; i < 21; i++){
		error = pthread_mutex_destroy(&mux[i]);
		if(error){
			printf("ERRO: Return code from pthread_mutex_destroy is %d\n", error);
			exit(-1);
		}
	}
	
	// Classificacao final da corrida
	
	printf("CLASSIFICACAO FINAL: \n");
	if(tempoCaracol1 < tempoCaracol2 && tempoCaracol1 < tempoCaracol3){
		if(tempoCaracol2 < tempoCaracol3){
			printf("1 lugar - Caracol 1\n");
			printf("2 lugar - Caracol 2\n");
			printf("3 lugar - Caracol 3\n");
		} else {
			printf("1 lugar - Caracol 1\n");
			printf("2 lugar - Caracol 3\n");
			printf("3 lugar - Caracol 2\n");
		}		
	}
	if(tempoCaracol2 < tempoCaracol1 && tempoCaracol2 < tempoCaracol3){
		if(tempoCaracol1 < tempoCaracol3){
			printf("1 lugar - Caracol 2\n");
			printf("2 lugar - Caracol 1\n");
			printf("3 lugar - Caracol 3\n");
		} else {
			printf("1 lugar - Caracol 2\n");
			printf("2 lugar - Caracol 3\n");
			printf("3 lugar - Caracol 1\n");
		}
	}
	if(tempoCaracol3 < tempoCaracol1 && tempoCaracol3 < tempoCaracol2){
		if(tempoCaracol1 < tempoCaracol2){
			printf("1 lugar - Caracol 3\n");
			printf("2 lugar - Caracol 1\n");
			printf("3 lugar - Caracol 2\n");
		} else {
			printf("1 lugar - Caracol 3\n");
			printf("2 lugar - Caracol 2\n");
			printf("3 lugar - Caracol 1\n");
		}
	}
	
	return 0;
}
