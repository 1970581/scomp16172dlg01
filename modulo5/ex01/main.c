#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//Biblioteca de threads
#include <pthread.h>

#define COMPRIMENTO_NOME 256		//Constante do comprimento do nome
#define COMPRIMENTO_MORADA 256		//Constante do comprimento da morada


typedef struct {  //Estrutura de identificacao
	int numero;
	char nome[COMPRIMENTO_NOME];
	char morada[COMPRIMENTO_MORADA];		
} identificacao;

identificacao vec[5];	//Variavel global de um vetor de identificacao

void* thread_func(void *arg){
	identificacao id = *((identificacao*)arg);	//Passagem de argumentos por referencia
	
	printf("Nome : %s\n", id.nome);				//Utiliza-se a variavel de refencia
	printf("Morada : %s\n", id.morada);
	printf("Numero : %d\n", id.numero);
	
	pthread_exit((void*)NULL);		//Termina a thread
}

int main(int argc, char *argv[]) {
	pthread_t threads[5];		//Inicializa 5 threads
	int i;
	int error;
	
	for(i = 0; i < 5; i++){		//Preenche o vetor
		strcpy(vec[i].morada, "Rua DALi");
		strcpy(vec[i].nome, "Hugo");
		vec[i].numero = i;
	}
	
	for(i = 0; i < 5; i++){		//Cria-se as threads
		error = pthread_create(&threads[i], NULL, thread_func, (void*)&vec[i]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	}
	
	for(i = 0; i < 5; i++){		//Espera que uma thread especifica termine, bloqueando a thread invocadora
		error = pthread_join(threads[i], NULL);
		if(error){
			printf("ERRO: Return code from pthread_join is %d\n", error);
			exit(-1);
		}
	}
	
	
	return 0;
}
