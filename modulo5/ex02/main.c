#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//Biblioteca de threads
#include <pthread.h>

#define COMPRIMENTO_VETOR 1000		//Constante do comprimento do vetor

int numeroProcurar = 15;
int vec[COMPRIMENTO_VETOR];			//Variavel global de um vetor de inteiro


typedef struct {  //Estrutura do parametro a passar as threads
	int numero;
	int thread;
} parthread;

parthread par[5];
int resposta;

void* thread_func(void *arg){
	parthread pari  = *((parthread*)arg);	//Passagem de argumentos por referencia
	int j = 0;
	
	
	
	
	for(j = pari.thread * (COMPRIMENTO_VETOR/5); j < 200 + (pari.thread * (COMPRIMENTO_VETOR/5)); j++){
		if(vec[j] == pari.numero){
			printf("Valor encontrado na posicao %i: \n", j);
			resposta = pari.thread +1;
			//printf("thread: %i &%i\n",r , &r );
			pthread_exit( (void*) &resposta );
		}
	}
	
	pthread_exit((void*)NULL);		//Termina a thread
}

int main(int argc, char *argv[]) {
	pthread_t threads[5];		//Inicializa 5 threads
	int i;
	int error;
	
	for(i = 0; i < COMPRIMENTO_VETOR; i++){		//Preenche o vetor
		vec[i] = i;
	}
	
	for (i=0; i < 5; i++){
		par[i].numero = numeroProcurar;
		par[i].thread = i;
	}
	
	
	for(i = 0; i < 5; i++){		//Cria-se as threads
		error = pthread_create(&threads[i], NULL, thread_func, (void*)&par[i]);
		if(error){
			printf("ERRO: Return code from pthread_create is %d\n", error);
			exit(-1);
		}
	}
	
	void * ret;
	
	for(i = 0; i < 5; i++){		//Espera que uma thread especifica termine, bloqueando a thread invocadora
		ret = NULL;
		error = pthread_join(threads[i], (void*)&ret);
		if(error){
			printf("ERRO: Return code from pthread_join is %d\n", error);
			exit(-1);
		}
		
		if(ret == NULL){
			printf("Thread[%i]Null\n", i+1);
		} else {
			//printf("r=%i\n", resposta);
			int apresentar =  *((int *)ret);
			printf("Encontrado valor na thread %i\n", apresentar);
			//printf("Encontrado valor na thread %i\n",  *((int *)ret));
		}
	}
	
	
	return 0;
}
