#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

//#include <time.h>  		// usada pelo random.

//Bibliotecas de memoria partilhada
//#include <sys/mman.h>
//#include <sys/stat.h>
//#include <sys/types.h>
//#include <fcntl.h>

//Bibliotes de semaforos
//#include <semaphore.h>

// Biblioteca de threads.
#include <pthread.h>

#define VECTOR_SIZE 300
#define DEBUG 1


//*******************************************
//  IMPORTANTE!!!!!
//********



typedef struct {  // Zona de memoria partilhada
	int 	numero_da_thread;
} parametroEnvio;


typedef struct {  // Representa uma prova de um aluno
	int     numeroAluno;
	int 	notaG1;
	int 	notaG2;
	int 	notaG3;
	int 	notaFinal;
} prova;

prova vec[VECTOR_SIZE];	// Vector das provas 300 elementos 

pthread_t threads[6];	// Vector dos id das threads criadas.

parametroEnvio envio[6] ;	// Vector dos parametros a passar as threads.

int g_lastStudent;		// numero do ultimo aluno cuja prova foi criada. Inicializa automaticamente a zero.

int g_element;			// ultimo elemento que a thread 1 preencheu no vector, vai em ordem de 0 a 299. Assosicado a variavel de condicao cond 
int g_lastElement;		// ultimo elemento que a thread 2 ou 3 fizeram a media. usado para atingir o g_element  Associado a varialve de condicao cond 
int g_p;			// numero global de positivas 
int g_n;			// numero global de negativas 
int indica_p;			// numero de positivas indicadas pelo Thread 2 ou 3.		Associado a variavel de condicao condP 
int indica_n;			// numero de negativas indicadas pelo Thread 2 ou 3.		Associado a variavel de condicao condN 

// MUTEXES
pthread_mutex_t mux;		// Mutex de exclusao mutua de acesso ao g_element e g_lastElement 
pthread_mutex_t muxExit;	// Mutex de acesso a indica_n e indica_p , g_p e g_n 


// CONDICOES
pthread_cond_t cond;	//Condicao para thread 2 e 3
pthread_cond_t condP;	// condicao para contagem de positivas 
pthread_cond_t condN;	// condicao para contagem de negativas 


// declaracao da funcao
int random_number(int max, int seed_multiplier);

void criarProva(prova * obj){
	g_lastStudent++;
	obj->numeroAluno = g_lastStudent;
	obj->notaG1 = random_number(20 , g_lastStudent + 4);
	obj->notaG2 = random_number(20 , g_lastStudent + 2);
	obj->notaG3 = random_number(20 , g_lastStudent + 12);
	obj->notaFinal = 0;
}


// FUNCAO DAS THREADS de criar as 300 provas no vector.
void* thread_func300(void *arg){
	int i = 0;

//	parametroEnvio * parametros = (parametroEnvio*) arg;
//	int threadNumber = parametros->numero_da_thread ;

	prova aux;

	int vecLength = VECTOR_SIZE;
	for (i = 0; i < vecLength ; i++){
		
		criarProva(&aux);
		vec[i].numeroAluno = aux.numeroAluno;
		vec[i].notaG1 = aux.notaG1;		
		vec[i].notaG2 = aux.notaG2;		
		vec[i].notaG3 = aux.notaG3;		
		vec[i].notaFinal = aux.notaFinal;

			pthread_mutex_lock(&mux);		//Foi criada uma prova, vamos sinalizar para ser feita a media.
				g_element++; 			// indicamos que existe mais um elemento a fazer media.
if(DEBUG)				printf("[%i] ",g_element);
				pthread_cond_signal(&cond);	// acordamos a thread 2 ou 3

			pthread_mutex_unlock(&mux);

		
	}

	
	pthread_exit((void*)NULL);		//Termina a thread
}

//=========================================0
// Funcao de thread 2 , para thread 2 e 3 , para fazer as medias.
void* thread_func2(void *arg){
	int max = VECTOR_SIZE;

	parametroEnvio * parametros = (parametroEnvio*) arg;
	int threadNumber = parametros->numero_da_thread ;


   while( 1 ){				//ciclo infinito
	pthread_mutex_lock(&mux);

		while(g_lastElement == g_element && g_lastElement < max) pthread_cond_wait(&cond, &mux); //espera enquanto nao existir media a fazer e aind nao chegarmos a 300.
		
		while(g_lastElement < g_element && g_lastElement < max){	//Faz as medias de todos que faltam fazer.

			int media = vec[g_lastElement].notaG1 + vec[g_lastElement].notaG2 + vec[g_lastElement].notaG3;
			media = media/3; 
			vec[g_lastElement].notaFinal = media;
			g_lastElement++;
			if (media >= 10) {				//Se a media positiva, sinalizamos a positiva
				pthread_mutex_lock(&muxExit);
				indica_p++;				// 
				pthread_cond_signal(&condP);
				pthread_mutex_unlock(&muxExit);
			}
			else {
				pthread_mutex_lock(&muxExit);		//sinalizacao de negativa
				indica_n++;
				pthread_cond_signal(&condN);
				pthread_mutex_unlock(&muxExit);
			}

if(DEBUG)			printf("%i_%i \n", g_lastElement, threadNumber);
		}
		if(g_lastElement >= max){		// Se ja fizemos as 300 medias saimos, MAS avisamos a outra thread, senao ela nunca termina.
			pthread_mutex_unlock(&mux);
			pthread_cond_signal(&cond);
			pthread_exit((void*)NULL);		
		}

	pthread_mutex_unlock(&mux); //pthread_cond_wait(&cond, &muxExit);

	
   }
	pthread_exit((void*)NULL);		//Termina a thread
}

void* thread_funcP(void *arg){

	while(1){
		pthread_mutex_lock(&muxExit);
			if(g_p + g_n >= 300) {pthread_mutex_unlock(&muxExit); break; }
			pthread_cond_wait(&condP, &muxExit);
			while(indica_p > g_p){		
				g_p++; 
if(DEBUG)				printf("+%i of %i ",g_p , g_p + g_n);
			}
			if(g_p + g_n >= 300) {pthread_mutex_unlock(&muxExit); pthread_cond_signal(&condN); break; }

		pthread_mutex_unlock(&muxExit);
	}

	pthread_exit((void*)NULL);		//Termina a thread
}
void* thread_funcN(void *arg){

	while(1){
		pthread_mutex_lock(&muxExit);
			if(g_p + g_n >= 300) {pthread_mutex_unlock(&muxExit); break; }
			pthread_cond_wait(&condN, &muxExit);
			while(indica_n > g_n){	
				g_n++; 
if(DEBUG)				printf("-%i of %i ",g_n , g_n + g_p);
			}	
			if(g_p + g_n >= 300) {pthread_mutex_unlock(&muxExit);pthread_cond_signal(&condP); break; }


		pthread_mutex_unlock(&muxExit);
	}

	pthread_exit((void*)NULL);		//Termina a thread
}



//  <<< MAIN >>>
int main(int argc, char *argv[]) { 
	int error;		//Diagnostico dos processos de partilha de memoria
	size_t pid ;
	int i = 0;
//	int j = 0;

	pid = getpid();
	printf("MAIN PID: %i\n", pid);
	
	//  *************** Criacao de MUTEX
	error = pthread_mutex_init( &mux, NULL );
	if(error)	{ printf("ERRO: Return code from pthread_mutex_init  %d\n", error); exit(-1);}

	error = pthread_mutex_init( &muxExit, NULL );
	if(error)	{ printf("ERRO: Return code from pthread_mutex_init  %d\n", error); exit(-1);}




	// Criacao de condicoes
	error = pthread_cond_init( &cond, NULL );
	if(error)	{ printf("ERRO: Return code from pthread_cond_init is %d\n", error); exit(-1); }
	error = pthread_cond_init( &condP, NULL );
	if(error)	{ printf("ERRO: Return code from pthread_cond_init is %d\n", error); exit(-1); }
	error = pthread_cond_init( &condN, NULL );
	if(error)	{ printf("ERRO: Return code from pthread_cond_init is %d\n", error); exit(-1); }



	// Inicialização dos vectores respostas.




	// Preenchimento do vector de parametros a passar as threads.
	for (i = 0; i < 6 ;i++){
		envio[i].numero_da_thread = i +1;		//Passagem apenas do numero da thread.
	}

	// Criacao de threads, manual visto terem funcoes diferentes.
	error = pthread_create(&threads[3], NULL, thread_funcP, (void*) &envio[3]);
	if(error)	{ printf("ERRO: Return code from pthread_create is %d\n", error); exit(-1); }
	error = pthread_create(&threads[4], NULL, thread_funcN, (void*) &envio[4]);
	if(error)	{ printf("ERRO: Return code from pthread_create is %d\n", error); exit(-1); }

	error = pthread_create(&threads[0], NULL, thread_func300, (void*) &envio[0]);
	if(error)	{ printf("ERRO: Return code from pthread_create is %d\n", error); exit(-1); }

	error = pthread_create(&threads[1], NULL, thread_func2, (void*) &envio[1]);
	if(error)	{ printf("ERRO: Return code from pthread_create is %d\n", error); exit(-1); }

	error = pthread_create(&threads[2], NULL, thread_func2, (void*) &envio[2]);
	if(error)	{ printf("ERRO: Return code from pthread_create is %d\n", error); exit(-1); }




	
	
	int numeroThreads = 5 ;	
	//Esperar que todas as threads filhas terminem	
	void * ret;
	for(i = 0; i < numeroThreads; i++){		
		ret = NULL;
		error = pthread_join(threads[i], (void*)&ret);
		if(error){
			printf("ERRO: Return code from pthread_join is %d\n", error);
			exit(-1);
		}
		printf("Main[pid:%i Thread[%i] ended.\n", pid, i+1);		

		// Se tivermos de ler a resposta, aqui copiamos o endresso contido em ret   
		// para um vector de "respostas".
		
		//  VAZIO //
		
	}

	//Resultados:
	int size = VECTOR_SIZE ;
	int ppositiva = g_p * 100 /  size ;
	int pnegativa = g_n * 100 /  size ;
	printf("Main Valores obtidos pelas threads: Positivas %i %i%c   , Negativas %i %i%c \n" , g_p , ppositiva , '%' , g_n , pnegativa, '%' );

	// Check de confirmação.
	int positivas = 0;
	int negativas = 0;
	for(i = 0; i< VECTOR_SIZE; i++){
		if(vec[i].notaFinal >= 10) positivas++; else negativas++;
	}
	printf("Main Check calculos: positivas: %i negativas: %i \n", positivas , negativas);

	// Fecho.   =========================	
	
	// Destruir o mutex.
	error = pthread_mutex_destroy(&mux);
	if(error) { printf("ERRO: Return code from pthread_mutex_destroy is %d\n", error); exit(-1);}
	error = pthread_mutex_destroy(&muxExit);
	if(error) { printf("ERRO: Return code from pthread_mutex_destroy is %d\n", error); exit(-1);}



	// Destruir as condicoes.
	error = pthread_cond_destroy( &cond);
	if(error)	{ printf("ERRO: Return code from pthread_cond_destroy is %d\n", error); exit(-1); }
	error = pthread_cond_destroy( &condP);
	if(error)	{ printf("ERRO: Return code from pthread_cond_destroy is %d\n", error); exit(-1); }
	error = pthread_cond_destroy( &condN);
	if(error)	{ printf("ERRO: Return code from pthread_cond_destroy is %d\n", error); exit(-1); }


	pthread_t 	myThread = pthread_self();
	pid_t  		myPID    = getpid();

	printf("Main PID[%i] Thread[%i]: terminado.\n", myPID ,(int) myThread);

	return 0;
}


	
	
/*
*	FUNCAO DE RANDOM
*/
int random_number(int max, int seed_multiplier){
	int valor_saida = -1;
	int max_number = max;
	//int minimum_number = 1;
	if(seed_multiplier == 0) seed_multiplier = 1;
	
	// Vamos alterar a seed to rand() com recurso a uma funcao que devolve o tempo actual.
	// Isto previne rand() de dar sempre o mesmo valor de saida.
	srand(time(NULL)*seed_multiplier);
	
	//Chamamos o rand() para obter um numero random de 0 a RAND_MAX 
	int numero_random = rand();
	
	// Dividimos o valor obtido pelo RAND_MAX para obtermos um float de 0 até 1.
	// Como em Java.
	float aux = (float) numero_random / RAND_MAX;

	// Multiplicamos por maior valor, mais 1 para obter numeros de 1 a 5.
	valor_saida = aux * (max_number) +1 ;


	//Devolvemos o valor.
	return(valor_saida);
}

