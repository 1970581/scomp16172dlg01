#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

typedef struct {
	int number;	
} shared_data_type;

int main(int argc, char *argv[]){ 
	int fd; 		//Descritor
	int error; 		//Diagnostico dos processos de partilha de memoria
	int data_size = sizeof(shared_data_type);		//Tamanho da estrutura definida
	shared_data_type *shared_data;					//Apontador de uma estrutura
	
	
	//PARTILHA DE MEMORIA - START
	fd = shm_open("/shmtest3", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);	// Abrir/criar ficheiro de memoria partilhada
	if(fd == -1){perror("SHM OPEN RETURNED -1");}
	
	error = ftruncate(fd, data_size);				// Indica o tamanho da zona de memoria
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	int i;
	int j;
	shared_data->number = 100;
	int constante = 1000;		//Numero de repeticoes
	pid_t pid;
	
	pid = fork();
	if(pid < 0) perror("Erro a fazer fork");
	
	//Codigo do filho
	if(pid == 0){
		for(i = 0; i < constante; i++){
		(shared_data->number)++;		
		(shared_data->number)--;
		printf("Processo filho %d ; Valor: %d\n", i , shared_data->number);
		}
		exit(0);
	}
	
	//Codigo do pai
	
		for(j = 0; j < constante; j++){
		(shared_data->number)++;
		(shared_data->number)--;
		printf("Processo pai %d ; Valor: %d\n", j , shared_data->number);
		}
		wait(NULL);
	
	
	
	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Nao mapea mais
	if(error == -1){perror("MUNMAP RETURNED -1");}
	
	error = close(fd);									// Fecha o descritor
	if(error == -1){perror("CLOSE RETURNED -1");}
	
	error = shm_unlink("/shmtest3");					// Fechar ficheiro de memoria partilhada
	if(error == -1){perror("UNLINK RETURNED -1");}
	
	return 0;
}

/*
 * Os resultados nem sempre vao ser consistentes pois dependem
 * das prioridades dentro do processador podendo gerar outros
 * valores (!= de 100).
 */
