#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>  		// usada pelo random.
//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

int random_number(int max, int seed_multiplier){
	int valor_saida = -1;
	int max_number = max;
	//int minimum_number = 1;
	if(seed_multiplier == 0) seed_multiplier = 1;
	
	// Vamos alterar a seed to rand() com recurso a uma funcao que devolve o tempo actual.
	// Isto previne rand() de dar sempre o mesmo valor de saida.
	srand(time(NULL)*seed_multiplier);
	
	//Chamamos o rand() para obter um numero random de 0 a RAND_MAX 
	int numero_random = rand();
	
	// Dividimos o valor obtido pelo RAND_MAX para obtermos um float de 0 até 1.
	// Como em Java.
	float aux = (float) numero_random / RAND_MAX;

	// Multiplicamos por maior valor, mais 1 para obter numeros de 1 a 5.
	valor_saida = aux * (max_number) +1 ;


	//Devolvemos o valor.
	return(valor_saida);
}

typedef struct {
	int numero[10];	
} shared_data_type;

int main(int argc, char *argv[]){
	int fd; 		//Descritor
	int error; 		//Diagnostico dos processos de partilha de memoria
	int data_size = sizeof(shared_data_type);		//Tamanho da estrutura definida
	shared_data_type *shared_data;					//Apontador de uma estrutura
	
	//PARTILHA DE MEMORIA - START
	fd = shm_open("/shmtest4", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);	// Abrir/criar ficheiro de memoria partilhada
	if(fd == -1){perror("SHM OPEN RETURNED -1");}
	
	error = ftruncate(fd, data_size);				// Indica o tamanho da zona de memoria
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	int i;
	int j;
	pid_t pid;
	int sequencia_num = 100;	//Sequencia de 100 numeros, para ler de 100 em 100 numeros
	int tamanho = 1000;			//Tamanho maximo do vetor
	int vec[tamanho];			//Vetor com o tamanho definido
	int status;					
	int globalmax = 0;			//Variavel global maxima
	
	for(i = 0; i < tamanho; i++){
		vec[i] = random_number(tamanho, i);
	}
	
	for(i = 0; i < 10; i++){
		pid = fork();
		if(pid < 0) perror("Erro a fazer fork");
		
		if(pid == 0){
			break;
		}
	}
	
	
	if(pid == 0){ //filho
		int valormax = 0;	//O contador inicia sempre a 0 para descobrir qual o valor maximo do respetivo filho
		for(j = i * sequencia_num; j < ((i * sequencia_num)+sequencia_num); j++){		
			if(vec[j] > valormax){		//Caso encontre um valor maior comparado com o valor anterior...
				valormax = vec[j];		//Armazena-o
			}
		}
		shared_data->numero[i] = valormax;		//O valor e armazenado na struct na posicao relativa ao filho
		printf("Filho - pos %d: %d\n", i, shared_data->numero[i]);		//Print para motivos de DEBUG	
		exit(0);
	}
	
	//pai		
	for(i = 0; i < 10; i++){		//O pai espera que os filhos terminem
		wait(&status);	
	}
	
	for(i = 0; i < 10; i++){
		printf("Valor max da pos %d: %d\n", i, shared_data->numero[i]);		//Imprime os valores maximos encontrados para cada posicao
		if(shared_data->numero[i] > globalmax){			//Caso encontre um valor maior comparado com o valor anterior...
			globalmax = shared_data->numero[i];			//Armazena-o
		}	
	}
	
	printf("Valor maximo global: %d\n", globalmax);		//Imprime o valor maximo global
		
	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Nao mapea mais
	if(error == -1){perror("MUNMAP RETURNED -1");}
	
	error = close(fd);									// Fecha o descritor
	if(error == -1){perror("CLOSE RETURNED -1");}
	
	error = shm_unlink("/shmtest4");					// Fechar ficheiro de memoria partilhada
	if(error == -1){perror("UNLINK RETURNED -1");}
	
	return 0; 
}
