#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#define BLOCK_READ 0	//Condicoes de espera ativa, para bloquear/permitir a leitura
#define OK_READ 1

struct info{			//Estrutura para as informacoes de cada filho
	char path[20];
	char word[20];
};

typedef struct {		//Estrutura utilizada para a partilha de memoria
	int numOccurrences[10];
	int esperaAtiva;
	struct info sInfo[10];
} shared_data_type;

int main(int argc, char *argv[]){
	int fd; 		//Descritor
	int error; 		//Diagnostico dos processos de partilha de memoria
	int data_size = sizeof(shared_data_type);		//Tamanho da estrutura definida
	shared_data_type *shared_data;					//Apontador de uma estrutura
	
	//PARTILHA DE MEMORIA - START
	fd = shm_open("/shmtest6", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);	// Abrir/criar ficheiro de memoria partilhada
	if(fd == -1){perror("SHM OPEN RETURNED -1");}
	
	error = ftruncate(fd, data_size);				// Indica o tamanho da zona de memoria
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	int i;
	pid_t pid;
	int status;
	
	for(i = 0; i < 10; i++){	//Preenche um valor default '0' o numero de ocorrencias
		shared_data->numOccurrences[i] = 0;
	}
	
	shared_data->esperaAtiva = BLOCK_READ;	//Bloquear a leitura para permitir o preenchimento de informacao
											//para cada filho
	for(i = 0; i < 10; i++){		
		pid = fork();
		if(pid < 0) perror("Erro a fazer fork");
		
		if(pid == 0){
			break;
		}
	}
	
	
	if(pid > 0){ //pai
		for(i = 0; i < 10; i++){
			char str1[] = "filhoX.txt";
			str1[5] = i + '0';	//O caracter 'X' vai ser substituido pelo numero que vai depender do nome do ficheiro de texto
			strcpy(shared_data->sInfo[i].path, str1);
			strcpy(shared_data->sInfo[i].word, "pedra");	//Todos os filhos vao procurar pela palavra 'pedra'
		}	
		shared_data->esperaAtiva = OK_READ;		//Depois de preencher as informacoes todas, o filho pode ler
	}
	
	
	if(pid == 0){ //filho
		while(shared_data->esperaAtiva != OK_READ);		//So vai ler quando a espera ativa desbloquear a leitura 'OK_READ'
		FILE *fp = fopen(shared_data->sInfo[i].path, "r");	//Abre o ficheiro respetivo
		if(fp == NULL){
			printf("Erro ao abrir o ficheiro\n");
		}
		int containsWord = 0;		//Contador de ocorrencias
		char buffer[500];
		while (fscanf(fp, " %s", buffer) == 1) {	//Condicao: A palavra a pesquisar nao pode conter mais de 500 caracteres
			if(strcmp(buffer, shared_data->sInfo[i].word) == 0){
				  containsWord++;					//Caso encontre a palavra que procura, o contador incrementa
			}
		}
		fclose(fp);		//Fecha o ficheiro respetivo
		shared_data->numOccurrences[i] = containsWord;		//Atualiza o numero de ocorrencias respetivo
		exit(0);
	}
	
	for(i = 0; i < 10; i++){
		wait(&status);			//O pai espere que os filhos terminem
	}		
	
	for(i = 0; i < 10; i++){	//Imprime o numero de ocorrencias de cada um
		printf("Numero de ocorrencias no filho %d: %d\n", i, shared_data->numOccurrences[i]);
	}
	
	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Nao mapea mais
	if(error == -1){perror("MUNMAP RETURNED -1");}
	
	error = close(fd);									// Fecha o descritor
	if(error == -1){perror("CLOSE RETURNED -1");}
	
	error = shm_unlink("/shmtest6");					// Fechar ficheiro de memoria partilhada
	if(error == -1){perror("UNLINK RETURNED -1");}
	
	return 0; 
}
