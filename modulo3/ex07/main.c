#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#define SAFETY 5	// Numero max de vezes que o pai escreve a frente do filho. Usado para nao ultrapasarmos o vector e escevermos por cima do que nao foi lido.

// Extrotura da memoria partilhada com uma extrotura aluno e um semaforo.
typedef struct {
	
	int nWrite;		//Numero de valor escritos na estrotura.
	int nRead;		//numero de valores lidos da estrotura.
	int buffer[10];		// buffer circular 
		
} shared_data_type;



int main(int argc, char *argv[]){ 
	int fd;			//Descritor da shared memory 
	int error;		//Diagnostico dos processos de partilha de memoria
	int data_size = sizeof(shared_data_type);	//Tamanho da estrutura definida

	shared_data_type *shared_data;				//Apontador de uma estrutura de memoria partilhada.
	int i = 0;
	int localNRead = 0;
	int localNWrite = 0;
	int valor = 0;
	int posicao = 0;
	
	
	//PARTILHA DE MEMORIA - START
	fd = shm_open("/shmtest", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);	// Abrir/criar ficheiro de memoria partilhada
	if(fd == -1){perror("SHM OPEN RETURNED -1 \nTYPE:\n> rm /dev/dhm/shmtest\n ");}
	
	error = ftruncate(fd, data_size);			// Indica o tamanho da zona de memoria					
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	
	//CODIGO
	
	shared_data->nRead = 0;	 // Inicializacao da extrotura. 	
	shared_data->nWrite = 0; // Garantir que nao existe lixo na extrotura.

	//Criar processo filho 
	size_t pid = fork();
	if(pid < 0) {perror("Erro a fazer fork.\n"); exit(1);}
	else{printf("%s[%i] em concorrencia.\n", (pid > 0) ? "PAI  " : "Filho" , getpid() );}

	//PAI
	if(pid > 0){				//sleep(1);  //decomentar para forcar pai a ser mais lento que filho.
		
		for (i = 0; i< 30; i++){
			while( shared_data->nRead + SAFETY  < shared_data->nWrite) {/*WAITING*/;} // Espera ativa enquanto
			// o numero de leituras muito inferior as escritas. Previne que escrevamos por cima do buffer circular.
			
			valor = i * 10;			//Valor a escrever 
			posicao = i % 10;			//posicao do vector onde escrever. Escreve no resto de i a dividir por 10 , ou seja 0-9, ciclico.
			
			shared_data->buffer[posicao] = valor;	//escrever o valor na memoria partilhada 

			//Ler o numero de escritas e incrementar em 1.
			localNWrite = shared_data->nWrite;	
			localNWrite++;				
			shared_data->nWrite = localNWrite ;

			//Debug para confirmar o que pai escreveu.
			printf("PAI  : Write %i na posicao %i . Total escritas: %i.\n", valor, posicao, localNWrite);
		}		


	}
	
	//FILHO
	if(pid == 0){

		for (i = 0; i< 30; i++){
			while( shared_data->nWrite <= shared_data->nRead) {/*WAITING*/;} // Espera ativa enquanto
			// o numero de escritas for inferiror ao numero de leituras. Previne que leiamos algo nao escrito.
			
			posicao = i % 10;			//posicao do vector onde ler.

			//Ler o valor da memoria partilhada 
			valor = shared_data->buffer[posicao];

			//Incrementar o numero de leituras na memoria partilhada em 1.
			localNRead = shared_data->nRead;
			localNRead++;
			shared_data->nRead = localNRead ;

			//Output do filho do que escreveu.
			printf("Filho: Read  %i na posicao %i . Total leituras: %i.\n", valor, posicao, localNRead);
		}	

		exit(0);
	}
	
	wait(NULL);	//FILHO JA MORREU, APENAS CODIGO PAI 

	
	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Desmapea
	if(error == -1){perror("MUNMAP RETURNED -1");}
	
	error = close(fd);						// Fecha o descritor
	if(error == -1){perror("CLOSE RETURNED -1");}
	
	error = shm_unlink("/shmtest");					// Fecha o ficheiro , senão nao cria na proxima corrida.
	if(error == -1) {perror("UNLINK RETURNED -1\n");}
	
	printf("PAI: terminado.\n");

	return 0;
}


