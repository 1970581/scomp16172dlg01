#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

#include <time.h>  		// usada pelo random.

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#define STR_SIZE 50
#define NR_DISC 10
#define READ_BLOCKED 0
#define READ_OK 1

//Declaracao do funcao random.
int random_number(int max, int seed_multiplier);

//Extrotura delineada pelo exercicio.
struct aluno{
	int numero;
	char nome[STR_SIZE];
	int disciplinas[NR_DISC];
};

// Extrotura da memoria partilhada com uma extrotura aluno e um semaforo.
typedef struct {
	
	int semaforo;		//Usado para permitir acesso de leitura ou nao.
	struct aluno sAluno;	// Dados do aluno 
		
} shared_data_type;



int main(int argc, char *argv[]){ 
	int fd;			//Descritor da shared memory 
	int error;		//Diagnostico dos processos de partilha de memoria
	int data_size = sizeof(shared_data_type);	//Tamanho da estrutura definida

	shared_data_type *shared_data;				//Apontador de uma estrutura de memoria partilhada.
	int i = 0;
	
	
	//PARTILHA DE MEMORIA - START
	fd = shm_open("/shmtest", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);	// Abrir/criar ficheiro de memoria partilhada
	if(fd == -1){perror("SHM OPEN RETURNED -1 \nTYPE:\n> rm /dev/dhm/shmtest\n ");}
	
	error = ftruncate(fd, data_size);			// Indica o tamanho da zona de memoria					
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	
	//CODIGO
	
	shared_data->semaforo = READ_BLOCKED ; // Garantir que o semaforo está em posisao que bloqueia leitura.	

	//Criar processo filho 
	size_t pid = fork();
	if(pid < 0) {perror("Erro a fazer fork.\n"); exit(1);}
	else{printf("%s[%i] em concorrencia.\n", (pid > 0) ? "pai" : "filho" , getpid() );}

	//PAI
	if(pid > 0){				//sleep(1);  //decomentar para forcar pai a ser mais lento que filho.
		while(shared_data->semaforo !=READ_BLOCKED );  //Desnecessario visto garantirmos no inicio. MAS placeholder para estensao de codigo.
		
		//Preencher aluno na memoria partilhada 
		shared_data->sAluno.numero = 1;
		strcpy(shared_data->sAluno.nome , "nome aluno");
		for(i = 0; i < NR_DISC ; i++){				//Notas do aluno 
			int nota_random = random_number( 20 , i+1);
			shared_data->sAluno.disciplinas[i] = nota_random;
			printf("PAI: Escrevi nota[%i] %i \n", i , nota_random);  //Info de debug.
		}
		shared_data->semaforo = READ_OK ;	//Colocar o semaforo na posicao para o filho ler.
	}
	
	//FILHO
	if(pid == 0){
		while(shared_data->semaforo != READ_OK ) {/*WAIT*/;} //Espera activa , ate o pai dar ordem de poder ler.

		//Ler a informacao do aluno da memorea partilhada e imprimir.
		printf("Filho: Aluno nº: %i\n", shared_data->sAluno.numero );
		printf("Filho: Nome: %s\n", shared_data->sAluno.nome); 
		int max = 0;
		int min = 21;
		int avg = 0;
		for(i = 0; i <10; i++){  //Estatisticas 
			if(max < shared_data->sAluno.disciplinas[i]) max = shared_data->sAluno.disciplinas[i];
			if(min > shared_data->sAluno.disciplinas[i]) min = shared_data->sAluno.disciplinas[i];
			avg += shared_data->sAluno.disciplinas[i];

		}
		float media = ((double) avg) / 10.0;		

		printf("Filho: Min: = %i Max = %i Media = %2.2f\n", min, max, media);

		exit(0);
	}
	
	wait(NULL);	//FILHO JA MORREU, APENAS CODIGO PAI 

	
	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Desmapea
	if(error == -1){perror("MUNMAP RETURNED -1");}
	
	error = close(fd);						// Fecha o descritor
	if(error == -1){perror("CLOSE RETURNED -1");}
	
	error = shm_unlink("/shmtest");					// Fecha o ficheiro , senão nao cria na proxima corrida.
	if(error == -1) {perror("UNLINK RETURNED -1\n");}
	
	printf("PAI: terminado.\n");

	return 0;
}


/*
*	FUNCAO DE RANDOM
*/
int random_number(int max, int seed_multiplier){
	int valor_saida = -1;
	int max_number = max;
	//int minimum_number = 1;
	if(seed_multiplier == 0) seed_multiplier = 1;
	
	// Vamos alterar a seed to rand() com recurso a uma funcao que devolve o tempo actual.
	// Isto previne rand() de dar sempre o mesmo valor de saida.
	srand(time(NULL)*seed_multiplier);
	
	//Chamamos o rand() para obter um numero random de 0 a RAND_MAX 
	int numero_random = rand();
	
	// Dividimos o valor obtido pelo RAND_MAX para obtermos um float de 0 até 1.
	// Como em Java.
	float aux = (float) numero_random / RAND_MAX;

	// Multiplicamos por maior valor, mais 1 para obter numeros de 1 a 5.
	valor_saida = aux * (max_number) +1 ;


	//Devolvemos o valor.
	return(valor_saida);
}

