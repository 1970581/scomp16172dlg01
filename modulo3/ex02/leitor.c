#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

typedef struct {
	int numero[10];
		
} shared_data_type;



//LEITOR
int main(int argc, char *argv[]){ 
	int fd; 		//Descritor
	int error; 		//Diagnostico dos processos de partilha de memoria
	int data_size = sizeof(shared_data_type);		//Tamanho da estrutura definida
	shared_data_type *shared_data;				//Apontador de uma estrutura
	
	
	//PARTILHA DE MEMORIA - START
	fd = shm_open("/shmtest", O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);	// Abrir/criar ficheiro de memoria partilhada
	if(fd == -1){perror("SHM OPEN RETURNED -1");}
	
	error = ftruncate(fd, data_size);				// Indica o tamanho da zona de memoria
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	
	shared_data_type dados;

	int soma = 0;
	int i = 0;	

	printf("Leitor: ");
	for(i= 0 ; i<10; i++) {
		dados.numero[i] = shared_data->numero[i];
		printf("%i ", dados.numero[i]);	
		soma += dados.numero[i];	
	}
	printf("\n");

	double media = (double) soma;
	media /= 10;
	printf("Leitor: Media = %3.2f\n", media);

	
	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Nao mapea mais
	if(error == -1){perror("MUNMAP RETURNED -1");}
	
	error = close(fd);									// Fecha o descritor
	if(error == -1){perror("CLOSE RETURNED -1");}
	
	error = shm_unlink("/shmtest");						// Fechar ficheiro de memoria partilhada
	if(error == -1){perror("UNLINK RETURNED -1");}
	
	return 0;
}
