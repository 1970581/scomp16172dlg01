#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

#include <time.h>  		// usada pelo random.

//Bibliotecas de memoria partilhada
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>



int random_number(int max, int seed_multiplier){
	int valor_saida = -1;
	int max_number = max;
	//int minimum_number = 1;
	if(seed_multiplier == 0) seed_multiplier = 1;
	
	// Vamos alterar a seed to rand() com recurso a uma funcao que devolve o tempo actual.
	// Isto previne rand() de dar sempre o mesmo valor de saida.
	srand(time(NULL)*seed_multiplier);
	
	//Chamamos o rand() para obter um numero random de 0 a RAND_MAX 
	int numero_random = rand();
	
	// Dividimos o valor obtido pelo RAND_MAX para obtermos um float de 0 até 1.
	// Como em Java.
	float aux = (float) numero_random / RAND_MAX;

	// Multiplicamos por maior valor, mais 1 para obter numeros de 1 a 5.
	valor_saida = aux * (max_number) +1 ;


	//Devolvemos o valor.
	return(valor_saida);
}







typedef struct {
	int numero[10];
		
} shared_data_type;


//WRITER
int main(int argc, char *argv[]){ 
	int fd;			//Descritor da shared memory 
	int error;		//Diagnostico dos processos de partilha de memoria
	int data_size = sizeof(shared_data_type);	//Tamanho da estrutura definida
	shared_data_type *shared_data;				//Apontador de uma estrutura
	int i = 0;
	int valor = 0;
	
	
	//PARTILHA DE MEMORIA - START
	fd = shm_open("/shmtest", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);	// Abrir/criar ficheiro de memoria partilhada
	if(fd == -1){perror("SHM OPEN RETURNED -1");}
	
	error = ftruncate(fd, data_size);			// Indica o tamanho da zona de memoria					
	if(error == -1){perror("FTRUNCATE RETURNED -1");}
	
	// Mapea
	shared_data = (shared_data_type*) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	
	//CODIGO
	
	printf("Escrit: ");
	
	//Preencher o shared structure com 10 inteiriros random de 1 a 20.
	for(i = 0; i <10; i++){
		valor = random_number(20, i +1 );
		shared_data->numero[i] = valor;
		printf("%i ", valor);
	}
	printf("\n");

	
	//PARTILHA DE MEMORIA - FINISH
	error = munmap(shared_data, data_size);				// Desmapea
	if(error == -1){perror("MUNMAP RETURNED -1");}
	
	error = close(fd);						// Fecha o descritor
	if(error == -1){perror("CLOSE RETURNED -1");}
	
	
	return 0;
}
