#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

int main (){
int i, estado , erro , fd [2];
pid_t pid;
char mens [100] , ler [100];
int ppp = 0;

pipe(fd);
dup2(fd[0] ,0);
dup2(fd[1] ,1);
close(fd [0]);
close(fd [1]);

for(i=1;i<5;i++){
 pipe(fd);
 pid = fork ();
	if(pid == 0) ppp++;
 if(pid > 0)
 erro = dup2(fd[1], 1);
 else
 erro = dup2(fd[0], 0);

 close(fd [0]);
 close(fd [1]);

 if(pid)
 break;
}

sprintf(mens ,"Este e’ o processo %d com a identificacao %d e o seu pai e’ %d\n", i, (int)getpid(), (int)getppid());	
 


write(1, mens , strlen(mens) + 1);
 read(0, ler , strlen(mens ));
 wait (& estado );
 fprintf(stderr , "Processo (F%i) actual = %d, dados =%s", ppp, (int)getpid(), ler);

  exit (0);
}
