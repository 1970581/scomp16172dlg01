#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>


struct mensagem {
	char texto[80];
	int numero;
}	;
	

int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	int fd[2][2];  // [0] leitura [1] escrita 
	int i;
	int error;

	//Ciclo de pipes
	for(i = 0; i < 2; i++){	
		error = pipe(fd[i]);	
		if(error){ 
			perror("Erro a criar pipe");
		}
	}

	//Ciclo de filhos
	for(i = 0; i < 3; i++){
		pid = fork();
		if(pid < 0) perror("Erro a fazer fork");

		//Codigo do filho
		if(pid == 0) {
			
			if(i == 0){	
				error = close(fd[i][0]);  // fechar o de leitura do Pipe 
				if(error) perror("Erro a fechar o pipe\n");
			
				error = close(fd[1][0]);  // fechar o de leitura do Pipe 
				if(error) perror("Erro a fechar o pipe\n");
				
				error = close(fd[1][1]);  // fechar o de escrita do Pipe 
				if(error) perror("Erro a fechar o pipe\n");
			
				//VAMOS REDIRECIONAR O STANDARD OUTPUT PARA ESCREVER DADOS NO PIPE 0.		
				dup2(fd[i][1], 1);
				error = close(fd[i][1]);
				if(error) perror("Erro a fechar o pipe\n");	
				execlp("ls" , "ls", "-la", (char*) NULL);	
				printf("FILHO: SE PASSOU AQUI execlp deu ERRO");			
			}		
			
			if(i == 1){
				error = close(fd[i][0]);  // fechar o de leitura do Pipe 
				if(error) perror("Erro a fechar o pipe\n");
				
				error = close(fd[0][1]);  // fechar o de escrita do Pipe 
				if(error) perror("Erro a fechar o pipe\n");
			
				//VAMOS REDIRECIONAR O STANDARD INPUT PARA LER DADOS DO PIPE 0.		
				dup2(fd[0][0], 0);
				error = close(fd[0][0]);
				if(error) perror("Erro a fechar o pipe\n");	
				
				//VAMOS REDIRECIONAR O STANDARD OUTPUT PARA ESCREVER DADOS NO PIPE 1.	
				dup2(fd[i][1], 1);
				error = close(fd[i][1]);
				if(error) perror("Erro a fechar o pipe\n");	
							
				execlp("sort" , "sort", (char*) NULL);	
				printf("FILHO: SE PASSOU AQUI execlp deu ERRO");
			}
			
			if(i == 2){
				error = close(fd[0][0]);  // fechar o de leitura do Pipe 
				if(error) perror("Erro a fechar o pipe\n");
				
				error = close(fd[0][1]);  // fechar o de escrita do Pipe 
				if(error) perror("Erro a fechar o pipe\n");
				
				error = close(fd[1][1]);  // fechar o de escrita do Pipe 
				if(error) perror("Erro a fechar o pipe\n");
				
				//VAMOS REDIRECIONAR O STANDARD INPUT PARA LER DADOS DO PIPE 1.		
				dup2(fd[1][0], 0);
				error = close(fd[1][0]);
				if(error) perror("Erro a fechar o pipe\n");	
				
				execlp("wc" , "wc", "-l",(char*) NULL);	
				printf("FILHO: SE PASSOU AQUI execlp deu ERRO");
			}
			
			exit(1);
		}
	}

	
		

	// Codigo do Pai, sempre
	printf("Pai. Iniciou.\n");
	
	
	for(i = 0; i < 2; i++){
		error = close(fd[i][0]); // fechar pipe leitura 
		if(error) perror("erro ao fechar pipe");
		
		error = close(fd[i][1]); // fechar pipe escrita  
		if(error) perror("erro ao fechar pipe");
	}
	
	
	for(i = 0; i < 3; i++){
		wait(&status);
	}
		
	printf("Pai. Terminou normalmente.\n");
	return 0;
}
