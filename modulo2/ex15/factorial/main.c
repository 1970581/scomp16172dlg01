#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


struct mensagem {
	char nome[80];	// Nome do produto 
	int preco;	//Preco
	int codigo;	//Codigo de barras do produto 
	int filho;	//Filho que pediu.
}	;
	



int calfactorial(int valor){
	int i = 0;
	int resultado = 1;

	if (valor == 0) return 0;
	if (valor < 1) return -1;

	for (i = 1; i <= valor; i++){
		resultado *= i;
	}
	return resultado;
}

	
int main(int argc, char *argv[]){


	char numeroTexto[256];
	int factorial = -1;
	int numero = -1;
	
	scanf("%s" , numeroTexto); //Ler numero string 

	numero = atoi(numeroTexto);//Converter para int 

	factorial = calfactorial(numero); //Calcular o factorial 
	
	int size = 11;
	int i = 0;
	char buffer[size];
	for(i = 0; i < size; i++) buffer[i] = '\0';
	snprintf ( buffer, size , "%i\0", factorial); // Imprime para o string buffer.
	printf("%s", buffer);	
	
	exit(0);
	return factorial; //Sem uso.


}
