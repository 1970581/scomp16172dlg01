#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>


struct mensagem {
	char texto[80];
	int numero;
}	;
	




int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	int fdPaiFilho[2]; 
	int fdFilhoPai[2];
	 
	size_t size;


	//Criar pipe
	int error = pipe(fdPaiFilho);
	if(error) perror("Erro a criar pipe");
	error = pipe(fdFilhoPai );
	if(error) perror("Erro a criar pipe");


	pid = fork();
	if(pid < 0) perror("Erro a fazer fork");

	//Codigo do filho
	if(pid == 0) {
		error = close(fdPaiFilho[1]);  // fechar o de escrita do Pipe 
		if(error) perror("Erro a fechar o pipe\n");
		
		error = close(fdFilhoPai[0]);  // fechar o de read do Pipe 
		if(error) perror("Erro a fechar o pipe\n");
	
		//VAMOS REDIRECIONAR O STANDARD IMPUT E OUTPUT PARA O PIPE.
		// 0 - STDIN 
		// 1 - STDOUT
		// 2 - STRERR 
		dup2(fdPaiFilho[0] , 0);  // Injectar o pipe_READ no STDIN 
		close(fdPaiFilho[0]);
		
		dup2(fdFilhoPai[1] , 1); // Injectar o pipe_WRITE no STDOUT 
		close(fdFilhoPai[1]);

		//Correr o programa de factorial 
		execlp("./factorial/factorial.exe" , "factorial.exe", (char*) NULL);		
		
	
		printf("FILHO: SE PASSOU AQUI execlp deu ERRO\n");//Atencao o printF esta para o pipe.
		printf("Filho: exit.\n"); //Atencao o printF esta para o pipe.
		exit(1);
	}
		

// Codigo do Pai, sempre
	error = close(fdPaiFilho[0]);  // fechar o de read do Pipe 
	if(error) perror("Erro a fechar o pipe\n");
		
	error = close(fdFilhoPai[1]);  // fechar o de write do Pipe 
	if(error) perror("Erro a fechar o pipe\n");

	


	//Ler numero 
	char numeroTexto[255];
	printf("Factorial de:");
	scanf("%s" , numeroTexto);
	int valor = 0;
	valor = atoi(numeroTexto);
	printf("Escreveu: %i\n", valor);
	
	//Escrever no PIPE 
	
	size = strlen(numeroTexto);
	error = write(fdPaiFilho[1] , &numeroTexto , size  );	//mandamos o numero para o pipe.
	if(error == -1) perror("Erro a escrever pelo pipe do pai");	
	error = close(fdPaiFilho[1]);  // fechar o de write do Pipe 
	if(error) perror("Erro a fechar o pipe\n");

	//Ler resposta do pipe
	int sizeb = 11;
	char buffer[sizeb]; 
	int i;
	for (i = 0; i< sizeb; i++) buffer[i] = '\0'; //IMPORTANTE PARA NAO VIR LIXO AGARADO NA STRING. "ZERAMOS" a string toda.

	error = read(fdFilhoPai[0] , &buffer, sizeb );
	if(error == -1) perror("Erro a ler pelo pipe do pai");	
	error = close(fdFilhoPai[0]);  
	if(error) perror("Erro a fechar o pipe\n");

	printf("O factorial de %s =\n", numeroTexto);
	printf(" %s\n", buffer);
	
	
	//Esperar pelo filho. 
	waitpid(pid, &status,0);

	printf("Pai. Terminou normalmente.\n");
	return 0;
}
