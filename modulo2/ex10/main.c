#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

int random_number(int i){
	int valor_saida = -1;
	unsigned int max_number = 5000;
	//unsigned int minimum_number = 1;
	
	// Vamos alterar a seed to rand() com recurso a uma funcao que devolve o tempo actual.
	// Isto previne rand() de dar sempre o mesmo valor de saida.
	srand(time(NULL)*i);
	
	//Chamamos o rand() para obter um numero random de 0 a RAND_MAX 
	int numero_random = rand();
	
	// Dividimos o valor obtido pelo RAND_MAX para obtermos um float de 0 até 1.
	// Como em Java.
	float aux = (float) numero_random / RAND_MAX;

	// Multiplicamos por maior valor, mais 1 para obter numeros de 1 a 5.
	valor_saida = aux * (max_number) +1 ;


	//Devolvemos o valor.
	return(valor_saida);
}
	

int main(int argc, char *argv[]){
	pid_t pid;
	int fd[6][2];  // [0] leitura [1] escrita 
	int i = 0;
	int error;
	int valor_gerado = 0;					//Variavel para o valor gerado do metodo random number
	int * valor_gerado_ptr = &valor_gerado;
	
	
	//Ciclo de pipes
	for(i = 0; i < 6; i++){	//IMPORTANTE 6 E NAO 5
		error = pipe(fd[i]);
		
		if(error){ 
			perror("Erro a criar pipe");
		}
	}

	

	//Ciclo de filhos
	for (i = 1; i < 6; i++) {		//Do 1 ao 5 sao os filhos, o 0 e o pai
		pid = fork();
		if(pid < 0) perror("Erro a fazer fork");

		//Codigo do filho
		if(pid == 0) {
			error = read(fd[i][0], valor_gerado_ptr, sizeof(int));
			if(error == -1) perror("Erro a ler do pipe");
			close(fd[i][0]);
			
			int numero_recebido = valor_gerado;
			int numero_filho_gerado = random_number(i);
			
			printf("Filho: %d, Pid do filho: %d\n", i, getpid());
			printf("Numero recebido: %d, Numero do filho gerado: %d\n", numero_recebido, numero_filho_gerado);
			
			if(i == 5) {
				
				if(numero_recebido > numero_filho_gerado){
					error = write(fd[0][1] , &numero_recebido, sizeof(int));
					if(error == -1) perror("Erro a escrever no pipe");
					close(fd[0][1]);	
				} else {
					error = write(fd[0][1] , &numero_filho_gerado, sizeof(int));
					if(error == -1) perror("Erro a escrever no pipe");
					close(fd[0][1]);	
				}
				
				
			} else {
				
				if(numero_recebido > numero_filho_gerado){
					error = write(fd[i+1][1] , &numero_recebido, sizeof(int));
					if(error == -1) perror("Erro a ler do pipe filho");
					close(fd[i+1][1]);	
				} else {
					error = write(fd[i+1][1] , &numero_filho_gerado, sizeof(int));
					if(error == -1) perror("Erro a ler do pipe filho");
					close(fd[i+1][1]);	
				}
				
				
			}
			
			
			exit(0);

		} //fim do filho

	}
	


	// Codigo do Pai, sempre
	valor_gerado = random_number(10);
	
	error = write(fd[1][1], &valor_gerado, sizeof(int));
	if(error == -1) perror("Erro a escrever no pipe");
	close(fd[1][1]);
	
	int numero_final = 0;
	int * numero_final_ptr = &numero_final;
	
	error = read(fd[0][0], numero_final_ptr, sizeof(int));
	if(error == -1) perror("Erro a ler do pipe");
	close(fd[0][0]);

	printf("Maior numero aleatorio gerado: %i\n", numero_final);	

	
	return 0;
}
