#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


struct tomate {
	int num;
	char nome[80];
}	;
	

int main(int argc, char *argv[]){
	int status;
	pid_t pid;
//	pid_t pidPai;
	int fd[2];
	char pipeBufferW[100];
//	int pipeBufferR[100];
	int num2;
	char  nome2[80];
	int * num2_ptr = &num2;
	struct tomate rolha;



	//Criar pipe
	int error = pipe(fd);
	//printf("error = %i\n",error);
	if(error) perror("Erro a criar pipe");

	pid = fork();
	if(pid < 0) perror("Erro a fazer fork");

	//Codigo do filho
	if(pid == 0) {
		error = close(fd[1]);  // fechar o de escrita
		if(error) perror("Erro a fechar o pipe filho escrita");
		
		//Ler inteiro do pipe.
		error = read(fd[0] , num2_ptr, sizeof(int) );
		if(error == -1) perror("Erro a ler do pipe filho");
		printf("FILHO: inteiro = %i\n", num2);
		
		//LEr nome.
		error = read(fd[0] , nome2, sizeof(char) *80 );
		if(error == -1) perror("Erro a ler do pipe filho");
		printf("FILHO: nome = %s\n", nome2);

		//ALINEA B
		error = read(fd[0] , &rolha, sizeof(struct tomate) );
		if(error == -1) perror("Erro a ler do pipe filho");
		printf("FILHO (b): inteiro = %i nome: %s\n", rolha.num , rolha.nome);




		//Fechar pipe e terminamos processo
		close(fd[0]);
		exit(0);
	}
		
	// Codigo do Pai, sempre
	error = close(fd[0]); // fechar pipe leitura pai 
	if(error) perror("erro ao fechar pipe leitura pai");
	
	// Obter o dados 
	printf("Numero:");
	scanf("%s" , pipeBufferW);
	num2 = atoi(pipeBufferW);
	printf("PAI: numero lido %i\n", num2);
	printf("Nome:");
	scanf("%s" , nome2);
	printf("PAI: nome lido %s\n", nome2);
	
	//Escrever no pipe 
	error = write(fd[1] , num2_ptr , sizeof(int) );
	if(error == -1) perror("Erro a escrever pelo pipe do pai");
	//printf("PAI numero escrito: %i  e escrevemos %i bytes no pipe, \n ", pidPai, error);
	error = write(fd[1] , nome2 , sizeof(char) *80);
	if(error == -1) perror("Erro a escrever pelo pipe do pai");

	
	//ALINEA B
	rolha.num = num2;
	strcpy(rolha.nome , nome2) ;

	error = write(fd[1] , &rolha , sizeof(struct tomate) );
	if(error == -1) perror("Erro a escrever pelo pipe do pai");
	


	error = close(fd[1]);	
	
	waitpid(pid, &status, 0);
	



	return 0;
}
