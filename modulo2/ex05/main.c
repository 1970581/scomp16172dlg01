#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


struct mensagem {
	char texto[80];
	int numero;
}	;
	

int main(int argc, char *argv[]){
//	int status;
	pid_t pid;
	int fd[2];
	struct mensagem m1;
	int vec1[1000];
	int vec2[1000];
	int i = 0;
	for 	(i = 0; i < 1000; i++){
		vec1[i] = i; //preencher o vetor
		vec2[i] = i * -1;
	}
	vec1[0] = 100;
	int resultado_final = 0;




	//Criar pipe
	int error = pipe(fd);
	//printf("error = %i\n",error);
	if(error) perror("Erro a criar pipe");


	//Ciclo de filhos
	for (i = 0; i<5; i++) {
		pid = fork();
		if(pid < 0) perror("Erro a fazer fork");

		//Codigo do filho
		if(pid == 0) {
			error = close(fd[0]);  // fechar o de leitura
			if(error) perror("Erro a fechar o pipe filho leitura\n");
			


			int resultado_filho = 0, j = 0;
			for (j = i * 200 ; j< i * 200 +200; j++){
				resultado_filho  += vec1[j] + vec2[j];
			}
		
			m1.numero = resultado_filho ;	// A escrever o numero na mensagem 
			error = write(fd[1] , &m1 , sizeof(struct mensagem) );
			if(error == -1) perror("Erro a escrever pelo pipe do pai");
			
			error = close(fd[1]);  // fechar o de escrita
			if(error) perror("Erro a fechar o pipe filho escrita");
			
			exit(0);

		} //fim do filho

	}
	


	// Codigo do Pai, sempre
	error = close(fd[1]); // fechar pipe escrita pai 
	if(error) perror("erro ao fechar pipe escrita pai");
	
	//Escrever mensagens.
	
	error = 1;	
	while(error > 0 ){		// Ciclo de leitura do ficheiro, que so termina quando o ficheiro terminar.
		error = read(fd[0] , &m1 , sizeof(struct mensagem) );
		if(error == -1) perror("Erro a ler pelo pipe do pai");
		if(error > 0) resultado_final += m1.numero ;

	}
	
	error = close(fd[0]);	
	
	printf("Resultado final %i . Esperado %i\n", resultado_final , vec1[0] );



	return 0;
}
