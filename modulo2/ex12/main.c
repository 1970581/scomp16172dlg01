#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>


struct mensagem {
	char texto[80];
	int numero;
}	;
	




int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	int fd[2];  // [0] leitura [1] escrita 
//	struct mensagem m1;
	size_t size;
	FILE * ficheiro;
	char texto[255];


	//Criar pipe
	int error = pipe(fd);
	if(error) perror("Erro a criar pipe");

	pid = fork();
	if(pid < 0) perror("Erro a fazer fork");

	//Codigo do filho
	if(pid == 0) {
		error = close(fd[1]);  // fechar o de escrita do Pipe 
		if(error) perror("Erro a fechar o pipe\n");
	
		//Inicio do Loop do filho 

		size = 1;
		
		//VAMOS REDIRECIONAR O STANDARD IMPUT PARA PASSAR A RECEBER DO PIPE.

		close(0);        //closing stdin
		dup(fd[0]);   //replacing stdin with pipe read
		close(fd[1]); //closing pipe write
		close(fd[0]); //closing pipe read
		execlp("od" , "od", (char*) NULL);		
//		system("od");				
			
		printf("FILHO: SE PASSOU AQUI execlp deu ERRO");
		
		close(fd[0]); // fechar o de leitura do Pipe 
		printf("Filho: terminou normalmente.\n");
		exit(1);
	}
		

	// Codigo do Pai, sempre
	error = close(fd[0]); // fechar pipe leitura  
	if(error) perror("erro ao fechar pipe");


	// ENVIAR O CONTEUDO DO FICHEIRO PELO PIPE 
	ficheiro = fopen( "test.txt" , "r");	//abrir ficheiro 

	size_t buf_size = sizeof(texto);			// tamanho do buffer 
	buf_size = sizeof(char); //REDUCAO DO BUFFER PARA 1 char de forma a evitar erros. Vai para o pipe char a char.
	//Corrige tudo, excepto o ultimo octal nao bate certo. Deve ser o exit code do comando "od" (octal dump) que e diferente.
	
	size_t buf_read = buf_size;		// numero de elementos lidos
	
	while(buf_read == buf_size ){		// Ciclo de leitura do ficheiro, que so termina quando o ficheiro terminar.
		buf_read = fread(texto , sizeof(char), buf_size , ficheiro);
		error = write(fd[1] , &texto , buf_size );
		if(error == -1) perror("Erro a escrever pelo pipe do pai");	
	}


	close(fd[1]); // fechar o de escrita
	
	//Esperar pelo filho. 
	waitpid(pid, &status,0);
	
	//Teste para confirmar output.	
	printf("#####################\n# BASH: od test.txt #\n#####################\n");

	system("od test.txt");

	printf("Pai. Terminou normalmente.\n");
	return 0;
}
