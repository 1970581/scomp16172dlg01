#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#define DATA_SIZE 50000
#define FILHO_SIZE 5000
#define Q_MAX 20

struct mensagem {
	char texto[80];
	int numero;
}	;

typedef struct {
	int codigo_cliente;
	int codigo_produto;
	int quantidade;
} registo;
	

int main(int argc, char *argv[]){
//	int status;
	pid_t pid;
	int fd[2];  // [0] leitura [1] escrita 
//	struct mensagem m1;
	int i = 0;
	int j = 0;
	
	registo data[DATA_SIZE];
	registo maiores[DATA_SIZE];
	int n_maiores = 0;
	
for(i = 0; i < DATA_SIZE ; i++){
	data[i].codigo_cliente = i;
	data[i].codigo_produto = i;
//	data[i].quantidade = i % 22; // resto da divisao por 21 , 1%22 = 1 , 21%22 = 21
	data[i].quantidade = 31;
}

	j = 0;
	for(i = 0; i < DATA_SIZE ; i++){
		if(data[i].quantidade > Q_MAX) j++;
	}
	printf("Existem %i no vector data[DATA_SIZE] maiores que Q_MAX\n",j);



	//Criar pipe
	int error = pipe(fd);
	//printf("error = %i\n",error);
	if(error) perror("Erro a criar pipe");


	//Ciclo de filhos
	for (i = 0; i<10; i++) {
		pid = fork();
		if(pid < 0) perror("Erro a fazer fork");

		//Codigo do filho
		if(pid == 0) {
			error = close(fd[0]);  
			if(error) perror("Erro a fechar o pipe filho read\n");

			//ESCREVER (LE O VALOR DA ESTROTURA E SE MAIOR QUE VINTE, MANDA PARA O PIPE)
			for (j = i * FILHO_SIZE ; j < i * FILHO_SIZE + FILHO_SIZE ; j++){
				if(data[j].quantidade > Q_MAX){
					error = write(fd[1] , &data[j], sizeof(registo));
					if(error == -1) perror("Filho write returned -1");
					n_maiores++;
				}
			} 		
			error = close(fd[1]);  // fechar escrita
			if(error) perror("FILHO Erro a fechar o pipe write");
			
			printf("FILHO%i encontrou: %i\n",i, n_maiores );
			exit(0);

		} //fim do filho

	}
	


	// Codigo do Pai, sempre
	error = close(fd[1]);  
	if(error) perror("erro ao fechar pipe write pai");
	
	//Escrever vector.
	size_t bytes_lidos = 1;
	while(bytes_lidos > 0){
		bytes_lidos = read(fd[0], &maiores[n_maiores], sizeof(registo));
		if(bytes_lidos > 0 ) n_maiores++;
		if(bytes_lidos == -1) perror("PAI read returned -1");
	}
	

	
	error = close(fd[0]); if(error) perror("erro ao fechar pipe read pai");


	printf("numero de dados maiores que foram guardados: %i\n",n_maiores);	

	j = 0;
//	int cl = 0;
//	int pro = 0;
//	int qt = 0;
	for(i = 0; i < DATA_SIZE ; i++){
		if(data[i].quantidade > Q_MAX) {
			j++;
		}
//		cl = data[i].codigo_cliente;
//		pro = data[i].codigo_produto;
//		qt = data[i].quantidade;
	}
	printf("Existem %i no vector data",j);
	j = DATA_SIZE;
	printf("[%i]\n",j);
	
	return 0;
}
