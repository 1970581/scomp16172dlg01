#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>


struct mensagem {
	char texto[80];
	int numero;
	int saldo;
	int instrucao;
}	;
	

int lerNumero(){	
	int min = -1;
	int max = 5;
//	int numero = 0;
	printf("Escreva um numero de %i a %i (-1 to exit)\n?", min+2, max);
	char numeroTexto[256];
	
	scanf("%s" , numeroTexto);
	int valor = atoi(numeroTexto);
	if (valor < min || valor > max || valor == 0){
		printf("Valor %i fora de limites, a sair.\n", valor);
		valor = -1; //exit(2);
	}
	return valor;
}

int numeroAleatorio(){
	int valor_saida = -1;

	unsigned int max_number = 5;
	//unsigned int minimum_number = 1;
	
	// Vamos alterar a seed to rand() com recurso a uma funcao que devolve o tempo actual.
	// Isto previne rand() de dar sempre o mesmo valor de saida.
	srand(time(NULL));
	
	//Chamamos o rand() para obter um numero random de 0 a RAND_MAX
	int numero_random = rand(); 
//	printf("DEBUG: rand() = %i\n", numero_random);
	
	// Dividimos o valor obtido pelo RAND_MAX para obtermos um float de 0 até 1.
	// Como em Java.
	float aux = (float) numero_random / RAND_MAX;

	// Multiplicamos por maior valor, mais 1 para obter numeros de 1 a 5.
	valor_saida = aux * (max_number) +1 ;

//	printf("Saiu o valor: %i\n", valor_saida);

	//Devolvemos o valor.
	return(valor_saida);	
}



int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	int fdPaiFilho[2];  // [0] leitura [1] escrita 
	int fdFilhoPai[2];
	struct mensagem m1;
//	int i = 0;
	int saldo = 20;
	size_t size;


	//Criar pipe
	int error = pipe(fdPaiFilho);
	if(error) perror("Erro a criar pipe");

	error = pipe(fdFilhoPai);
	if(error) perror("Erro a criar pipe");


	pid = fork();
	if(pid < 0) perror("Erro a fazer fork");

	//Codigo do filho
	if(pid == 0) {
		error = close(fdPaiFilho[1]);  // fechar o de escrita do Pipe Pai->Filho 
		if(error) perror("Erro a fechar o pipe\n");
	
		error = close(fdFilhoPai[0]);  // fechar o de leitura do Pipe Filho->Pai
		if(error) perror("Erro a fechar o pipe\n");

		//Inicio do Loop do filho 
		startfilho:		

			// Lemos a instrucao 
			size = read(fdPaiFilho[0], &m1, sizeof(struct mensagem));
			if(size <0) perror("Filho read return -1");
			if(size <= 0) exit(1);
		
			if(m1.instrucao == 0) goto endfilho; // Se instrucao zero, terminamos. Senao continua.

			//Nao sendo 0 a instrucao, geramos numero aleatorio e devolvemos ao Pai.
			m1.numero = numeroAleatorio();				
			printf("Filho: rand= %i.\n", m1.numero);
			size = write(fdFilhoPai[1], &m1, sizeof(struct mensagem));
			if(size <= 0) exit(1);

			//Depois lemos o saldo.
			size = read(fdPaiFilho[0], &m1, sizeof(struct mensagem));
			if(size <0) perror("Filho read return -1");
			if(size <= 0) exit(1);
			saldo = m1.saldo;  //Actualiza o saldo.  
		
		// Loop ao ciclo do filho.
		goto startfilho;

		endfilho: //Fechar o processo filho 
		close(fdPaiFilho[0]); // fechar o de leitura do Pipe Pai->Filho 
		close(fdFilhoPai[1]); // fechar o de escrita do Pipe Filho->Pai
		printf("Filho: terminou normalmente.\n");
		exit(1);


	}
		

	// Codigo do Pai, sempre
	error = close(fdPaiFilho[0]); // fechar pipe leitura pai->filho  
	if(error) perror("erro ao fechar pipe");
	error = close(fdFilhoPai[1]); // fechar pipe escrita filho->pai  
	if(error) perror("erro ao fechar pipe");

	saldo = 20;
	int numero_escrito = -1;
	m1.instrucao = 1;

	//Inicio do loop do pai.
	startPai:

		//Confirmar saldo. Se positivo, obter numero a apostar. Se negativo, abortar programa com auto -1.
		printf("Saldo: %i\n", saldo);
		if (saldo >0) numero_escrito = lerNumero();
		else numero_escrito = -1;
	
		//Calcular instrucao para o filho. 0 ABORT  1 CONTINUE
		if(numero_escrito == -1) m1.instrucao = 0; //abortar 
		else m1.instrucao = 1; //continuar 

		// passar a instrucao de ABORT/CONTINUE ao filho 
		size = write(fdPaiFilho[1], &m1, sizeof(struct mensagem));
		if(size <= 0 ) { perror("Pai write returned <= 0");	exit(1);}

		if(numero_escrito == -1) goto endPai; //Temos um abort, vamos para o fim.
	
		//Ler o valor random devolvido pelo filho.
		size = read(fdFilhoPai[0], &m1, sizeof(struct mensagem));
		if(size <0) perror("Pai read return -1");
		if(size <= 0) exit(1);//ERRO 

		//Comparar e actualizar o saldo.
		if(m1.numero == numero_escrito) saldo += 10;	//acertou, +10 euros 
		else saldo += -5;				// falhou -5 euros 

		//enviar saldo ao filho.
		m1.saldo = saldo;
		size = write(fdPaiFilho[1], &m1, sizeof(struct mensagem));
		if(size <= 0) { perror("Pai write returned <= 0");	exit(1);}

		goto startPai; //Loop ao processo pai.
	

	//Fim do processo pai.
	endPai:

	close(fdPaiFilho[1]); // fechar o de escrita do Pipe Pai->Filho 
	close(fdFilhoPai[0]); // fechar o de leitura do Pipe Filho->Pai
	
	waitpid(pid, &status,0);
	printf("Pai. Terminou normalmente.\n");


	



	return 0;
}
