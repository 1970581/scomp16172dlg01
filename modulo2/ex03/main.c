#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


struct mensagem {
	char texto[80];
}	;
	

int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	int fd[2];
	struct mensagem m1;
	struct mensagem m2;



	//Criar pipe
	int error = pipe(fd);
	//printf("error = %i\n",error);
	if(error) perror("Erro a criar pipe");

	pid = fork();
	if(pid < 0) perror("Erro a fazer fork");

	//Codigo do filho
	if(pid == 0) {
		error = close(fd[1]);  // fechar o de escrita
		if(error) perror("Erro a fechar o pipe filho escrita");
		
		//LEr mensagem 1
		error = read(fd[0] , &m1, sizeof(struct mensagem) );
		if(error == -1) perror("Erro a ler do pipe filho");
		printf("FILHO: mensagem %s\n", m1.texto);

		//LEr mensagem 1
		error = read(fd[0] , &m2, sizeof(struct mensagem) );
		if(error == -1) perror("Erro a ler do pipe filho");
		printf("FILHO: mensagem %s\n", m2.texto);

		//Esperar pelo fecho do pipe pelo pai.
		error = read(fd[0] , &m2, sizeof(struct mensagem) );
		//printf("error= %i\n", error);
		if(error != 0) perror("Ainda lemos mais informacaoW");

		//Fechar pipe e terminamos processo
		close(fd[0]);
		exit(0);
	}
		
	// Codigo do Pai, sempre
	error = close(fd[0]); // fechar pipe leitura pai 
	if(error) perror("erro ao fechar pipe leitura pai");
	
	//Escrever mensagens.
	
	strcpy(m1.texto, "Hello world"); 
	strcpy(m2.texto, "Good bye"); 
	
	//Escrever no pipe 
	error = write(fd[1] , &m1 , sizeof(struct mensagem) );
	if(error == -1) perror("Erro a escrever pelo pipe do pai");
	
	error = write(fd[1] , &m2 , sizeof(struct mensagem) );
	if(error == -1) perror("Erro a escrever pelo pipe do pai");

	

	error = close(fd[1]);	
	
	waitpid(pid, &status, 0);
	



	return 0;
}
