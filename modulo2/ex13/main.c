#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>


struct mensagem {
	char texto[80];
	int numero;
}	;
	




int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	int fd[2];  // [0] leitura [1] escrita 
	char texto[255];


	//Criar pipe
	int error = pipe(fd);
	if(error) perror("Erro a criar pipe");

	pid = fork();
	if(pid < 0) perror("Erro a fazer fork");

	//Codigo do filho
	if(pid == 0) {
		error = close(fd[0]);  // fechar o de leitura do Pipe 
		if(error) perror("Erro a fechar o pipe\n");
	
		
		//VAMOS REDIRECIONAR O STANDARD OUTPUT PARA ESCREVER DADOS DO PIPE.
		
		dup2(fd[1], 1);
		error = close(fd[1]);
		if(error) perror("Erro a fechar o pipe\n");
					
		execlp("sort" , "sort", "test.txt", (char*) NULL);	
		printf("FILHO: SE PASSOU AQUI execlp deu ERRO");
			
		printf("Filho: terminou normalmente.\n");
		exit(1);
	}
		

	// Codigo do Pai, sempre
	printf("Pai. Iniciou.\n");
	error = close(fd[1]); // fechar pipe escrita  
	if(error) perror("erro ao fechar pipe");

	size_t buf_size = sizeof(texto);	// tamanho do buffer
	buf_size = sizeof(char); 			//REDUCAO DO BUFFER PARA 1 char de forma a evitar erros. Vai para o pipe char a char.

	size_t buf_read = buf_size;			// numero de elementos lidos

	while(buf_read == buf_size){		// Ciclo de leitura do ficheiro, que so termina quando o ficheiro terminar.
		buf_read = read(fd[0], texto, sizeof(texto));
		if(buf_read == -1) perror("Erro a escrever pelo pipe do pai");	
	}
	
	close(fd[0]); // fechar o de leitura
	
	printf("%s", texto);
	
	//Esperar pelo filho. 
	waitpid(pid, &status,0);
	
	
	printf("Pai. Terminou normalmente.\n");
	return 0;
}
