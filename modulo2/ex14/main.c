#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>


struct mensagem {
	char texto[80];
	int numero;
}	;
	




int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	int fd[2];  // [0] leitura [1] escrita 
//	struct mensagem m1;
	size_t size;


	//Criar pipe
	int error = pipe(fd);
	if(error) perror("Erro a criar pipe");

	pid = fork();
	if(pid < 0) perror("Erro a fazer fork");

	//Codigo do filho
	if(pid == 0) {
		error = close(fd[1]);  // fechar o de escrita do Pipe 
		if(error) perror("Erro a fechar o pipe\n");
	
		//Inicio do Loop do filho 

		//DEBUG DO PIPE (NAO CORRE) 
		size = 0;
//		size = 1; // descomentar para correr debug.
		char linha2[80] ;
		while(size>0) {size = read(fd[0] , &linha2, 80); printf("%s",linha2);}
		//FIM DE DEbug DO PIPE
		
		//VAMOS REDIRECIONAR O STANDARD IMPUT PARA PASSAR A RECEBER DO PIPE.
		// 0 - STDIN 
		// 1 - STDOUT
		// 2 - STRERR 
		dup2(fd[0] , 0);  // Injectar o pipe de leitura fd[0] no descritor do STDIN 0
		close(fd[0]);

		execlp("more" , "more", (char*) NULL);		// correr o pipe.
//		system("more");				
			
		printf("FILHO: SE PASSOU AQUI execlp deu ERRO");
		
		close(fd[0]); // fechar o de leitura do Pipe 
		printf("Filho: terminou normalmente.\n");
		exit(1);
	}
		

	// Codigo do Pai, sempre
	error = close(fd[0]); // fechar pipe leitura  
	if(error) perror("erro ao fechar pipe");



	// ENVIAR O CONTEUDO PELO PIPE 
	
	size = 20;
	char linha[80];
	int i;
	for(i = 1; i < 101; i = i+1){
		sprintf(linha, "linha %i\n", i);  //imprime para o string linha o conteudo de uma linha tipo printf 

		size = strlen(linha);	
		error = write(fd[1] , &linha , size  );	//mandamos a linha para o pipe.
//		printf("%i\n", size);
		if(error == -1) perror("Erro a escrever pelo pipe do pai");	
	}

	close(fd[1]); // fechar o de escrita
	
	//Esperar pelo filho. 
	waitpid(pid, &status,0);

	printf("Pai. Terminou normalmente.\n");
	return 0;
}
