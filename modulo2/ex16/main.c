#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

struct mensagem {
	char texto[80];
	int numero;
}	;
	

int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	int fd[2];  		// [0] leitura [1] escrita
	int fdPaiFilho[2];	// [0] leitura [1] escrita
	int i;

	//Criar pipe
	int error = pipe(fd);
	if(error) perror("Erro a criar pipe");
	
	error = pipe(fdPaiFilho);
	if(error) perror("Erro a criar pipe");
	
	
	for(i = 0; i < 2; i++){
	pid = fork();
	if(pid < 0) perror("Erro a fazer fork");

		//Codigo do filho
		if(pid == 0) {
			if(i == 0){
				error = close(fd[0]); // fechar pipe leitura 
				if(error) perror("erro ao fechar pipe");
				
				//VAMOS REDIRECIONAR O STANDARD OUTPUT PARA PASSAR A ESCREVER DO PIPE.
				// 0 - STDIN 
				// 1 - STDOUT
				// 2 - STRERR 
				dup2(fd[1] , 1);  // Injectar o pipe de escrita fd[1] no descritor do STDOUT 1
				close(fd[1]);	
								
				execlp("./programa1/programa1.exe" , "programa1.exe", (char*) NULL);		// correr o pipe.
				printf("FILHO: SE PASSOU AQUI execlp deu ERRO\n");//Atencao o printF esta para o pipe.	
				exit(1);
			}
			
			if(i == 1){
				error = close(fdPaiFilho[1]);  // fechar o de escrita do Pipe 
				if(error) perror("Erro a fechar o pipe\n");
				
				dup2(fdPaiFilho[0] , 0);  // Injectar o pipe_READ no STDIN 
				close(fdPaiFilho[0]);
								
				execlp("./programa2/programa2.exe" , "programa2.exe", (char*) NULL);		// correr o pipe.
				printf("FILHO: SE PASSOU AQUI execlp deu ERRO\n");//Atencao o printF esta para o pipe.	
				exit(1);
			}
		}	
	}	

	// Codigo do Pai, sempre
	printf("Pai. Iniciou.\n");
	char texto[255];
	error = close(fd[1]); // fechar pipe escrita 
	if(error) perror("erro ao fechar pipe");
	
	error = close(fdPaiFilho[0]);  // fechar o de read do Pipe 
	if(error) perror("Erro a fechar o pipe\n");
	
	
	FILE *f = fopen("fx.txt", "w");
	if (f == NULL){
		printf("Error opening file!\n");
		exit(1);
	}
	
	size_t buf_size = sizeof(texto);	// tamanho do buffer
	buf_size = sizeof(char); 			//REDUCAO DO BUFFER PARA 1 char de forma a evitar erros. Vai para o pipe char a char.

	size_t buf_read = buf_size;			// numero de elementos lidos

	while(buf_read == buf_size){		// Ciclo de leitura do ficheiro, que so termina quando o ficheiro terminar.
		buf_read = read(fd[0], texto, sizeof(texto));
		if(buf_read == -1) perror("Erro a escrever pelo pipe do pai");	
	}	
	
	int size = strlen(texto);
	error = write(fdPaiFilho[1] , texto , size);	//mandamos o resultado para o pipe.
	if(error == -1) perror("Erro a escrever pelo pipe do pai");	
	error = close(fdPaiFilho[1]);  // fechar o de write do Pipe 
	if(error) perror("Erro a fechar o pipe\n");
	
	
	fprintf(f, "%s", texto);
			
	for(i = 0; i < 2; i++){
		wait(&status);
	}
			
	printf("Pai. Terminou normalmente.\n");
	return 0;
}
