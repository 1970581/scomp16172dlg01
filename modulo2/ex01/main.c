#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


	
	

int main(int argc, char *argv[]){
	pid_t pid;
	pid_t pidPai;
	int fd[2];
	int pipeBufferW[10];
	int pipeBufferR[10];

	//Criar pipe
	int error = pipe(fd);
	//printf("error = %i\n",error);
	if(error) perror("Erro a criar pipe");

	pid = fork();
	if(pid < 0) perror("Erro a fazer fork");

	//Codigo do filho
	if(pid == 0) {
		error = close(fd[1]);  // fechar o de escrita
		if(error) perror("Erro a fechar o pipe filho escrita");
		
		//Ler do pipe.
		error = read(fd[0] , pipeBufferR, sizeof(int) );
		if(error == -1) perror("Erro a ler do pipe filho");
		printf("FILHO Lemos %i bytes. Pid do pai: %i de %i \n", error, pipeBufferR[0], getppid());
		
		//Fechar pipe e terminamos processo
		close(fd[0]);
		exit(0);
	}
		
	// Codigo do Pai, sempre
	error = close(fd[0]); // fechar pipe leitura pai 
	if(error) perror("erro ao fechar pipe leitura pai");
	
	// Obter e escrever no buffer o pid do pai 
	pidPai = getpid();
	pipeBufferW[0] = pidPai; 	

	error = write(fd[1] , pipeBufferW , sizeof(int) ); //escrever no pipe o pid do pai
	if(error == -1) perror("Erro a escrever pelo pipe do pai");
	printf("PAI pid: %i  e escrevemos %i bytes no pipe, \n ", pidPai, error);
	error = close(fd[1]);	
	
	wait(NULL);
	



	return 0;
}
