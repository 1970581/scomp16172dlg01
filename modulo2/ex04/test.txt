#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


struct mensagem {
	char texto[80];
}	;
	

int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	int fd[2];
	struct mensagem m1;
	struct mensagem m2;
	FILE *ficheiro;
	




	//Criar pipe
	int error = pipe(fd);
	//printf("error = %i\n",error);
	if(error) perror("Erro a criar pipe");

	pid = fork();
	if(pid < 0) perror("Erro a fazer fork");

	//Codigo do filho
	if(pid == 0) {
		error = close(fd[1]);  // fechar o de escrita
		if(error) perror("Erro a fechar o pipe filho escrita");

		size_t lidos = 80;

		while (lidos > 0) {	//Ciclo de leitura do pipe que so termina quando o pai fechar o descritor de escrita.	
			//LEr pipe 
			lidos = read(fd[0] , &m1, sizeof(struct mensagem) );
			if(lidos == -1) perror("Erro a ler do pipe filho");
			printf("FILHO: mensagem %s\n", m1.texto);
		}

		//Fechar pipe e terminamos processo
		close(fd[0]);
		exit(0);
	}

		
	// Codigo do Pai, sempre
	error = close(fd[0]); // fechar pipe leitura pai 
	if(error) perror("erro ao fechar pipe leitura pai");
	
	//Escrever mensagens.
	
	ficheiro = fopen( "test.txt" , "r");	//abrir ficheiro 

	 size_t buf_size = 80;			// tamanho do buffer 
	size_t buf_read = buf_size;		// numero de elementos lidos
	
	while(buf_read == buf_size ){		// Ciclo de leitura do ficheiro, que so termina quando o ficheiro terminar.
		buf_read = fread(m1.texto , sizeof(char), buf_size , ficheiro);
		error = write(fd[1] , &m1 , sizeof(struct mensagem) );
		if(error == -1) perror("Erro a escrever pelo pipe do pai");

	}
	printf("(DEBUG:) Leitura de ficheiro terminada.");
	fclose(ficheiro);		

	error = close(fd[1]);	
	
	waitpid(pid, &status, 0);
	



	return 0;
}
