#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


struct mensagem {
	char texto[80];
	int numero;
}	;
	

int main(int argc, char *argv[]){
	int status;
	pid_t pid;
	int fd[2];  // [0] leitura [1] escrita 
	struct mensagem m1;
	int i = 0;


	//Criar pipe
	int error = pipe(fd);
	//printf("error = %i\n",error);
	if(error) perror("Erro a criar pipe");


	//Ciclo de filhos
	for (i = 0; i<10; i++) {
		pid = fork();
		if(pid < 0) perror("Erro a fazer fork");

		//Codigo do filho
		if(pid == 0) {
			error = close(fd[1]);  // fechar o de escrita
			if(error) perror("Erro a fechar o pipe filho escrita\n");

		
			// Ler  
			error = read(fd[0] , &m1 , sizeof(struct mensagem) );
			if(error == -1) perror("FILHO read return -1");
			pid = getpid();
			printf("Filho %i (pid:%i) , ronda %i diz: %s\n", i+1, pid, m1.numero, m1.texto);
	
			error = close(fd[0]);  // fechar o de leitura
			if(error) perror("FILHO Erro a fechar o pipe leitura");
			
			exit(m1.numero );

		} //fim do filho

	}
	


	// Codigo do Pai, sempre
	error = close(fd[0]); // fechar pipe leitura pai 
	if(error) perror("erro ao fechar pipe leitura pai");
	
	//Escrever mensagens.
	for (i = 0; i<10; i++){
		strcpy(m1.texto , "WIN!");
		m1.numero = i;
		error = write(fd[1], &m1, sizeof(struct mensagem) );
		if(error == -1) perror("Pai write returned -1");
		sleep(2);
	}	

	
	error = close(fd[1]); if(error) perror("erro ao fechar pipe escrita pai");

	for (i = 0; i < 10; i++){
		pid = wait(&status);
		printf("Terminou processo (%i) da ronda: %i\n", pid, WEXITSTATUS(status) );
	}

	



	return 0;
}
