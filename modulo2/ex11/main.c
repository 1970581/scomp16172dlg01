#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


struct mensagem {
	char nome[80];	// Nome do produto 
	int preco;	//Preco
	int codigo;	//Codigo de barras do produto 
	int filho;	//Filho que pediu.
}	;
	

int main(int argc, char *argv[]){
//	int status;
	pid_t pid;
	int fdPai[2];  // [0] leitura [1] escrita 
	int fdFilho[5][2];
	struct mensagem m1;
	int i = 0;
	int j = 0;

	//Criar pipe
	int error = pipe(fdPai);
	//printf("error = %i\n",error);
	if(error) perror("Erro a criar pipe");

	for (i = 0; i < 5; i++){
		error = pipe(fdFilho[i]);
		if(error) perror("Erro a criar pipe");
	}



	//Ciclo de filhos
	for (i = 0; i<5; i++) {
		pid = fork();
		if(pid < 0) perror("Erro a fazer fork");

		//Codigo do filho
		if(pid == 0) {
			error = close(fdPai[0]);  // fechar o de leitura
			if(error) perror("Erro a fechar o pipe filho read\n");

			//Vamos fechar todos os pipes do filhos de escrita.
			for (j = 0; j < 5; j++){
				if(j != i){		//Excepto o deste filho.
					error = close(fdFilho[j][1]);// Escrita
					if(error) perror("erro ao fechar pipe filho");
					error = close(fdFilho[j][0]);//Leitura
					if(error) perror("erro ao fechar pipe filho");

				}
			}	
			//Agora fechamos o pipe deste filho de escrita.
			error = close(fdFilho[i][1]);// Escrita
			if(error) perror("erro ao fechar pipe filho");


			//Escrever, pedir ao pai info sobre o produto.
			m1.codigo = i * 10 + 1;	//Codigo de barras.
			m1.filho  = i;		//Filho que pede.

			error = write(fdPai[1], &m1, sizeof(struct mensagem));
			if(error == -1) perror("FILHO write return -1");

			// Ler  
			error = read(fdFilho[i][0] , &m1 , sizeof(struct mensagem) );
			if(error == -1) perror("FILHO read return -1");
			pid = getpid();
			printf("Filho %i (pid:%i) , COD: %i NOME: %s PRECO: %i\n", i, pid, m1.codigo , m1.nome, m1.preco );
	
			error = close(fdPai[1]);  // fechar o de write
			if(error) perror("FILHO Erro a fechar o pipe ");
			
			error = close(fdFilho[i][0]);  // fechar o de leitura
			if(error) perror("FILHO Erro a fechar o pipe ");
				
			
			exit(1);

		} //fim do filho

	}
	


	// Codigo do Pai, sempre
	error = close(fdPai[1]); // fechar pipe escrita pai 
	if(error) perror("erro ao fechar pipe escrita pai");
	
	for (j = 0; j < 5; j++){
			error = close(fdFilho[j][0]);//Leitura
			if(error) perror("erro ao fechar pipe filho");
	}	

	size_t buffer = 1;
	int filho_int;
	
	do{	//Ciclo de leitura e escrita.
	
		//LER 
		buffer = read(fdPai[0], &m1, sizeof(struct mensagem));
		if(buffer < 0) perror("PAI read error from main pipe\n");
		
		// Caso nao exista mais nada para ler sai do loop sem escrever...
		if(buffer == 0) break;		

		//Processar na base de dados, escrever a mensagem a devolver.
		sprintf(m1.nome , "xpt%i", m1.codigo); // Escreve o nome na mensagem.
		m1.preco = m1.codigo *13;		// Escreve um preco 
		
		//WRITE
		filho_int = m1.filho;

		buffer = write(fdFilho[filho_int][1], &m1, sizeof(struct mensagem));
		if(buffer < 0) perror("PAI write error in son pipe\n");


	}while(buffer >0);

	//Fechar pipes que faltam.
	error = close(fdPai[0]); if(error) perror("PAI erro ao fechar pipe leitura pai");

	for (j = 0; j < 5; j++){
			error = close(fdFilho[j][1]);//WRITE
			if(error) perror("PAI erro ao fechar pipe dos filhos");
	}	

	return 0;
}
