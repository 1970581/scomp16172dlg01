#include <stdio.h>
#include <string.h>
#include "c.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


struct mensagem {
	char texto[80];
	int numero;
}	;
	

int main(int argc, char *argv[]){
	pid_t pid;
	int fd[5][2]; //Matriz de 5;2 para criar 5 pipes
	struct mensagem m1;
	int vec1[1000];
	int vec2[1000];
	int i = 0;
	int error;
	for(i = 0; i < 1000; i++){
		vec1[i] = i; //preencher o vetor
		vec2[i] = i;
	}
	int resultado_final = 0;	//Variavel para mostar o resultado de 2 SOMAS FEITAS
	int resultado[1000];
	int l = 0;	//Variavel importante para saber as posicoes de CADA UMA DAS SOMAS FEITAS
	int resultado_total = 0;
	
	//Ciclo de pipes
	for(i = 0; i < 5; i++){
		error = pipe(fd[i]);
		
		if(error){ 
			perror("Erro a criar pipe");
		}
	}

	//Ciclo de filhos
	for (i = 0; i < 5; i++) {
		
		pid = fork();
		if(pid < 0) perror("Erro a fazer fork");

		//Codigo do filho
		if(pid == 0) {
			error = close(fd[i][0]);  // fechar o de leitura
			if(error) perror("Erro a fechar o pipe filho leitura\n");
			
			int resultado_filho = 0;
			int j = 0;
			for (j = i * 200 ; j< i * 200 +200; j++){
				
				resultado_filho  = vec1[j] + vec2[j];
				m1.numero = resultado_filho ;	// A escrever o numero das 2 somas feitas 
				error = write(fd[i][1] , &m1 , sizeof(struct mensagem) );
				
				if(error == -1){ 
					perror("Erro a escrever pelo pipe do pai");
				}
			
						
			}
		
			error = close(fd[i][1]);  // fechar o de escrita
				
				if(error){
					 perror("Erro a fechar o pipe filho escrita");
				}	
			
			exit(0);

		} //fim do filho

	}
	

	for(i = 0; i < 5; i++){
		// Codigo do Pai, sempre
		error = close(fd[i][1]); // fechar pipe escrita pai 
		if(error) perror("erro ao fechar pipe escrita pai");
			error = 1;	
			
			while(error > 0){		// Ciclo de leitura do ficheiro, que so termina quando o ficheiro terminar.
				error = read(fd[i][0] , &m1 , sizeof(struct mensagem) );
				if(error == -1){
					perror("Erro a ler pelo pipe do pai");
				}
			
				if(error > 0){ 
					resultado_final = m1.numero ;
					resultado[l] = resultado_final;
					l++;
				}		

			}
	
		error = close(fd[i][0]);
	}
		

	for(i = 0; i < 1000; i++){
		resultado_total += resultado[i];
		printf("Resultado[%d]: %d\n", i, resultado[i]);
	}

	printf("Resultado total: %d\n", resultado_total);

	return 0;
}
